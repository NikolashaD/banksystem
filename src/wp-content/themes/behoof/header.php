<!DOCTYPE html>
<html lang="en">
  <head>
<!--    <meta charset="utf-8">-->
      <meta http-equiv="Content-Type" content="text/html; charset=windows-1251 /">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<link rel="icon" type="image/png" href="<?php bloginfo('template_directory'); ?>/images/favicon.png">

    <title>Behoof</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php bloginfo('template_directory'); ?>/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php bloginfo('template_directory'); ?>/css/styles.css" rel="stylesheet">
      

    <!-- Custom styles for this template -->
    <link href="<?php bloginfo('template_directory'); ?>/bootstrap/examples/offcanvas/offcanvas.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="bootstrap/assets/js/html5shiv.js"></script>
      <script src="bootstrap/assets/js/respond.min.js"></script>
    <![endif]-->

    <script src="<?php bloginfo('template_directory'); ?>/bootstrap/assets/js/jquery.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/bootstrap/examples/offcanvas/offcanvas.js"></script>

    
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/date.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.datePicker-2.1.2.js"></script>
    
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/Chart.js"></script>
     <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/maskedinput.js"></script> 
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/meiomask.js"></script> 
    
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/date.format.js"></script> 
    

    
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/datePicker.css" type="text/css" />

    <link href="<?php bloginfo('template_directory'); ?>/style.css" rel="stylesheet">
    
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/statistics.js"></script>

    <script src="<?php bloginfo('template_directory'); ?>/js/main.js"></script>

    <script>
        var baseUrl = "<?php echo get_bloginfo('wpurl'); ?>";
        var adminAjaxUrl = baseUrl + "/wp-admin/admin-ajax.php";
    </script>    
  </head>
<?php 
 $category_args = array(
     'type'                     => 'post',
     'child_of'                 => 0,
     'parent'                   => '',
     'orderby'                  => 'name',
     'order'                    => 'ASC',
     'hide_empty'               => 1,
     'hierarchical'             => 1,
     'exclude'                  => '',
     'include'                  => '',
     'number'                   => '',
     'taxonomy'                 => 'category',
     'pad_counts'               => false

 );
$categories = get_categories( $category_args );
?>
  <body>
    <div class="navbar navbar-fixed-top navbar-inverse" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php bloginfo('url');?>"><img id="logo" src="<?php bloginfo('template_directory'); ?>/images/logo.png" /></a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav main-menu">
            <li><a href="<?php bloginfo('url');?>"><span class="glyphicon glyphicon-home"></span>  Дом</a></li>
            <?php foreach($categories as $category): ?>
                <?php
                    $category_id = $category->cat_ID;
                    $page_url = get_category_link( $category_id );
                    $non_array = array('law-loans', 'business-loans', 'phys-loans', 'consumer-loans');
                ?>
                <?php if($category->slug != 'uncategorized'):  ?>
                    <?php //var_dump($non_array); ?>
                    <?php if(!in_array($category->slug,$non_array)): ?>
                    <?php if($category->slug == REQUESTS || $category->slug == RESPONSES): ?>

                        <?php if(is_user_logged_in()): ?>
                            <?php
                                $user_role = get_user_role();
                                if($user_role != 'client' && $user_role != 'editor'): ?>
                                    <li><a href="<?php echo $page_url; ?>"><?php echo $category->name; ?></a></li>
                                <?php endif; ?>
                        <?php endif; ?>

                    <?php else: ?>
                        <li><a href="<?php echo $page_url; ?>"><?php echo $category->name; ?></a></li>
                    <?php endif; ?>
                <?php endif; ?>
                <?php endif; ?>
            <?php endforeach; ?>
<!--            <li><a href="<?=ABOUT_US;?>"><span class="glyphicon glyphicon-comment"></span> О нас</a></li>-->
            <li><a href="<?=CONTACT_US;?>"><span class="glyphicon glyphicon-envelope"></span> Обратная связь</a></li>
              <?php if(is_user_logged_in()): ?>
                  <?php
                        $user_role = get_user_role();
                  ?>
                  <?php  if($user_role == 'client'): ?>
                      <li><a href="<?=PERSONAL_CABINET;?>"><span class="glyphicon glyphicon-user"></span> Личный кабинет</a></li>
                   <?php else: ?>
                      <li><a href="<?=PERSONAL_ADMIN_CABINET;?>"><span class="glyphicon glyphicon-user"></span> Рабочий кабинет</a></li>
                  <?php endif; ?>

              <?php endif; ?>
          </ul>
        </div><!-- /.nav-collapse -->
      </div><!-- /.container -->
    </div><!-- /.navbar -->
