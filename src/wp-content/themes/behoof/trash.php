<?php get_header(); ?>

<?php
    global $wpdb;
    $args = array(
        'role'         => 'client',
        'meta_key'     => '',
        'meta_value'   => '',
        'meta_compare' => '',
        'meta_query'   => array(),
        'include'      => array(),
        'exclude'      => array(),
        'orderby'      => 'login',
        'order'        => 'ASC',
        'offset'       => '',
        'search'       => '',
        'number'       => '',
        'count_total'  => false,
        'fields'       => 'all',
        'who'          => ''
    );

    $clients = get_users( $args );





    $category = get_category_by_slug( 'responses' );
    $category_id = $category->cat_ID;
    $contracts = query_posts('cat='.$category_id);
    
    $role = get_user_role();
    
    $sql = 'SELECT last_id FROM wp_blacklist';
    $black_ids = $wpdb->get_results($sql);
    $black_array = array();
    foreach($black_ids as $key=>$bi){
        $black_array[] =  $bi->last_id;
    }   


//    echo '<br><br><br><br>';
//    var_dump($percent_per_month );
?>


<div class="container">
    <div class="header">
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav profile-nav">
                <li><a href="#"></a></li>
            </ul>
        </div><!-- /.nav-collapse -->
        <ul class="nav nav-pills pull-right">
            <?php if($role == ADMIN || $role == 'head'): ?>
                <li><a href="<?=PERSONAL_ADMIN_CABINET;?>&statistics=true">Статистика</a></li>
            <?php endif;?>
            <li><a href="<?=PERSONAL_ADMIN_CABINET;?>&clients=true">Клиенты</a></li>
            <?php if($role == 'operator'): ?>
                <li><a href="<?=PERSONAL_ADMIN_CABINET;?>&contracts=true">Договора</a></li>
            <?php endif; ?>
            <li><a href="<?=wp_logout_url('/');?>">Выход</a></li>
        </ul>
    </div>



    <div class="row marketing">       
        <?php if(isset($_GET['clients']) && !empty($_GET['clients'])): ?>
            <div >
                <h2 style="margin-left:50px;" class="text-muted">Список всех клиентов банка</h2>
                <br><br>
                <div style="margin-left: 50px; background-color: #f2dede;" id="square-event"></div> <span style="padding-left: 10px;">- отмечены клиенты, которые находятся в <span style="font-wieght:bold;">черном</span> списке компании</span>      
                <table style="margin-top:70px;" class="table">
                    <tr>
                        <th style="vertical-align: middle;">№</th>
                        <th style="vertical-align: middle;">Клиент</th>
                        <th style="vertical-align: middle;">Адрес электронной почты</th>
                        <th style="vertical-align: middle;">Дата регистрации</th>
                        <th style="vertical-align: middle;">Текст оповещения</th>
                        <th style="vertical-align: middle;">Оповестить</th>
                        <?php if($role == 'security'): ?>
                            <th style="vertical-align: middle;">Бан</th>
                        <?php endif; ?>
                    </tr>
                    <?php if (!empty($clients)): ?>
                        <?php foreach ($clients as $key=>$client): ?>
                        <?php 
                            $black_class = '';
                            $blacklist = false;
                            if(in_array($client->ID, $black_array)) { $black_class = 'danger'; $blacklist = true; }; 
                         ?>
                            <tr class="<?=$black_class;?>">
                                <td style="vertical-align: middle;" width="20px"><?=$key+1; ?></td>
                                <td style="vertical-align: middle;" width="180px"><?=$client->user_nicename.'<br>'.$client->display_name; ?></td>
                                <td style="vertical-align: middle;" width="200px"><?=$client->user_email;?></td>
                                <td style="vertical-align: middle;" width="150px"><?=date('Y-m-d', strtotime($client->user_registered)); ?></td>
                                <td><textarea class="form-control status-comment" style="width:400px;" id="<?=$client->ID;?>"></textarea></td>
                                <td style="vertical-align: middle;"><a id="<?=$client->ID;?>" class="send-message" href="<?php echo ''; ?>">Оповестить &nbsp<span class="glyphicon glyphicon-envelope"></span></a></td>
                                <?php if($role == 'security'): ?>
                                    <td style="vertical-align: middle;"><?php if(!$blacklist): ?><a style="color:black;" id="<?=$client->ID;?>" class="ban-user" href="<?php echo ''; ?>">Бан &nbsp<span class="glyphicon glyphicon-fire"></span></a><?php endif; ?></td>
                                <?php endif; ?>                                
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </table>
            </div>

        <?php elseif(isset($_GET['contracts']) && !empty($_GET['contracts'])): ?>
            <div >
                <h2 style="margin-left:50px;" class="text-muted">Список всех заключенных договоров банка</h2>
                <br>
                <div style="margin-left: 50px;"   id="square-white"></div> <span style="padding-left: 10px;">- отмечены кредиты, которые в текущем месяце уже были оплачены</span>
                <br><br>
                <div style="margin-left: 50px; background-color: #fcf8e3;" id="square-event"></div> <span style="padding-left: 10px;">- отмечены кредиты, которые следует оплатить в текущем месяце</span>
                <br><br>
                <div style="margin-left: 50px; background-color: #f2dede;" id="square-event"></div> <span style="padding-left: 10px;">- отмечены кредиты, платежи по которым просрочены</span>
                <br><br>
                <div style="margin-left: 50px;" id="square-grey"></div> <span style="padding-left: 10px;">- отмечены кредиты, платежи по которым полностью погашены</span>

                <table style="margin-top:70px;" class="table table-hover">
                    <tr class="grey">
                        <th>№</th>
                        <th>Название кредита</th>
                        <th>Клиент</th>
                        <th>Дата заключения</th>
                        <th>Дата последнего платежа</th>
                        <th>Кол-во взносов</th>
                        <th>Возможность досрочного погашения</th>
                    </tr>
                    <?php if (!empty($contracts)): ?>
                        <?php foreach ($contracts as $key=>$contract): ?>
                            <?php
                            if($contract->post_excerpt == 'Approved'): ?>
                                <?php
                                $user = get_user_by( 'login', explode(' ',$contract->post_title)[1]);
                                
                                $user_name = $user->user_login. '<br>' .$user->display_name;

                                $post_name = $contract->post_name;
                                $loan_id = explode('-',$post_name)[2];
                                $loan_title = get_post($loan_id)->post_title;

                                $events = get_clients_events($user->ID);
                                $main_per_month = '';
                                $percent_per_month = '';
                                $col_months = '';
                                if($events){
                                    foreach($events as $event){
                                        if(trim($event['title']) == trim($loan_title)){
                                            $main_per_month = $event['main_per_month'];
                                            $percent_per_month = $event['percent_per_month'];
                                            $col_months = $event['col_months'];
                                            $event_ID = $event['id'];
                                            $event_start_date = $event['start_date'];
                                        }
                                    }
                                }
                            
                            $pay_year_month = explode('-', $event_start_date)[0].'-'.explode('-', $event_start_date)[1];
                                
                            $cur_month = date('m');
                            $pay_month = explode('-', $event_start_date)[1];
                            
                            $cur_day = date('d');
                            $pay_day = explode('-', $event_start_date)[2];
                            
                            $active = true;
                            $canceled = false;
                            $tr_class = '';


                            if($pay_year_month == date('Y-m')){
                                $tr_class = '';
                                $active = false;
                            }
                            else{
                                $months_count = (int)abs((strtotime(date('Y-m')) - strtotime($pay_year_month))/(60*60*24*30));
                                if( $months_count > 1 ){
                                    $tr_class = "danger";
                                    $active = true;
                                    // считать пеню
                                    $days = (strtotime(date('Y-m-d')) - strtotime($event_start_date)) / (24 * 3600);
                                    $fine_rate = preg_replace('/\%/', '', get_field('fine_rate',$loan_id));
                                    $rate = get_rate($days, $fine_rate,$main_per_month);                                     
                                }else{
                                    $days = (date('d') - $pay_day);
                                    if($days < 0){
                                        $tr_class = "";
                                        $active = false;
                                    }else{
                                        $tr_class = 'warning';
                                        $active = true;              
                                        // считать пеню
                                        $fine_rate = preg_replace('/\%/', '', get_field('fine_rate',$loan_id));
                                        $rate = get_rate($days, $fine_rate,$main_per_month);                                 
                                    }
                                }
                            }                            
                            
                            if($col_months <= 0){
                                $tr_class = 'canceled';
                                $canceled = true;
                            }
                            ?>
                                <tr class="<?=$tr_class; ?> line-one">
                                    <td width="10px" style="vertical-align: middle;"><?=$key+1; ?></td>
                                    <td width="160px" style="vertical-align: middle;"><a href="<?=get_permalink($loan_id);?>"><?=$loan_title; ?></a></td>
                                    <td width="200px" style="vertical-align: middle;"><?=$user_name; ?></td>
                                    <td style="vertical-align: middle;"><?=date('Y-m-d', strtotime($contract->post_date)); ?></td>
                                    <td style="vertical-align: middle;"><?=date('Y-m-d', strtotime($event_start_date)); ?></td>
                                    <td style="vertical-align: middle;"><?=$col_months; ?></td>
                                    <td>
                                        Основной:<br><?=$main_per_month; ?><hr>
                                        <?php if($tr_class == 'class="danger"' || $tr_class == 'class="warning"'): ?>                                      
                                            Пеня:<br> <?=$rate; ?> дог ед.<hr>
                                        <?php endif; ?>                                        
                                        Проценты:<br><?=$percent_per_month; ?>                                    
                                    </td>                                    
                                    <td width="100px;" style="vertical-align: middle;">
                                        <?php if($active && !$canceled): ?>
                                            <a id="<?=$event_ID;?>" class="payment" href="<?php echo ''; ?>">Оплатить   <span class="glyphicon glyphicon-usd"></span></a>
                                        <?php endif; ?>
                                        <?php if(!$active && !$canceled): ?>
                                            Оплачено   <span class="glyphicon glyphicon-ok"></span>
                                        <?php endif; ?>
                                        <?php if($canceled): ?>
                                            <a style="color:black;" id="<?=$event_ID.'-'.$contract->ID; ?>" class='remove-contract' href="<?php echo ''; ?>">Закрыть    <span class="glyphicon glyphicon-trash"></span></a>
                                        <?php endif; ?>
                                    </td>
                                    <td  style="vertical-align: middle;">
                                        <?php if(get_field('advanced_repayment',$loan_id) == 'да' && !is_null(get_advanced_repayment($event_ID))): ?>
                                            <p>Сумма:</p>
                                            <p><?php echo get_advanced_repayment($event_ID); ?></p>
                                            <a  id="<?=$event_ID.'-'.$contract->ID; ?>" class='remove-contract' href="<?php echo ''; ?>">Погасить    <span class="glyphicon glyphicon-trash"></span></a>
                                        <?php endif; ?>                                        
                                    </td>                                        
                                </tr>
                                <tr class="line-two">
                                    <td colspan="8">
                                <table class="data"  cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                        <div class="info-box">
                                            <p ><span style="display: inline-block; width: 100px; float:left;">Subdivision:</span><span class="subdivision data"></span> </p> 
                                             <p ><span style="display: inline-block; width: 100px; float:left;">Ð¡onstruction:</span><span class="construction data"></span> </p>
                                            <p ><span style="display: inline-block; width: 100px; float:left;">Comments:</span><span class="comments data"></span> </p>
                                            <a class="ed edit" href="#">Edit</a><a class="del remove" href="#">Delete</a>
                                        </div>
                                        </td>
                                    </tr>
                                </table>
                                
                            </td> 
                                </tr>
                             <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </table>
            </div>
        
        <?php elseif(isset($_GET['statistics']) && !empty($_GET['statistics']) && ($role == ADMIN || $role == 'head')): ?>
        <div class="col-lg-6">
            <script src="<?php bloginfo('template_directory'); ?>/js/statistics.js"></script>

            <h2 style="margin-left:150px;" class="text-muted">Статистика</h2>
            <?php             
                $category = get_category_by_slug( 'responses' );
                $category_id = $category->cat_ID;                  
                
                $activeMonthContracts = get_active_contracts($category_id, 'month');
                $addedMonthContracts = get_added_contracts($category_id, 'month');
                $overduedMonthContracts = get_overdued_contracts($category_id, 'month');           
                $extinguishedMonthContracts = get_extinguished_ontracts($category_id, 'month');
    
                $active_month_contracts = count($activeMonthContracts);
                $added_month_contracts = count($addedMonthContracts);
                $overdued_month_contracts = count($overduedMonthContracts);
                $extinguished_month_contracts = count($extinguishedMonthContracts);
                
                $max_array = array($active_month_contracts, $added_month_contracts, $overdued_month_contracts, $extinguished_month_contracts);
                $max = get_max($max_array);
            ?>
            <script>
                jQuery(document).ready(function(){
                    max = <?=$max;?>;
                    ativeMonthContracts = <?=$active_month_contracts;?>;
                    addedMonthContracts = <?=$added_month_contracts;?>;
                    overduedMonthContracts = <?=$overdued_month_contracts;?>;
                    extinguishedMonthContracts = <?=$extinguished_month_contracts;?>;
                    monthStatistics(ativeMonthContracts, addedMonthContracts, overduedMonthContracts, extinguishedMonthContracts , max);
                });        
            </script>            
            <canvas id="last-month" width="600" height="400"></canvas> 
            
            <br><br><br><br><br><br>
            <?php 
                $addedYearContracts = get_added_contracts($category_id, 'year');
                $extinguishedYearContracts = get_extinguished_ontracts($category_id, 'year');
            ?>
            
            <script>
                jQuery(document).ready(function(){
                    addedYearContracts = '<?php echo $addedYearContracts;?>';
                    extinguishedYearContracts = '<?php echo $extinguishedYearContracts;?>';
                    yearStatistics(addedYearContracts,extinguishedYearContracts);
                });        
            </script>              
            <canvas id="last-year" width="800" height="500"></canvas>              
            
        </div>       
        
        
        <div class="col-lg-6">
            <h2 style="margin-left:150px; margin-top:50px;" class="text-muted"></h2>
             &nbsp &nbsp&nbsp&nbsp&nbsp<span style="text-decoration: underline;">Представлена статистика за последний месяц </span>
            <br><br>
            <div style="margin-left:30px; background-color: rgba(151,187,205, 0.5);" id="square-curday"></div> <span style="padding-left: 10px;">- отмечены все активные договора по кредитам</span>
            <br><br>
            <div style="margin-left:30px; background-color: rgba(4, 163, 156, 0.3);" id="square-curday"></div> <span style="padding-left: 10px;">- отмечены все договора, заключенные в этом месяце </span>
            <br><br>
            <div style="margin-left:30px; background-color: rgba(178, 34, 34, 0.3);" id="square-curday"></div> <span style="padding-left: 10px;">- отмечены все договора по кредитам, платежи по которым просрочены</span>        
            <br><br>
            <div style="margin-left:30px; background-color: rgba(220,220,220,0.5);" id="square-curday"></div> <span style="padding-left: 10px;">- отмечены все договора по кредитам, платежи по которым погашены</span>
        
        </div>     
        
        <div style="margin-top:250px;" class="col-lg-6">
            <h2 style="margin-left:150px; margin-top:50px;" class="text-muted"></h2>
             &nbsp &nbsp&nbsp&nbsp&nbsp<span style="text-decoration: underline; margin-left: 190px;">Представлена статистика за последний год </span>
            <br><br>
            <div style="margin-left:190px; background-color: rgba(4, 163, 156, 0.3);" id="square-curday"></div> <span style="padding-left: 10px;">- договора, заключенные в этом месяце </span>
            <br><br>
            <div style="margin-left:190px; background-color: rgba(220,220,220,0.5);" id="square-curday"></div> <span style="padding-left: 10px;">- договора, платежи по которым погашены</span>
                    
            <br><br><br>
            <?php 
                $addedContracts = count(get_added_contracts($category_id, 'all_period'));
                $extinguishedContracts = count(get_extinguished_ontracts($category_id, 'all_period'));
                $z = $addedContracts + $extinguishedContracts;
                
                $x = round($addedContracts * 100 / $z);
                $y = round($extinguishedContracts * 100 / $z);
            ?>
            
            <script>
                jQuery(document).ready(function(){
                    addedContracts = <?php echo $x;?>;
                    extinguishedContracts = <?php echo $y;?>;
                    allStatistics(addedContracts,extinguishedContracts);
                });        
            </script>    
            &nbsp &nbsp&nbsp&nbsp&nbsp<span style="text-decoration: underline; margin-left: 190px;">Представлена статистика за весь период </span>
            <br><br><br>
            <canvas style="margin-left:250px;" id="all" width="300" height="300"></canvas>  
        </div>    
        <? else: ?>
        
        
        <?php endif;?>
        
  
    </div>

<?php get_footer(); ?>

</div><!--/.container-->
<style>
.form-control {
    display: initial;
}
</style>

































           DateTime[] datesOfPayment = {new DateTime(2011, 11, 30), 
                                            new DateTime(2011, 12, 31), 
                                            new DateTime(2012, 1,31), 
                                            new DateTime(2012,2,29), 
                                            new DateTime(2012,3,31)};
            //дальше определяем текущую дату, например 
            DateTime currentDate = new DateTime(2012, 4, 4);
            int fine = 0; // определяем переменную для пени
            int payment = 200000; // определяем ежемесячный платеж
            double fineRate = 5; // ставка пени
            int days = 0; // переменная для количества дней в месяце
            for (int i = 0; i < datesOfPayment.Length-1; i++)
            {
                days = (datesOfPayment[i] - datesOfPayment[i + 1]).Days;
                fine += MonthFineCount(days, payment * (i + 1), fineRate);
            }
            days = (currentDate - datesOfPayment[datesOfPayment.Length - 1]).Days;
            fine += MonthFineCount(days, payment * datesOfPayment.Length, fineRate);
                Console.WriteLine(fine);


                Console.ReadLine();
        }
        
        private static int MonthFineCount(int days, int sum, double fineRate)
        {
            int fine = Convert.ToInt32(sum * days * fineRate / 100);
            return fine;
        }
