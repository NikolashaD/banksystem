<?php
$post_id  = get_the_ID();
$item = get_post( $post_id);
?>
<div class="row row2">
    <h2><a href="<?php echo get_permalink( $item->ID ); ?> "><?php echo the_title(); ?></a></h2>
    <p><?php echo implode(' ', array_slice(explode(' ', $item->post_content), 0, 300)); echo"..."; ?></p>
    <br><br>
</div><!--/row-->
