<?php
    $post_id  = get_the_ID();
    $user_ID = get_current_user_id();
    $post_category = get_the_category( $post_id );
    $item = get_post( $post_id);
    $is_show = false;
    $user_role = get_user_role();
    foreach($post_category as $cat){
        $parent_id = $cat->parent;
        $cat_array[] = get_category($parent_id)->slug;
    }

?>
<div class="row row2">
<div class="after"></div>
<div class="remove">
  <h2><?php echo $item->post_title; ?></h2>
    <?php if(in_array(LOANS,$cat_array) || $post_category[0]->slug == LOANS): ?>
        <a style="margin-left:350px;" href="#conditions">Перейти к основным условиям <span class="glyphicon glyphicon-map-marker"></span></a>
        <a style="margin-left:30px;" href="#graph">Перейти к графику выплат <span class="glyphicon glyphicon-map-marker"></span></a>
    <?php endif; ?>

  <p><pre><?php echo $item->post_content; ?></pre></p>
  <br><br>

  <?php if(in_array(LOANS,$cat_array) || $post_category[0]->slug == LOANS): ?>
      <a name="conditions"></a>
    <h3>Условия</h3>
<?php
    $all_fields = get_field_objects($item->ID);
 
?>

      <table class="table table-striped">
        <?php foreach($all_fields as $key=>$field): ?>
            <tr>
                <?php if($field['name'] != 'check_role' && $field['name'] != 'loans_summ' && $field['name'] != 'percent'): ?>
                    <th><?=$field['label'];?></th>
                    <td><?=get_field($key,$item->ID); ?></td>
                <?php endif; ?>
            </tr>
          <?php endforeach; ?>
      </table>
    <br>
    <a name="graph"></a>
    <h3>Расчетная информация по платежам</h3>
    <?php 
        $loan_summ = get_field('loans_summ',$item->ID);
        $loan_summ = (int)preg_replace('/[^\d]+/', '', $loan_summ);
        
        $percent = explode(' ', get_field('percent',$item->ID))[0];
        $percent = preg_replace('/\%/', '', $percent);
        $percent = str_replace(',', '.', $percent);
        $percent = (float) $percent;
        $term = explode(' ',get_field('loans_term',$item->ID))[0];

        $payment_arr = get_all_payment_data($term, $loan_summ, $percent);
        
        $total_summ = $payment_arr['total_summ'];
        $percent_per_month = $payment_arr['percent_per_month'];
        $main_summ_per_month = $payment_arr['main_summ_per_month'];    
       
    ?>
      <table class="table table-striped">
            <tr class="warning">
                <th>Данные:</th>
                <th>Сумма кредита</th>
                <th>Процентная ставка</th>
                <th>На срок</th>
                <th></th>
            </tr> 
            <tr>
                <td></td>
                <td><?=$loan_summ;?></td>
                <td><?=$percent;?> % годовых</td>
                <td><?=$term;?> года/лет</td>
                <td></td>
            </tr>             
            <tr class="warning">
                <th>График выплат</th>
                <th>Полная сумма выплат</th>
                <th>Выплачено процентов</th>
                <th>Основной платеж</th>
                <th>Выплачено за услуги</th>
            </tr>
            <tr>
                <td>За весь период выплат</td>
                <td><?=$total_summ;?></td>
                <td><?=$percent_per_month*$term*12;?></td>
                <td><?=$main_summ_per_month*$term*12;?></td>
                <td>0.00</td>
            </tr>   
            <tr>
                <td>Ежемесячно <br>(<?=$term*12;?> мес.)</td>
                <td><?=$percent_per_month+$main_summ_per_month; ?></td>
                <td><?=$percent_per_month;?></td>
                <td><?=$main_summ_per_month;?></td>
                <td>0.00</td>
            </tr>              
      </table>    


    <button style="width:150px; margin-left: 632px;" class="btn btn-lg btn-primary btn-block place-order" id="<?php echo $post_id; ?>" type="submit">Подать заявку <span class="glyphicon glyphicon-chevron-right"></span></button><br>
  <?php endif; ?>
    <?php if($post_category[0]->slug == REQUESTS): ?>
        <?php if($item->post_excerpt != 'Approved'): ?>
            <h3>Изменить статус</h3>


            <pre><br />
                <span>Текущий статус : </span><span style="color:darkred; text-decoration: underline;"><?=$item->post_excerpt; ?></span><br />
                <?php 
                              
                switch(end(explode('&&&',$item->comment_status))){
                    case 'Processing':
                        $by = 'Комментарий оператора';
                        $last_state = 'Processing';
                        break;
                    case 'Reviewing by security':
                        $by = 'Комментарий службы безопасности';
                        break;
                    case 'Reviewing by head':
                        $by = 'Комментарий члена кредитного комитета';
                        $last_state = 'Reviewing by head';
                        break;
                    default:
                        break;
                }         
                
                switch($item->post_excerpt){
                    case 'Processing':
                        ?>
                        <?php if($user_role == 'operator'): ?>
                            <?php $is_show = true; ?>
                            <div>
                                <input style="margin-left:-55px" type="radio" name="status" value="security-review" checked> На проверку службе безопасности<br />
                        <?php endif; ?>
                        <?php
                        break;
                    case 'Reviewing by security':
                        ?>
                        <?php if($user_role == 'security'): ?>
                            <?php $is_show = true; ?>
                            <div>
                                <?php if($last_state == 'Processing'): ?>
                                    <input style="margin-left:-175px;" type="radio" checked name="status" value="processing"> Оператору<br />
                                <?php endif; ?>
                                <?php if($last_state == 'Reviewing by head'): ?>
                                    <input style="margin-left:-280px;" type="radio" checked name="status" value="head-review"> На проверку члену кредитного комитета <br /><br />
                                <?php endif; ?>
                        <?php endif; ?>
                        <?php
                        break;
                    case 'Reviewing by head':
                        ?>
                        <?php if($user_role == 'head'): ?>
                            <?php $is_show = true; ?>
                            <div>
                                <input style="margin-left:-55px" type="radio" name="status" value="security-review" checked> На проверку службе безопасности<br /><br />
                        <?php endif; ?>
                        <?php
                        break;
                    default:
                        break;
                }
                ?>

                <?php if($is_show): ?>
                    <label style="margin-left: -70px;"><?=$by;?>:</label>   <label><?=explode('&&&',$item->comment_status)[0];?></label>
                    <textarea placeholder="Комментарий..." class="form-control status-comment" rows="3" style="width:275px; margin-left:50px;"></textarea>
                    <input style="margin-left: -20px;" type="submit" class="change-status" value="Изменить статус" />
                    </div>
                <?php endif; ?>
            </pre>

            <br /><bt /><br />
            <?php if($user_role == 'operator'): ?>
                <button style="width:25%; float:left;" class="btn btn-lg btn-primary btn-block no" id="<?php echo $post_id; ?>" type="submit"><span class="glyphicon glyphicon-chevron-left"></span> Отказать в кредите</button>
                <button style="width:25%; margin-left:610px;" class="btn btn-lg btn-primary btn-block yes" id="<?php echo $post_id; ?>" type="submit">Подтвердить кредит <span class="glyphicon glyphicon-chevron-right"></span></button><br>
            <?php endif; ?>
            <?php if($user_role == 'head'): ?>  
                <?php
                    $vote_inf = get_vote_information($user_ID,$post_id);      
                    $is_voted = $vote_inf['is_voted'];
                    $yes = $vote_inf['yes'];
                    $no = $vote_inf['no'];
                    $is_inprocess = $vote_inf['is_inprocess'];
                    
                    if($is_inprocess){
                        $state = 'открыто';
                    }else{
                        $state = 'закрыто';
                        ?>
                        <script>
                            jQuery(document).ready(function(){
                                removeApproveRequest(null,'<?=$vote_inf['func'];?>',true);
                            });
                        </script>
                        <?php
                    }
                ?>
                
                <?php if(!$is_voted): ?>               
                    <form class="webPoll" method="post" action="/poll/test.php">
                        <h4>Голосование по заявке</h4>
                        <fieldset>
                        <ul>
                        <li>
                        <label class='poll_active'>
                        <input type='radio' name="vote" value='1' /><span style="font-size:15px;">Подтвердить</span>
                        </label><br>
                        <label class='poll_active'>
                        <input type='radio' name="vote" value='0' /><span style="font-size:15px;">Отклонить</span>
                        </label>
                        </li>
                        </ul>
                        </fieldset>
                        <p class="buttons">
                            <button style="margin-top:10px;" type="submit" name="<?php echo $post_id.'-'.get_current_user_id(); ?>  " class="vote">Голосовать!</button>
                        </p>
                    </form> 
                <?php else: ?>
                   <form class="webPoll" method="post" action="/poll/test.php">
                       <a name="voting"></a>
                        <h4>Результаты голосования  : <?=$state;?></h4>
                        <fieldset>
                        <ul style="padding-right: 50px;">
                            <li>
                                <label class='poll_active'>
                                    <span style="font-size:15px;">Подтвердить</span>                                    
                                </label>
                                <div class="progress">
                                  <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="<?=$yes;?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $yes; echo '%'; ?>">
                                  </div>
                                </div>                              
                            </li>
                            <li>
                                <label class='poll_active'>
                                    <span style="font-size:15px;">Отклонить</span>
                                </label>
                                <div class="progress">
                                  <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="<?=$no?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $no; echo '%'; ?>">
                                  </div>
                                </div>                               
                            </li>
                        </ul>
                        </fieldset>
                    </form>                 
                <?php endif; ?>
            <?php endif; ?>    
    <?php else: ?>
            <script>
                jQuery(document).ready(function(){
                    limit = $('span#limit').text();
                    
                    loanSumm = $('span#loanSumm').text();
                    loanTerm = $('span#loanTerm').text();
                    loanPercentage = $('span#loanPercentage').text().split(' ')[0];
                    var now = new Date();
                    curDate = now.format("yyyy/mm/dd");
                    
                    belky = limit.match(/\$/);
                    
                    if(belky == null){
                        belky = true;
                        $('input[type="checkbox"]#belky').attr('checked','checked');
                    }else{
                        belky = false;                        
                    }
                    
                    $('input[type="text"]#loanSumm').val(loanSumm);
                    $('input[type="text"]#from').val(curDate);
                    $('input[type="text"]#to').val(loanTerm);
                    $('input[type="text"]#percent').val(loanPercentage);                    
                    
                });
            </script>
  
            <div style="width:61%; float:left;" >
                <label>Сумма кредита: </label> <input  style="width:330px" type="text" id="loanSumm" disabled> <input disabled type="checkbox" value="true" id="belky"> Бел.руб
                    
                <br><br>
                <label>На период с</label>
                <br />
                <input style="float: left; width:330px;" type="text" id="from" disabled>
                <br /><br /><label>На</label><br>
                <input style=" width:330px;"  type="text" id="to" disabled><span> лет</span>
                <br /><br />
                <label>Под</label> <input style=" width:330px;" type="text" id="percent" disabled><span> % годовых </span>
                <br /><br />
                <label>Цель кредита: </label> <textarea class="form-control status-comment" rows="3" style="width:500px;" id="aim" placeholder="" ></textarea>
            </div>
            <div style="margin-top:345px;">
                <a href="#"  style="text-decoration:none; width:30%; margin-left:570px;" class="btn btn-lg btn-primary btn-block export" >Экспорт договора в Word <span class="glyphicon glyphicon-chevron-right"></span></a><br>
<!--                <a href="#"  style="text-decoration:none; width:30%; margin-left:570px;" class="btn btn-lg btn-primary btn-block export" >Заполнение договора online<span class="glyphicon glyphicon-chevron-right"></span></a><br>-->
                <a href="#"  style="text-decoration:none; width:30%; margin-left:570px;" class="btn btn-lg btn-primary btn-block addUser" >Создать личный кабинет <span class="glyphicon glyphicon-chevron-right"></span></a><br>
            </div>

    <?php endif; ?>
<?php endif; ?>

    <?php if($post_category[0]->slug != REQUESTS): ?>
    <br><br>
    <?php //display_comments_evolved(); ?>
    <?php endif; ?>
</div>
</div><!--/row-->


