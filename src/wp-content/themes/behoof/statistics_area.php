<div class="col-lg-6">
    

    <h2 style="margin-left:150px;" class="text-muted">Статистика</h2>
    <?php             
        $category = get_category_by_slug( 'responses' );
        $category_id = $category->cat_ID;                  

        $activeMonthContracts = get_active_contracts($category_id, 'month');
        $addedMonthContracts = get_added_contracts($category_id, 'month');
        $overduedMonthContracts = get_overdued_contracts($category_id, 'month');           
        $extinguishedMonthContracts = get_extinguished_ontracts($category_id, 'month');

        $active_month_contracts = count($activeMonthContracts);
        $added_month_contracts = count($addedMonthContracts);
        $overdued_month_contracts = count($overduedMonthContracts);
        $extinguished_month_contracts = count($extinguishedMonthContracts);

        $max_array = array($active_month_contracts, $added_month_contracts, $overdued_month_contracts, $extinguished_month_contracts);
        $max = get_max($max_array);
    ?>
    <script>
        jQuery(document).ready(function(){
            max = <?=$max;?>;
            ativeMonthContracts = <?=$active_month_contracts;?>;
            addedMonthContracts = <?=$added_month_contracts;?>;
            overduedMonthContracts = <?=$overdued_month_contracts;?>;
            extinguishedMonthContracts = <?=$extinguished_month_contracts;?>;
            monthStatistics(ativeMonthContracts, addedMonthContracts, overduedMonthContracts, extinguishedMonthContracts , max);
        });        
    </script>            
    <canvas id="last-month" width="600" height="400"></canvas> 

    <br><br><br><br><br><br>
    <?php 
        $addedYearContracts = get_added_contracts($category_id, 'year');
        $extinguishedYearContracts = get_extinguished_ontracts($category_id, 'year');
    ?>

    <script>
        jQuery(document).ready(function(){
            addedYearContracts = '<?php echo $addedYearContracts;?>';
            extinguishedYearContracts = '<?php echo $extinguishedYearContracts;?>';
            yearStatistics(addedYearContracts,extinguishedYearContracts);
        });        
    </script>              
    <canvas id="last-year" width="800" height="500"></canvas>              

</div>       


<div class="col-lg-6">
    <h2 style="margin-left:150px; margin-top:50px;" class="text-muted"></h2>
     &nbsp &nbsp&nbsp&nbsp&nbsp<span style="text-decoration: underline;">Представлена статистика за последний месяц </span>
    <br><br>
    <div style="margin-left:30px; background-color: rgba(151,187,205, 0.5);" id="square-curday"></div> <span style="padding-left: 10px;">- отмечены все активные договора по кредитам</span>
    <br><br>
    <div style="margin-left:30px; background-color: rgba(4, 163, 156, 0.3);" id="square-curday"></div> <span style="padding-left: 10px;">- отмечены все договора, заключенные в этом месяце </span>
    <br><br>
    <div style="margin-left:30px; background-color: rgba(178, 34, 34, 0.3);" id="square-curday"></div> <span style="padding-left: 10px;">- отмечены все договора по кредитам, платежи по которым просрочены</span>        
    <br><br>
    <div style="margin-left:30px; background-color: rgba(220,220,220,0.5);" id="square-curday"></div> <span style="padding-left: 10px;">- отмечены все договора по кредитам, платежи по которым погашены</span>

</div>     

<div style="margin-top:250px;" class="col-lg-6">
    <h2 style="margin-left:150px; margin-top:50px;" class="text-muted"></h2>
     &nbsp &nbsp&nbsp&nbsp&nbsp<span style="text-decoration: underline; margin-left: 190px;">Представлена статистика за последний год </span>
    <br><br>
    <div style="margin-left:190px; background-color: rgba(4, 163, 156, 0.3);" id="square-curday"></div> <span style="padding-left: 10px;">- договора, заключенные в этом месяце </span>
    <br><br>
    <div style="margin-left:190px; background-color: rgba(220,220,220,0.5);" id="square-curday"></div> <span style="padding-left: 10px;">- договора, платежи по которым погашены</span>

    <br><br><br>
    <?php 
        $addedContracts = count(get_added_contracts($category_id, 'all_period'));
        $extinguishedContracts = count(get_extinguished_ontracts($category_id, 'all_period'));
        $z = $addedContracts + $extinguishedContracts;

        $x = round($addedContracts * 100 / $z);
        $y = round($extinguishedContracts * 100 / $z);
    ?>

    <script>
        jQuery(document).ready(function(){
            addedContracts = <?php echo $x;?>;
            extinguishedContracts = <?php echo $y;?>;
            allStatistics(addedContracts,extinguishedContracts);
        });        
    </script>    
    &nbsp &nbsp&nbsp&nbsp&nbsp<span style="text-decoration: underline; margin-left: 190px;">Представлена статистика за весь период </span>
    <br><br><br>
    <canvas style="margin-left:250px;" id="all" width="300" height="300"></canvas>  
</div>    
