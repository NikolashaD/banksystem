<?php get_header(); ?>
<?php if( is_home()): ?>
    <?php get_template_part( 'front-page' ); ?>
<?php endif; ?>
<?php if(!is_home() && is_paged() ) : ?>
    <?php get_page($page); ?>
<?php endif; ?>
<?php if(is_single($post) ) : ?>
    <?php get_template_part( 'single' ); ?>
<?php endif; ?>
<?php if(is_category() ) : ?>
    <?php get_template_part( 'category' ); ?>
<?php endif; ?>
<?php if(is_tag($tag) ) : ?>
    <?php get_template_part( 'tag-page' ); ?>
<?php endif; ?>
<?php get_footer(); ?>
