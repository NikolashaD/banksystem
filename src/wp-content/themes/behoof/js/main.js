jQuery(document).ready(function(){
    $('ul.navbar-nav > li').click(addClassActive);
    $('.place-order').click(redirectPage);
    $('.send-request').click(handlingRequest);
    $('.no').click(removeApproveRequest);
    $('.yes').click(removeApproveRequest);
    $('.change-status').click(changeStatus);
    $('.export').click(exportToWordDoc);

    $('.addUser').click(addPersonalCabinet);

    $('.edit-profile').click(editProfile);

    $('.payment').click(payment);
    $('.remove-contract').click(removeContract);
    $('.send-message').click(sendMessage);
    $('.ban-user').click(banUser);
    
    $('button.vote').click(vote);




    $('img.avatar').addClass('img-circle');
    $('table.calendar-table').find('tr').each(function(ind,val){
        //$(val).find('td.day-with-date').eq( 5 ).css('background', '#F3F3F2');
        //$(val).find('td.day-with-date').eq( 6 ).css('background', '#F3F3F2');
    });

    $('div.widget-area').find('ul').addClass('nav');

    $('.calnk').parent().parent().css('background', 'Thistle');
    
    $('textarea.wpcf7-textarea').addClass('form-control status-comment');
    $('input[type="submit"].wpcf7-submit').css('width','380px');


    $('div.cal').find('span.calnk').each(function(ind,val){
        eventText = $(val).text();
        $(val).parent().parent().addClass('ev');
        $(val).parent().parent().attr('data-title', eventText);
        $(val).parent().remove();
        $(val).remove();
    });

    if($('ul.controls,inline,clearfix')){
        $('ul.controls,inline,clearfix').remove();
        $('div#___comments_0').remove();
    }
  
//    $('#from').datePicker({
//        displayClose:true,
//        closeOnSelect:false
//    });
    
    $('input[type="text"]').setMask();
    $('span').setMask();
    

    $('.line-one').click(function(){
        $(this).toggleClass('act');
        $(this).next('.line-two').fadeToggle(400);
    });
});



function vote(){
    $('div.alert-danger').remove();
    attrib = $(this).attr('name').split('-');
    
    request_id = attrib[0];
    user_id = attrib[1];
    checked = $('input[type="radio"][name="vote"]:checked').val();
    
    if($('input[type="radio"][name="vote"]:checked').length == 0){
        $('form.webPoll').before('<div class="alert alert-danger">Нужно выбрать один из вариантов для голосования.</div>');
        return false;
    }

    data = {action: 'set_vote',
        user_id:user_id,
        request_id:request_id,
        checked:checked
    }
    jQuery.ajaxSetup({async:false});
    jQuery.post(adminAjaxUrl, data, function(response){
        url = $(location).attr('href');
        location.reload(url+'#voting');
    });    
    
    return false;    
}


function banUser(){
    message = 'Вы уверены, что хотите отправить данного клиента в черный список?';
    if (confirm(message)) {    
        user_id = $(this).attr('id');

        data = {action: 'ban_user',
            user_id:user_id
        }
        jQuery.ajaxSetup({async:false});
        jQuery.post(adminAjaxUrl, data, function(response){
            location.reload();
        });
        $('html, body').animate({ scrollTop: 0 }, 'fast');
    }

    return false;    
}


function sendMessage(){
    user_id = $(this).attr('id');
    message = $('textarea#'+user_id).val();

    data = {action: 'send_message',
        user_id:user_id,
        message:message
    }
    jQuery.ajaxSetup({async:false});
    jQuery.post(adminAjaxUrl, data, function(response){
        location.reload();
    });
    $('html, body').animate({ scrollTop: 0 }, 'fast');

    return false;
}

function removeContract(){
    id = $(this).attr('id');

    data = {action: 'remove_contract',
        id:id
    }
    jQuery.ajaxSetup({async:false});
    jQuery.post(adminAjaxUrl, data, function(response){
        location.reload();
    });
    $('html, body').animate({ scrollTop: 0 }, 'fast');

    return false;
}


function payment(){
    $('div.alert-error').remove();
    
    id = $(this).attr('id');
    
    pay_main_permonth = $('input#pay-main-permonth-'+id).val().replace(/\s+/g, '');
    pay_percent_permonth = $('input#pay-percent-permonth-'+id).val().replace(/\s+/g, '');
    pay_rate = $('input#pay-rate-'+id).val().replace(/\s+/g, '');
    
    main_permonth = parseInt($('span.main_permonth_'+id).text());
    percent_permonth = parseInt($('span.percent_permonth_'+id).text());
    rate = parseInt($('span.rate_'+id).text());
    
    if( pay_main_permonth > main_permonth ||
            pay_percent_permonth > percent_permonth ||
                pay_rate > rate){
                    $('div.error-'+id).after('<div style="color: #b94a48;" class="alert-error"> * Неверно указана сумма платежа.</div>');
                    return false;        
        }

    data = {action: 'payment',
        id:id,
        pay_main_permonth:pay_main_permonth,
        pay_percent_permonth:pay_percent_permonth,
        pay_rate:pay_rate
    }
    jQuery.ajaxSetup({async:false});
    jQuery.post(adminAjaxUrl, data, function(response){
        location.reload();
    });
    $('html, body').animate({ scrollTop: 0 }, 'fast');

    return false;
}



function editProfile(){
    first_name = $('#firstName').val();
    last_name = $('#lastName').val();
    password = $('#password').val();

    data = {action: 'edit_profile',
        first_name:first_name,
        last_name:last_name,
        password:password
    }
    jQuery.ajaxSetup({async:false});
    jQuery.post(adminAjaxUrl, data, function(response){
        location.reload();
    });
    $('html, body').animate({ scrollTop: 0 }, 'fast');

    return false;
}


function addPersonalCabinet(){
    first_name = $('#firstName').text();
    last_name = $('#lastName').text();
    father_name = $('#fatherName').text();
    passport = $('#passport').text();
    email = $('#email').text();
    url = $(location).attr('href');
    loan_id = $('.loan-id').val();

    loanSumm = $('#loanSumm').text();
    
    belky = $('input[type="checkbox"]:checked').val();

    along = 'long';

    from = $('#from').val();
    to = $('#to').val();
    aim = $('#aim').val();
    percent = $('#percent').val();

    data = {action: 'add_personal_cabinet',
        first_name:first_name,
        last_name:last_name,
        father_name:father_name,
        passport:passport,
        email:email,
        url:url,
        loan_id:loan_id,

        loanSumm:loanSumm,
        belky:belky,
        along:along,
        from:from,
        to:to,
        aim:aim,
        percent:percent
    }
    jQuery.ajaxSetup({async:false});
    jQuery.post(adminAjaxUrl, data, function(response){
        if(response.success){
            $('div.remove').remove();
            $('div.after').after('<div class="alert alert-success">'+ response.message +'</div>');
        }else{
            $('div.alert-danger').remove();
            $('input#id').after('<div class="alert alert-danger">'+ response.message +'</div>');
        }
    });
    $('html, body').animate({ scrollTop: 0 }, 'fast');

    return false;
}



function exportToWordDoc(){
    first_name = $('#firstName').text();
    last_name = $('#lastName').text();
    father_name = $('#fatherName').text();
    passport = $('#passport').text();
    loan_id = $('.loan-id').val();



    data = {action: 'export_to_word_doc',
        first_name:first_name,
        last_name:last_name,
        father_name:father_name,
        passport:passport,
        loan_id:loan_id
    }
    jQuery.ajaxSetup({async:false});
    jQuery.post(adminAjaxUrl, data, function(response){
        title = response.title;
        url = response.url;
        window.location.href = url;
    });

    return false;
}



function changeStatus(){
    if(statusValidation()){
        status = $('input[type="radio"][name="status"]:checked').val();
        user_id = $('input[type="hidden"].user-id').val();
        loan_id = $('input[type="hidden"].loan-id').val();
        comment = $('.status-comment').val();

        data = {action: 'change_status',
            status:status,
            user_id:user_id,
            loan_id:loan_id,
            comment:comment
        }
        jQuery.ajaxSetup({async:false});
        jQuery.post(adminAjaxUrl, data, function(response){
    //        location.reload();
            window.location.href = 'http://localhost/behoof/src/?page_id=377';
        });
    }

    return false;
}


function addClassActive(){
    $('ul.navbar-nav > li').removeClass('active');
    $(this).addClass('active');
}

function redirectPage(){
    loan_id = $(this).attr('id');
    id = $(this).attr('id');
    var data = {action: 'redirect',
        id:id,
        loan_id:loan_id
    }
    jQuery.ajaxSetup({async:false});
    jQuery.post(adminAjaxUrl, data, function(response){
        page_url = response.url;
        loan_id = response.loan_id;
        window.location.href = page_url + '&loanId=' + loan_id;
    });

    return false;
}

function handlingRequest(){
    if(formValidation()){
        var query = document.location.toString().replace(/^.*?\?/,'').split('&');

        loan_id = query[1].split('=')[1];
        
        str = $(location).attr('href');
        url = str.slice(0, str.indexOf('&')-1);

    // проверяем данные
    // Также нужно как-то передать информацию о выбранном кредите
        if($('#loanterm').is('input')){
            var loanterm = $('#loanterm').val();
        }else{
            var loanterm = $('#loanterm').text();
        }    
        var $loanData = {
            checkRole:$('input[type="hidden"]#check-role').val(),
            limit:$('span#limit').text(),
            term:$('span#term').text(),
            loanSumm: $('input#loansumm').val(),
            loanTerm: loanterm,
            loanPercentage: $('span#loanpercentage').text()
        };

        var $personalData = {
            lastName: $('input#lastname').val(),
            firstName: $('input#firstname').val(),
            fatherName: $('input#fathername').val(),
            mobilePhone: $('input#mobilephone').val(),
            email: $('input#email').val()
        };

        var $passportData = {
            passport: $('input#serialnumber').val(),
            issuedBy: $('input#issuedby').val(),
            issuedDate: $('input#issueddate').val(),
            birthDate: $('input#birthdate').val(),
            birthPlace: $('input#birthplace').val(),
            index: $('input#homeindex').val(),
            region: $('input#homeregion').val(),
            city: $('input#homecity').val(),
            address: $('input#homeaddress').val(),
            phoneNumber: $('input#homephonenumber').val()
        };

        var $workInformation = {
            typeOfEmployment: $('select#type-of-empl option:selected').val(),
            typeOfEmploymentText: $('select#type-of-empl option:selected').text(),
            nameOfOrganisation: $('input#workname').val(),
            phoneNumber: $('input#workphonenumber').val(),
            heldPosition: $('input#workposition').val(),
            workDuration: $('input#workduration').val(),
            index: $('input#workindex').val(),
            region: $('input#workregion').val(),
            city: $('input#workcity').val(),
            address: $('input#workaddress').val()
        };

        var $morePersonalData = {
            children: $('select#children option:selected').val(),
            childrenText: $('select#children option:selected').text(),
            status: $('select#status option:selected').val(),
            statusText: $('select#status option:selected').text(),
            education: $('select#education option:selected').val(),
            educationText: $('select#education option:selected').text(),
            personalCar: $('select#personal-car option:selected').val(),
            personalCarText: $('select#personal-car option:selected').text(),
            personalIncome: $('input#personalincome').val(),
            amount: $('input#amount').val(),
            payments: $('input#otherpayments').val()
        };

        var $bailslData = {
            bailLastName1: $('input#baillastname1').val(),
            bailLastName2: $('input#baillastname2').val(),

            bailFirstName1: $('input#bailfirstname1').val(),
            bailFirstName2: $('input#bailfirstname2').val(),

            bailFatherName1: $('input#bailfathername1').val(),
            bailFatherName2: $('input#bailfathername2').val(),

            bailAddress1: $('input#bailaddress1').val(),
            bailAddress2: $('input#bailaddress1').val(),

            bailMobilePhone1: $('input#bailmobilephone1').val(),
            bailMobilePhone2: $('input#bailmobilephone2').val(),

            bailHomePhone1: $('input#bailhomephone1').val(),
            bailHomePhone2: $('input#bailhomephone2').val()
        };


        var data = {action: 'handling',
            loanData:$loanData,
            personalData:$personalData,
            passportData:$passportData,
            workInformation:$workInformation,
            morePersonalData:$morePersonalData,
            bailsData:$bailslData,
            url:url,
            loan_id:loan_id
        }
        jQuery.ajaxSetup({async:false});
        jQuery.post(adminAjaxUrl, data,function(response){
            if(response.success){
                $('div.alert-danger').remove();
                $('div.wrap').remove();
                $('div.remove').remove();
                $('input#id').after('<div class="alert alert-success">'+ response.message +'</div>');
            }else{
                $('div.alert-danger').remove();
                $('input#id').after('<div class="alert alert-danger">'+ response.message +'</div>');
            }
        });
        $('html, body').animate({ scrollTop: 0 }, 'fast');
        
    }else{
        
    }
    return false;
}


function removeApproveRequest(e,class_name,hand){
    if(!class_name){
        class_name = $(this).attr('class');
        class_name = class_name.split(" ");
        class_name = class_name[class_name.length-1];
    }

    if(class_name == 'no'){
        message = "Вы уверены в том, что хотите удалить эту заявку?"
        $func = 'remove';
    }
    if(class_name == 'yes'){
        message = "Вы уверены в том, что хотите подтвердить эту заявку?"
        $func = 'approve';
    }
    if (hand || confirm(message)) {
        url = $(location).attr('href');
        user_email = $('.user-email').val();
        var data = {action: 'remove_approve_request',
            url:url,
            user_email:user_email,
            func:$func
        }
        jQuery.ajaxSetup({async:false});
        jQuery.post(adminAjaxUrl, data,function(response){
            if(response.success){
                $('div.remove').remove();
                $('div.after').after('<div class="alert alert-success">'+ response.message +'</div>');
            }else{
                $('div.alert-danger').remove();
                $('input#id').after('<div class="alert alert-danger">'+ response.message +'</div>');
            }
        });
        $('html, body').animate({ scrollTop: 0 }, 'fast');
    }
    return false;
}

function formValidation() {
    $('div.alert-danger').remove();
    $('input[type="text"]').css('border-color','');
    required = false;
    curYear = new Date().getFullYear();
    
    var limit = $('span#limit').text();
    var term = $('span#term').text();

    
    var loansumm = $('input#loansumm').val().replace(/\s/g, '');;
    if($('#loanterm').is('input')){
        var loanterm = $('#loanterm').val();
    }else{
        var loanterm = $('#loanterm').text();
    }    
    
    var lastname = $('input#lastname').val();
    var firstname = $('input#firstname').val();
    var fathername = $('input#fathername').val();
    var mobilephone = $('input#mobilephone').val();
    var email = $('input#email').val();
    
    
    var serialnumber = $('input#serialnumber').val();
    var birthdate = $('input#birthdate').val();
    date = birthdate.split('/');
    
    var homephonenumber = $('input#homephonenumber').val();
    var personalincome = $('input#personalincome').val();
    
    var issueddate = $('input#issueddate').val();
    
    
    $('input.required').each(function(ind,val){
        if ($(val).val()=="")
        {
            $(val).css('border-color','#b94a48');            
            required = true;
        }        
    });
    if(required){
        $('input#id').after('<div class="alert alert-danger">Все обязательные поля должны быть заполнены!</div>');
        $('html, body').animate({ scrollTop: 0 }, 'fast');
        return false;
    }

    limit = limit.replace(/^[^\d]+|[^\d]+$|\s/g, "").match(/\d+/g);
    
    term = term.replace(/^[^\d]+|[^\d]+$|\s/g, "").match(/\d+/g);
    
    if(limit.length > 1){
        if(parseInt(loansumm) < parseInt(limit[0]) || parseInt(loansumm) > parseInt(limit[1])){
            $('input#id').after('<div class="alert alert-danger">Вы неверно указали предполагаемую сумму кредита.</div>');
            $('html, body').animate({ scrollTop: 0 }, 'fast');
            return false;            
        }
    }
    if(term.length > 1){
        if(parseInt(loanterm) < parseInt(term[0]) || parseInt(loanterm) > parseInt(term[1])){
            $('input#id').after('<div class="alert alert-danger">Вы неверно указали предполагаемый срок кредита.</div>');
            $('html, body').animate({ scrollTop: 0 }, 'fast');
            return false;            
        }
    }    
    
    

    var nreg = /^[а-я]+$/i;
    if(!nreg.test(lastname) || !nreg.test(firstname) || !nreg.test(fathername)){
        $('input#id').after('<div class="alert alert-danger">Неверно введены данные ФИО (разрешены только русские буквы).</div>');
        $('html, body').animate({ scrollTop: 0 }, 'fast');
        return false;
    }      
    
    var mreg = /^[0-9]{2}\s[0-9]{3}\s[0-9]{2}\s[0-9]{2}$/i;
    if(!mreg.test(mobilephone)){
        $('input#id').after('<div class="alert alert-danger">Неверный номер мобильного телефона.</div>');
        $('html, body').animate({ scrollTop: 0 }, 'fast');
        return false;
    }    
    
    var ereg = /^\w+@\w+\.\w{2,4}$/i;
    if (!ereg.test(email))
    {
        $('input#id').after('<div class="alert alert-danger">Неверный адрес электронной почты.</div>');
        $('html, body').animate({ scrollTop: 0 }, 'fast');
        return false;
    }
    
//    var sreg = /[a-zA-Z]{2}\s\d{7}$/i;
    var sreg = /^(?:AB|BM|HB|KH|MP|MH|MC|KB|PP)\s[0-9]{7}$/;
    
    if (!sreg.test(serialnumber))
    {
        $('input#id').after('<div class="alert alert-danger">Неверно указана серия пасспорта.</div>');
        $('html, body').animate({ scrollTop: 0 }, 'fast');
        return false;
    }    
    
    var dreg = /^((?:19|20)\d\d)[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$/i;
    if (date[0]<1940 || (curYear-date[0])<18 )
    {
        $('input#id').after('<div class="alert alert-danger">Указана неверная дата рождения. Либо Ваш возраст не входит в категорию платежеспособного возраста.</div>');
        $('html, body').animate({ scrollTop: 0 }, 'fast');
        return false;
    }else if(!dreg.test(birthdate)){
        $('input#id').after('<div class="alert alert-danger">Указана неверная дата рождения.</div>');
        $('html, body').animate({ scrollTop: 0 }, 'fast');
        return false;        
    }  
    
    var ireg = /^((?:19|20)\d\d)[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$/i;
    if (!ireg.test(issueddate) )
    {
        $('input#id').after('<div class="alert alert-danger">Указана неверная дата выдачи пасспорта.</div>');
        $('html, body').animate({ scrollTop: 0 }, 'fast');
        return false;
    }    

    var preg = /^[0-9]+/;
    if (!preg.test(personalincome))
    {
        $('input#id').after('<div class="alert alert-danger">Неверно указан  персональный доход.</div>');
        $('html, body').animate({ scrollTop: 0 }, 'fast');
        return false;
    }     
    
    var hreg =  /^[0-9]{3}\s[0-9]{2}\s[0-9]{2}$/;
    if (!hreg.test(homephonenumber))
    {
        $('input#id').after('<div class="alert alert-danger">Неверно указан домашний телефон.</div>');
        $('html, body').animate({ scrollTop: 0 }, 'fast');
        return false;
    }     
    
    
    return true;
}

function statusValidation(){
    $('div.alert-danger').remove();
    comment = $('textarea.status-comment').val();
    
    if(comment == ""){
        $('textarea.status-comment').before('<div class="alert alert-danger">Для изменения статуса, объясните причину.</div>');
        return false;
    }   
    
    return true;
}