function monthStatistics(ativeMonthContracts, addedMonthContracts, overduedMonthContracts, extinguishedMonthContracts, max ){
    lastMonth = $("#last-month").get(0).getContext("2d");
    Bar = new Chart(lastMonth);
   
    monthNames = [ "Январья", "Февраль", "Март", "Апрель", "Май", "Июнь",
    "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" ];
    d = new Date();
    $monthName = monthNames[d.getMonth()];
    
    data = {
	labels : [$monthName],
	datasets : [
		{
			fillColor : "rgba(151,187,205,0.5)",
			strokeColor : "rgba(151,187,205,1)",
            		data : [ativeMonthContracts]
		},    
		{
			fillColor : "rgba(4, 163, 156, 0.3)",
			strokeColor : "rgba(4, 163, 156, 0.7)",
            		data : [addedMonthContracts]
		},  
		{
			fillColor : "rgba(178, 34, 34, 0.3)",
			strokeColor : "rgba(178, 34, 34, 1)",
            		data : [overduedMonthContracts]
		},                
		{
			fillColor : "rgba(220,220,220,0.5)",
			strokeColor : "rgba(220,220,220,1)",
			data : [extinguishedMonthContracts]
		}
    	]
    }

    Bar.defaults = {

            //Boolean - If we show the scale above the chart data			
            scaleOverlay : false,

            //Boolean - If we want to override with a hard coded scale
            scaleOverride : false,

            //** Required if scaleOverride is true **
            //Number - The number of steps in a hard coded scale
            scaleSteps : null,
            //Number - The value jump in the hard coded scale
            scaleStepWidth : null,
            //Number - The scale starting value
            scaleStartValue : null,

            //String - Colour of the scale line	
            scaleLineColor : "rgba(0,0,0,.1)",

            //Number - Pixel width of the scale line	
            scaleLineWidth : 1,

            //Boolean - Whether to show labels on the scale	
            scaleShowLabels : true,

            //Interpolated JS string - can access value
            scaleLabel : "<%=value%>",

            //String - Scale label font declaration for the scale label
            scaleFontFamily : "'Arial'",

            //Number - Scale label font size in pixels	
            scaleFontSize : 12,

            //String - Scale label font weight style	
            scaleFontStyle : "normal",

            //String - Scale label font colour	
            scaleFontColor : "#666",	

            ///Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines : true,

            //String - Colour of the grid lines
            scaleGridLineColor : "rgba(0,0,0,.05)",

            //Number - Width of the grid lines
            scaleGridLineWidth : 1,	

            //Boolean - If there is a stroke on each bar	
            barShowStroke : true,

            //Number - Pixel width of the bar stroke	
            barStrokeWidth : 1,

            //Number - Spacing between each of the X value sets
            barValueSpacing : 5,

            //Number - Spacing between data sets within X values
            barDatasetSpacing : 10,

            //Boolean - Whether to animate the chart
            animation : true,

            //Number - Number of animation steps
            animationSteps : 100,

            //String - Animation easing effect
            animationEasing : "easeOutQuart",

            //Function - Fires when the animation is complete
            onAnimationComplete : null

    }
    
    
    options = Bar.defaults;   
    
    new Chart(lastMonth).Bar(data,options);
    
}

function yearStatistics(addedYearContracts,extinguishedYearContracts){
    
     addedArray = addedYearContracts.split(",");
     extinguishedArray = extinguishedYearContracts.split(",");
     
     lastYear = $("#last-year").get(0).getContext("2d");
     Bar = new Chart(lastYear);
   
     monthNames = [ "Январья", "Февраль", "Март", "Апрель", "Май", "Июнь",
    "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" ];
     d = new Date();
    $monthName = monthNames[d.getMonth()];
    
 data = {
	labels : ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь",
            "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь"],
	datasets : [
		{
			fillColor : "rgba(4, 163, 156, 0.3)",
			strokeColor : "rgba(4, 163, 156, 0.7)",
			data : [addedArray[0],addedArray[1],addedArray[2],addedArray[3],addedArray[4],addedArray[5],addedArray[6],addedArray[7],addedArray[8],addedArray[9],addedArray[10]]
		},
		{
			fillColor : "rgba(220,220,220,0.6)",
			strokeColor : "rgba(220,220,220,1)",
			data : [extinguishedArray[0],extinguishedArray[1],extinguishedArray[2],extinguishedArray[3],extinguishedArray[4],extinguishedArray[5],extinguishedArray[6],extinguishedArray[7],extinguishedArray[8],extinguishedArray[9],extinguishedArray[10]]
		}
	]
}

    Bar.defaults = {

            //Boolean - If we show the scale above the chart data			
            scaleOverlay : false,

            //Boolean - If we want to override with a hard coded scale
            scaleOverride : false,

            //** Required if scaleOverride is true **
            //Number - The number of steps in a hard coded scale
            scaleSteps : null,
            //Number - The value jump in the hard coded scale
            scaleStepWidth : null,
            //Number - The scale starting value
            scaleStartValue : null,

            //String - Colour of the scale line	
            scaleLineColor : "rgba(0,0,0,.1)",

            //Number - Pixel width of the scale line	
            scaleLineWidth : 1,

            //Boolean - Whether to show labels on the scale	
            scaleShowLabels : true,

            //Interpolated JS string - can access value
            scaleLabel : "<%=value%>",

            //String - Scale label font declaration for the scale label
            scaleFontFamily : "'Arial'",

            //Number - Scale label font size in pixels	
            scaleFontSize : 12,

            //String - Scale label font weight style	
            scaleFontStyle : "normal",

            //String - Scale label font colour	
            scaleFontColor : "#666",	

            ///Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines : true,

            //String - Colour of the grid lines
            scaleGridLineColor : "rgba(0,0,0,.05)",

            //Number - Width of the grid lines
            scaleGridLineWidth : 1,	

            //Boolean - If there is a stroke on each bar	
            barShowStroke : true,

            //Number - Pixel width of the bar stroke	
            barStrokeWidth : 1,

            //Number - Spacing between each of the X value sets
            barValueSpacing : 10,

            //Number - Spacing between data sets within X values
            barDatasetSpacing : 1,

            //Boolean - Whether to animate the chart
            animation : true,

            //Number - Number of animation steps
            animationSteps : 100,

            //String - Animation easing effect
            animationEasing : "easeOutQuart",

            //Function - Fires when the animation is complete
            onAnimationComplete : null

    }
    
    
    options = Bar.defaults;   
    
    new Chart(lastYear).Bar(data,options);
    
}

function allStatistics(addedContracts,extinguishedContracts){
    
     all = $("#all").get(0).getContext("2d");
     Pie = new Chart(all);
     
     var data = [
	{
		value: extinguishedContracts,
                color : "rgba(220,220,220,0.9)"		
	},
	{
		value : addedContracts,
		color:"rgba(4, 163, 156, 0.7)"
	}			
     ]
     
     Pie.defaults = {
	//Boolean - Whether we should show a stroke on each segment
	segmentShowStroke : true,
	
	//String - The colour of each segment stroke
	segmentStrokeColor : "#fff",
	
	//Number - The width of each segment stroke
	segmentStrokeWidth : 2,
	
	//Boolean - Whether we should animate the chart	
	animation : true,
	
	//Number - Amount of animation steps
	animationSteps : 100,
	
	//String - Animation easing effect
	animationEasing : "easeOutBounce",
	
	//Boolean - Whether we animate the rotation of the Pie
	animateRotate : true,

	//Boolean - Whether we animate scaling the Pie from the centre
	animateScale : false,
	
	//Function - Will fire on animation completion.
	onAnimationComplete : null
    }
    
    options = Pie.defaults;   
    
    new Chart(all).Pie(data,options);
    
}