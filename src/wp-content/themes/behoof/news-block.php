<?php
$category = get_category_by_slug( 'news' );
$category_id = $category->cat_ID;
$news = query_posts('cat='.$category_id);
foreach($news as $key=>$the_news): ?>
    <?php  ?>
    <?php
    $the_news_id = $the_news->ID;
    $the_news_url = get_permalink( $the_news_id );
    $the_news_title = implode(' ', array_slice(explode(' ', $the_news->post_title), 0, 4));
    $the_news_content = $the_news->post_content;
    $the_news_body = implode(' ', array_slice(explode(' ', $the_news_content), 0, 20));
    ?>
    <?php if($key < 9): ?>
        <div class="col-6 col-sm-6 col-lg-4">
            <h2><?php //echo $the_news_title; ?></h2>
            <p><?php echo $the_news_body; ?></p>
            <p><a class="btn btn-default" href="<?php echo $the_news_url; ?>">Читать далее &raquo;</a></p>
        </div><!--/span-->
    <?php endif; ?>
<?php endforeach; ?>