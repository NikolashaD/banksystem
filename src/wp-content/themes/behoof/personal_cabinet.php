<?php get_header(); ?>

<?php

    $category = get_category_by_slug( 'news' );
    $category_id = $category->cat_ID;
    $news = query_posts('cat='.$category_id);

    $user_id = get_current_user_id();
    $user = get_userdata( $user_id );
    $user_name = get_userdata( $user_id )->display_name;
    $avatar = get_avatar($user_id,170);

    $events = get_clients_events($user_id);

?>

<div class="container">
    <div class="header">
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav profile-nav">
                <li><a href="#"></a></li>
            </ul>
        </div><!-- /.nav-collapse -->
        <ul class="nav nav-pills pull-right">
            <li><a href="<?=PERSONAL_CABINET;?>&loanslist=true">Мои кредиты</a></li>
            <li><a href="<?=PERSONAL_CABINET;?>&calendar=true">Календарь</a></li>
            <li><a href="<?=wp_logout_url('/');?>">Выход</a></li>
        </ul>
        <a class="navbar-brand" href="<?=PERSONAL_CABINET;?>"><?=$avatar;?></a>

    </div>

    <div class="jumbotron">
        <h3 class="text-muted">Добро пожаловать, <?=$user_name;?></h3>
        <img class="img-thumbnail" src=""><img class="img-thumbnail" src=""><img class="img-thumbnail" src=""><img class="img-thumbnail" src=""><img class="img-thumbnail" src=""><img class="img-thumbnail" src=""><img class="img-thumbnail" src=""><img class="img-thumbnail" src=""><img class="img-thumbnail" src=""><img class="img-thumbnail" src=""><img class="img-thumbnail" src=""><img class="img-thumbnail" src="">
    </div>

    <div class="row marketing">
        <?php if(isset($_GET['calendar']) && !empty($_GET['calendar'])): ?>
            <div class="col-lg-6">
                <h2 style="margin-left:150px;" class="text-muted">Расписание</h2>
                <div id="square-curday"></div> <span style="padding-left: 10px;">- отмечен настоящий день</span>
                <br><br>
                <div id="square-event"></div> <span style="padding-left: 10px;">- отмечены дни платежей (при наведении отображается полная информация)</span>
                <div class="cal" ><?php echo calendar(); ?></div>
            </div>
            <div class="col-lg-6">
                <h2 style="margin-left:50px;" class="text-muted">Список выплат по кредитам</h2>
                <span>За более подробной информацией по кредитам и выплатам обращайтесь к оператором нашего банка.</span>
                <table style="margin-top:70px;" class="table">
                    <tr>
                        <th>Платеж по кредиту</th>
                        <th>Дата</th>
                        <th>Сумма платежа/мес</th>
<!--                        <th>Проценты/мес</th>-->
                        <th>Кол-во взносов</th>
                    </tr>
                    <?php if (!empty($events)): ?>
                        <?php foreach ($events as $item): ?>
                            <tr>
                                <td width="200px"><?php echo $item['title']; ?></td>
                                <td width="100px"><?php echo $item['start_date']; ?></td>
                                <td>Основной:<br> <?php echo $item['main_per_month']; ?> <br> Проценты:<br> <?php echo $item['percent_per_month']; ?> </td>
<!--                                <td>--><?php //echo $item['per_month']; ?><!--</td>-->
                                <td><?php echo $item['col_months']; ?></td>
                             </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </table>


            </div>
        <?php elseif(isset($_GET['loanslist']) && !empty($_GET['loanslist'])): ?>

           <?php
                $category = get_category_by_slug( RESPONSES );
                $category_id = $category->cat_ID;
                $posts = query_posts('cat='.$category_id.'&author='.$user_id);              
           ?>


            <table class="table">
                <tr>
                    <th>Название</th>
                    <th>На кого</th>
                    <th>Дата подписания договора</th>
                    <th>Мои данные</th>
                    <th>Узнать больше</th>
                </tr>
                <?php if (!empty($posts)): ?>
                    <?php foreach ($posts as $item): ?>
                <?php
                    global $wpdb;
                    $loan_id = get_page_by_title( wp_get_post_tags($item->ID)[0]->name, $output, 'post' )->ID;
                    
                    $event_data = $wpdb->get_results("SELECT * FROM ".WP_CALENDAR_TABLE." WHERE loan_id=".$loan_id." AND event_author=".$user_id." ");
                   
                    $loan_summ = $event_data[0]->event_summ;
                    $percent = $event_data[0]->event_percentage;
                    $term = $event_data[0]->event_col_months/12;      
                    $currency = $event_data[0]->event_currency; 
                    
                    $payment_arr = get_all_payment_data($term, $loan_summ, $percent);
        
                    $total_summ = $payment_arr['total_summ'];
                    $percent_per_month = $payment_arr['percent_per_month'];
                    $main_summ_per_month = $payment_arr['main_summ_per_month']; 
                
                ?>
                        <tr class="line-one">
                            <td width="300px"><?php echo wp_get_post_tags($item->ID)[0]->name; ?></td>
                            <td><?php echo $item->post_title; ?></td>
                            <td><?php echo $item->post_date ?></td>
                            <td><a id="<?=$item->ID;?>" href="<?php echo get_permalink( $item->ID ); ?>">Мои данные</a></td>
                            <td><a id="<?=$item->ID;?>" href="<?php echo get_permalink( get_page_by_title( wp_get_post_tags($item->ID)[0]->name, $output, 'post' )->ID ); ?>">Узнать больше   <span class="glyphicon glyphicon-list-alt"></span></a></td>
                        </tr>
                        
                        <tr class="line-two" style="display:none;">
                            <td colspan="8">
                                <table class="table table-striped">
                                      <tr class="warning">
                                          <th>Данные:</th>
                                          <th>Сумма кредита</th>
                                          <th>Процентная ставка</th>
                                          <th>На срок</th>
                                          <th>Денежная единица</th>
                                      </tr> 
                                      <tr>
                                          <td></td>
                                          <td><?=$loan_summ;?></td>
                                          <td><?=$percent;?> % годовых</td>
                                          <td><?=$term;?> года/лет</td>
                                          <td><?=$currency;?></td>
                                      </tr>             
                                      <tr class="warning">
                                          <th>График выплат</th>
                                          <th>Полная сумма выплат</th>
                                          <th>Выплачено процентов</th>
                                          <th>Основной платеж</th>
                                          <th>Выплачено за услуги</th>
                                      </tr>
                                      <tr>
                                          <td>За весь период выплат</td>
                                          <td><?=$total_summ;?></td>
                                          <td><?=$percent_per_month*$term*12;?></td>
                                          <td><?=$main_summ_per_month*$term*12;?></td>
                                          <td>0.00</td>
                                      </tr>   
                                      <tr>
                                          <td>Ежемесячно <br>(<?=$term*12;?> мес.)</td>
                                          <td><?=$percent_per_month+$main_summ_per_month; ?></td>
                                          <td><?=$percent_per_month;?></td>
                                          <td><?=$main_summ_per_month;?></td>
                                          <td>0.00</td>
                                      </tr>              
                                </table>                           
                            </td>
                        </tr>                         
                        
                    <?php endforeach; ?>
                <?php endif; ?>
            </table>



        <?php else: ?>
                <div class="col-lg-6">
                <?php for($i=0; $i<6; $i++): ?>
                    <?php
                        $the_news_id = $news[$i]->ID;
                        $the_news_url = get_permalink( $the_news_id );
                        $the_news_title = implode(' ', array_slice(explode(' ', $news[$i]->post_title), 0, 4));
                        $the_news_content = $news[$i]->post_content;
                        $the_news_body = implode(' ', array_slice(explode(' ', $the_news_content), 0, 30));
                    ?>
                    <a href="<?=$the_news_url;?>"><h4><?=$the_news_title; ?></h4></a>
                    <p><?=$the_news_body;?>...</p>
                    <br>
                <?php endfor; ?>
                </div>

                <div class="col-lg-6">
                    <h2 style="margin-left:150px;" class="text-muted">Расписание</h2>


                    <div style="margin-left:150px;" id="square-curday"></div> <span style="padding-left: 10px;">- отмечен настоящий день</span>
                    <br><br>
                    <div style="margin-left:150px;" id="square-event"></div> <span style="padding-left: 10px;">- отмечены дни платежей</span>

                    <div style="margin-left:150px;"><?php echo minical(); ?></div>
                </div>

                <div class="col-lg-6">
                    <h2 style="margin-top:50px; margin-left:150px;" class="text-muted">Изменить данные о себе</h2>
                    <br><br>
                    <div style=" margin-left:150px; width:350px;" role="form">
                        <div class="form-group">
                            <label for="email1">Адрес электронной почты</label>
                            <input type="email" class="form-control" id="email1" disabled value="<?=$user->user_email;?>">
                        </div>
                        <div class="form-group">
                            <label for="login">Логин</label>
                            <input type="text" class="form-control" id="login" disabled value="<?=$user->user_login;?>">
                        </div>
                        <div class="form-group">
                            <label for="password">Пароль</label>
                            <input type="password" class="form-control" id="password" placeholder="Пароль">
                        </div>
                        <div class="form-group">
                            <label for="firstName">Имя</label>
                            <input type="text" class="form-control" id="firstName"  value="<?=$user->first_name;?>">
                        </div>
                        <div class="form-group">
                            <label for="lastNames">Фамилия</label>
                            <input type="text" class="form-control" id="lastName"  value="<?=$user->last_name;?>">
                        </div>

                        <button type="submit" class="btn btn-default edit-profile">Сохранить изменения</button>
                    </div>

                </div>
        <?php endif; ?>

    </div><!--/row-->

    <?php get_footer(); ?>

</div><!--/.container-->
