<?php get_header(); ?>

<?php
    global $wpdb;
    $args = array(
        'role'         => 'client',
        'meta_key'     => '',
        'meta_value'   => '',
        'meta_compare' => '',
        'meta_query'   => array(),
        'include'      => array(),
        'exclude'      => array(),
        'orderby'      => 'login',
        'order'        => 'ASC',
        'offset'       => '',
        'search'       => '',
        'number'       => '',
        'count_total'  => false,
        'fields'       => 'all',
        'who'          => ''
    );

    $clients = get_users( $args );





    $category = get_category_by_slug( 'responses' );
    $category_id = $category->cat_ID;
    
    $page_num = get_cur_page_num();
    $contracts = get_pagination_data($category_id, $page_num);
//    $contracts = query_posts('cat='.$category_id);
    
    $role = get_user_role();
    
    $sql = 'SELECT last_id FROM wp_blacklist';
    $black_ids = $wpdb->get_results($sql);
    $black_array = array();
    foreach($black_ids as $key=>$bi){
        $black_array[] =  $bi->last_id;
    }   


//    echo '<br><br><br><br>';
//    var_dump($percent_per_month );
?>


<div class="container">
    <div class="header">
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav profile-nav">
                <li><a href="#"></a></li>
            </ul>
        </div><!-- /.nav-collapse -->
        <ul class="nav nav-pills pull-right">
            <li><a href="<?=PERSONAL_ADMIN_CABINET;?>">Главная</a></li>
            <?php if($role == ADMIN || $role == 'head'): ?>
                <li><a href="<?=PERSONAL_ADMIN_CABINET;?>&statistics=true">Статистика</a></li>
            <?php endif;?>
            <li><a href="<?=PERSONAL_ADMIN_CABINET;?>&clients=true">Клиенты</a></li>
            <?php if($role == 'operator' || $role == ADMIN): ?>
                <li><a href="<?=PERSONAL_ADMIN_CABINET;?>&contracts=true">Договора</a></li>
            <?php endif; ?>
            <li><a href="<?=wp_logout_url('/');?>">Выход</a></li>
        </ul>
    </div>



    <div class="row marketing">       
        <?php if(isset($_GET['clients']) && !empty($_GET['clients'])): ?>
            <div >
                <h2 style="margin-left:50px;" class="text-muted">Список всех клиентов банка</h2>
                <br><br>
                <div style="margin-left: 50px; background-color: #f2dede;" id="square-event"></div> <span style="padding-left: 10px;">- отмечены клиенты, которые находятся в <span style="font-wieght:bold;">черном</span> списке компании</span>      
                <table style="margin-top:70px;" class="table">
                    <tr>
                        <th style="vertical-align: middle;">№</th>
                        <th style="vertical-align: middle;">Клиент</th>
                        <th style="vertical-align: middle;">Адрес электронной почты</th>
                        <th style="vertical-align: middle;">Дата регистрации</th>
                        <th style="vertical-align: middle;">Текст оповещения</th>
                        <th style="vertical-align: middle;">Оповестить</th>
                        <?php if($role == 'security'): ?>
                            <th style="vertical-align: middle;">Бан</th>
                        <?php endif; ?>
                    </tr>
                    <?php if (!empty($clients)): ?>
                        <?php foreach ($clients as $key=>$client): ?>
                        <?php 
                            $black_class = '';
                            $blacklist = false;
                            if(in_array($client->ID, $black_array)) { $black_class = 'danger'; $blacklist = true; }; 
                         ?>
                            <tr class="<?=$black_class;?>">
                                <td style="vertical-align: middle;" width="20px"><?=$key+1; ?></td>
                                <td style="vertical-align: middle;" width="180px"><?=$client->user_nicename.'<br>'.$client->display_name; ?></td>
                                <td style="vertical-align: middle;" width="200px"><?=$client->user_email;?></td>
                                <td style="vertical-align: middle;" width="150px"><?=date('Y-m-d', strtotime($client->user_registered)); ?></td>
                                <td><textarea class="form-control status-comment" style="width:400px;" id="<?=$client->ID;?>"></textarea></td>
                                <td style="vertical-align: middle;"><a id="<?=$client->ID;?>" class="send-message" href="<?php echo ''; ?>">Оповестить &nbsp<span class="glyphicon glyphicon-envelope"></span></a></td>
                                <?php if($role == 'security'): ?>
                                    <td style="vertical-align: middle;"><?php if(!$blacklist): ?><a style="color:black;" id="<?=$client->ID;?>" class="ban-user" href="<?php echo ''; ?>">Бан &nbsp<span class="glyphicon glyphicon-fire"></span></a><?php endif; ?></td>
                                <?php endif; ?>                                
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </table>
            </div>
        <?php endif;?>
        
        
        <?php if(isset($_GET['contracts']) && !empty($_GET['contracts'])): ?>
            <div >
                <h2 style="margin-left:50px;" class="text-muted">Список всех заключенных договоров банка</h2>
                <br>
                <div style="margin-left: 50px;"   id="square-white"></div> <span style="padding-left: 10px;">- отмечены кредиты, которые в текущем месяце уже были оплачены</span>
                <br><br>
                <div style="margin-left: 50px; background-color: #fcf8e3;" id="square-event"></div> <span style="padding-left: 10px;">- отмечены кредиты, которые следует оплатить в текущем месяце</span>
                <br><br>
                <div style="margin-left: 50px; background-color: #f2dede;" id="square-event"></div> <span style="padding-left: 10px;">- отмечены кредиты, платежи по которым просрочены</span>
                <br><br>
                <div style="margin-left: 50px;" id="square-grey"></div> <span style="padding-left: 10px;">- отмечены кредиты, платежи по которым полностью погашены</span>

                <table style="margin-top:70px;" class="table">
                    <tr class="grey">
                        <th>№</th>
                        <th>Название кредита</th>
                        <th>Клиент</th>
                        <th>Дата заключения</th>
                        <th>Дата последнего платежа</th>
                        <th>Кол-во взносов</th>
                        <th>Возможность досрочного погашения</th>
                    </tr>
                    <?php if (!empty($contracts)): ?>
                        <?php foreach ($contracts as $key=>$contract): ?>
                            <?php
                            if($contract->post_excerpt == 'Approved'): ?>
                                <?php
                                $user = get_user_by( 'login', explode(' ',$contract->post_title)[1]);
                                
                                $user_name = $user->user_login. '<br>' .$user->display_name;

                                $post_name = $contract->post_name;
                                $loan_id = explode('-',$post_name)[2];
                                $loan_title = get_post($loan_id)->post_title;

                                $events = get_clients_events($user->ID);
                                $main_per_month = '';
                                $percent_per_month = '';
                                $col_months = '';
                                if($events){
                                    foreach($events as $event){
                                        if(trim($event['title']) == trim($loan_title)){
                                            $main_per_month = $event['main_per_month'];
                                            $percent_per_month = $event['percent_per_month'];
                                            $col_months = $event['col_months'];
                                            $event_ID = $event['id'];
                                            $event_start_date = $event['start_date'];
                                            
                                            $loan_summ = $event['loan_summ'];
                                            $loan_currency = $event['loan_currency'];
                                            $loan_all_summ = $event['loan_all_summ'];
                                            $loan_percentage = $event['loan_percentage'];
                                            
                                            $main_per_month_part = $event['main_per_month_part'];
                                            $percent_per_month_part = $event['percent_per_month_part'];
                                            $rate_part = $event['rate_part'];
                                        }
                                    }                                    
                                }
                            
                            $pay_year_month = explode('-', $event_start_date)[0].'-'.explode('-', $event_start_date)[1];
                                
                            $cur_month = date('m');
                            $pay_month = explode('-', $event_start_date)[1];
                            
                            $cur_day = date('d');
                            $pay_day = explode('-', $event_start_date)[2];
                            
                            $active = true;
                            $canceled = false;
                            $tr_class = '';


                            if($pay_year_month == date('Y-m')){
                                $tr_class = '';
                                $active = false;
                            }
                            else{
                                $months_count = (int)abs((strtotime(date('Y-m')) - strtotime($pay_year_month))/(60*60*24*30));
                                if( $months_count > 1 ){
                                    $tr_class = "danger";
                                    $active = true;
                                    // считать пеню
                                    $days = (strtotime(date('Y-m-d')) - strtotime($event_start_date)) / (24 * 3600);
                                    $fine_rate = preg_replace('/\%/', '', get_field('fine_rate',$loan_id));
                                    $rate = get_rate_data($main_per_month, $fine_rate,$months_count);                                     
                                }else{
                                    $days = (date('d') - $pay_day);
                                    if($days < 0){
                                        $tr_class = "";
                                        $active = true;
                                    }else{
                                        $tr_class = 'warning';
                                        $active = true;              
                                        // считать пеню
                                        $fine_rate = preg_replace('/\%/', '', get_field('fine_rate',$loan_id));
                                        $rate = (int)($main_per_month * $days * $fine_rate / 100);                              
                                    }
                                }
                            }                            
                            
                            if($col_months <= 0){
                                $tr_class = 'canceled';
                                $canceled = true;
                            }
                            if(is_null($months_count)){
                                if( ($main_per_month - $main_per_month_part) == 0 &&
                                      ($percent_per_month - $percent_per_month_part) == 0 &&
                                        ($rate - $rate_part) == 0){
                                            pay_for_tis_month($event_ID);
                                            ?>
                                            <script>
                                                jQuery(document).ready(function(){
                                                    location.reload();
                                                });
                                            </script>
                                             <?php
                                }
                            }else{
                                if( ($main_per_month*$months_count - $main_per_month_part) == 0 &&
                                      ($percent_per_month*$months_count - $percent_per_month_part) == 0 &&
                                        ($rate - $rate_part) == 0){
                                            pay_for_tis_month($event_ID);
                                            ?>
                                            <script>
                                                jQuery(document).ready(function(){
                                                    location.reload();
                                                });
                                            </script>
                                             <?php
                                }                        
                            }
                            ?>
                                <tr class="<?=$tr_class; ?> line-one">
                                    <td width="10px" style="vertical-align: middle;"><?=$key+1; ?></td>
                                    <td width="400px" style="vertical-align: middle;"><a href="<?=get_permalink($loan_id);?>"><?=$loan_title; ?></a></td>
                                    <td width="200px" style="vertical-align: middle;"><?=$user_name; ?></td>
                                    <td style="vertical-align: middle;"><?=date('Y-m-d', strtotime($contract->post_date)); ?></td>
                                    <td style="vertical-align: middle;"><?=date('Y-m-d', strtotime($event_start_date)); ?></td>
                                    <td style="vertical-align: middle;"><?=$col_months; ?></td>
                                    <td  style="vertical-align: middle;">
                                        <?php if(get_field('advanced_repayment',$loan_id) == 'да' && !is_null(get_advanced_repayment($event_ID))): ?>
                                        Есть
                                        <?php else: ?>
                                        Нет
                                        <?php endif; ?>                                        
                                    </td>                                        
                                </tr>
                                <tr class="line-two" style="display:none;">
                                    <td colspan="8" >
                                        <table class="two">
                                            <tr>
                                                <th>Сумма кредита :</th>
                                                <td ><?=$loan_summ;?></td>
                                                <th>Сумма основного долга :</th>
                                                <td><span class="main_permonth_<?=$event_ID;?>"><?php if(is_null($months_count))  echo ($main_per_month-$main_per_month_part); else echo ($main_per_month*$months_count-$main_per_month_part); ?></span> </td>
                                                <td><input style="text-align:left!important;" alt="decimal" id="pay-main-permonth-<?=$event_ID;?>"  type="text" /></td>
                                                <td><div class="error-<?=$event_ID;?>"></div> </td>
                                                <td>  </td>
                                            </tr>
                                            <tr>
                                                <th>Процентная ставка :</th>
                                                <td><?=$loan_percentage;?> %</td>
                                                <th>Сумма долга по процентам :</th>
                                                <td><span class="percent_permonth_<?=$event_ID;?>"><?php if(is_null($months_count)) echo ($percent_per_month-$percent_per_month_part); else echo ($percent_per_month*$months_count-$percent_per_month_part); ?></span></td>
                                                <td><input style="text-align:left!important;" alt="decimal" id="pay-percent-permonth-<?=$event_ID;?>"  type="text" />  </td>
                                                <td><?php if($active && !$canceled): ?>
                                                        <a id="<?=$event_ID;?>" class="payment" href="<?php echo ''; ?>">Оплатить   <span class="glyphicon glyphicon-usd"></span></a>
                                                    <?php endif; ?>
                                                    <?php if(!$active && !$canceled): ?>
                                                        Оплачено   <span class="glyphicon glyphicon-ok"></span>
                                                    <?php endif; ?>
                                                    <?php if($canceled): ?>
                                                        <a style="color:black;" id="<?=$event_ID.'-'.$contract->ID; ?>" class='remove-contract' href="<?php echo ''; ?>">Закрыть    <span class="glyphicon glyphicon-trash"></span></a>
                                                    <?php endif; ?>
                                                </td>
                                                <td>  </td>
                                            </tr>                                                                                        
                                            <tr>
                                                <th>Сумма выплат за весь период :</th>
                                                <td><?=$loan_all_summ;?></td>
                                                <th>Cумма платежа пени :  </th>
                                                <td><span class="rate_<?=$event_ID;?>"><?php echo $rate-$rate_part; ?></span></td>
                                                <td><input alt="decimal" style="text-align:left!important;" id="pay-rate-<?=$event_ID;?>"  type="text" />  </td>
                                                <td>  </td>
                                                <td> </td>
                                            </tr>                                            
                                                <tr>
                                                    <th>Денежная единица :</th>
                                                    <td><?=$loan_currency;?></td>
                                                    <?php if(get_field('advanced_repayment',$loan_id) == 'да' && !is_null(get_advanced_repayment($event_ID))): ?>
                                                        <th>Cумма платежа при досрочном погашении :  </th>
                                                        <td><?php echo get_advanced_repayment($event_ID); ?></td>
                                                        <td><a href="#" id="<?=$event_ID.'-'.$contract->ID; ?>" class='remove-contract'>Досрочно погасить кредит <span class="glyphicon glyphicon-trash"></span></a></td>
                                                    <?php else: ?>
                                                        <td> </td>
                                                        <td> </td>                                                        
                                                    <?php endif; ?>
                                                    <td> </td>
                                                    <td> </td>
                                                </tr>                                              
                                        </table>                               
                                    </td> 
                                </tr>
                             <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </table>
            </div>
        <?php get_pagination_links($category_id, $page_num); ?>
        <?php endif;?>
        
        
        <?php if(isset($_GET['statistics']) && !empty($_GET['statistics'])): ?>
            <?php get_template_part( 'statistics_area' ); ?> 
        <?php endif;?>
        
        <?php if( !isset($_GET['statistics']) && !isset($_GET['clients']) && !isset($_GET['contracts']) ): ?>
            <?php get_template_part( 'work_main_area' ); ?> 
        <?php endif;?>
    </div>

<?php get_footer(); ?>

</div><!--/.container-->
<style>
.form-control {
    display: initial;
}
input[type=text]{
    display: initial;
}
</style>