<?php get_header(); ?>


<div class="container">
    <div class="header">
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav profile-nav">
                <li><a href="#"></a></li>
            </ul>
        </div><!-- /.nav-collapse -->
    </div>
    
    <div class="row marketing">  
        <div style="width:65%; float:left;" class="col-lg-6">
            <h2 style="margin-left:15px;">О банке</h2>
            <pre>
            <span>
                Сегодня Бехуф не только успешно работает на отечественном финансовом рынке, но и достойно представляет Беларусь на международной арене, выступая партнером для своих клиентов. Неиссякаемый энтузиазм, опыт, профессионализм, командный дух сотрудников и умение работать на опережение – вот фундамент для наших будущих достижений, являющихся стабильной платформой и вносящих достойный вклад в развитие молодой и растущей белорусской экономики.                
            </span>
            </pre>
            
            <h2 style="margin-left:15px;">Как нас найти</h2>
            <iframe width="100%" height="450" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps?f=q&amp;source=s_q&amp;hl=ru&amp;geocode=&amp;q=%D0%9A%D1%83%D0%BB%D1%8C%D0%BC%D0%B0%D0%BD+3+%D0%BC%D0%B8%D0%BD%D1%81%D0%BA&amp;aq=&amp;sll=59.955999,30.325613&amp;sspn=0.026686,0.084543&amp;ie=UTF8&amp;hq=&amp;hnear=%D1%83%D0%BB.+%D0%9A%D1%83%D0%BB%D1%8C%D0%BC%D0%B0%D0%BD+3,+%D0%A1%D0%BE%D0%B2%D0%B5%D1%82%D1%81%D0%BA%D0%B8%D0%B9+%D1%80%D0%B0%D0%B9%D0%BE%D0%BD,+%D0%9C%D0%B8%D0%BD%D1%81%D0%BA,+%D0%9C%D0%B8%D0%BD%D1%81%D0%BA%D0%B8%D0%B9+%D1%80%D0%B0%D0%B9%D0%BE%D0%BD,+%D0%9C%D0%B8%D0%BD%D1%81%D0%BA%D0%B0%D1%8F+%D0%BE%D0%B1%D0%BB%D0%B0%D1%81%D1%82%D1%8C,+%D0%91%D0%B5%D0%BB%D0%B0%D1%80%D1%83%D1%81%D1%8C&amp;t=m&amp;z=14&amp;ll=53.919774,27.582541&amp;output=embed"></iframe><br /><small><a href="https://www.google.com/maps?f=q&amp;source=embed&amp;hl=ru&amp;geocode=&amp;q=%D0%9A%D1%83%D0%BB%D1%8C%D0%BC%D0%B0%D0%BD+3+%D0%BC%D0%B8%D0%BD%D1%81%D0%BA&amp;aq=&amp;sll=59.955999,30.325613&amp;sspn=0.026686,0.084543&amp;ie=UTF8&amp;hq=&amp;hnear=%D1%83%D0%BB.+%D0%9A%D1%83%D0%BB%D1%8C%D0%BC%D0%B0%D0%BD+3,+%D0%A1%D0%BE%D0%B2%D0%B5%D1%82%D1%81%D0%BA%D0%B8%D0%B9+%D1%80%D0%B0%D0%B9%D0%BE%D0%BD,+%D0%9C%D0%B8%D0%BD%D1%81%D0%BA,+%D0%9C%D0%B8%D0%BD%D1%81%D0%BA%D0%B8%D0%B9+%D1%80%D0%B0%D0%B9%D0%BE%D0%BD,+%D0%9C%D0%B8%D0%BD%D1%81%D0%BA%D0%B0%D1%8F+%D0%BE%D0%B1%D0%BB%D0%B0%D1%81%D1%82%D1%8C,+%D0%91%D0%B5%D0%BB%D0%B0%D1%80%D1%83%D1%81%D1%8C&amp;t=m&amp;z=14&amp;ll=53.919774,27.582541" style="color:#0000FF;text-align:left">Просмотреть увеличенную карту</a></small>            
        </div>
       
        
        <div style="width:35%;" class="col-lg-6">               
            <h2 class="text-muted" >КОНТАКТ-ЦЕНТР</h2>
<pre>            
Новые услуги банка:  +375 17 289 90 90,
487 -  velcom, МТС, life:)
Поддержка клиентов круглосуточно: +375 17 289 92 92           
</pre>              
            <h2 style="/*margin-left:150px;*/" class="text-muted">Отправить сообщение </h2>
            <?php
                echo (wpcf7_contact_form_tag_func(array( 'id' => 529, 'title' => '[contact-form-7]' ), null, 'contact-form-7'));               
            ?>          
        </div>
    </div>






<?php get_footer(); ?>

</div><!--/.container-->