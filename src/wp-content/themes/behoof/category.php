<?php get_header(); ?>
<?php
$category = get_the_category();
$cat_array = array();
$sub_cat_array = array();

if(count($category) > 1){
    foreach($category as $cat){
        $parent_id = $cat->parent;
        if($parent_id != 0){
            $cat_array[] = get_category($parent_id)->slug;
            $category_id = $parent_id;
        }
    }
    foreach(get_categories('&child_of='.$category_id) as $sub){
        $sub_cat_array[] = $sub->term_id;
    }
}else{
    $category_id = $category[0]->cat_ID;
}


$page_num = get_cur_page_num();
$category_slug = $category[0]->slug;

$user_role = get_user_role();

if($category_slug != RESPONSES && !in_array(LOANS, $cat_array) && $category_slug != LOANS){
    $lastPosts = get_pagination_data($category_id, $page_num);
}else{
    $lastPosts = query_posts('cat='.$category_id);
}

?>
<div class="container">
    
<div class="row row1 row-offcanvas row-offcanvas-right">
  <div class="col-xs-12 col-sm-9">
    <p class="pull-right visible-xs">
      <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
    </p>
  <div class="row row2">
  <?php if(in_array(LOANS, $cat_array) || $category_slug == LOANS): ?>
      <h2>Список кредитов:</h2>
      <?php foreach($sub_cat_array as $sub_cat_id): ?>
          <?php $posts = query_posts('cat='.$sub_cat_id); ?>
          <br><h4 style="color: #04A39C; font-weight: bold;"><?=get_category($sub_cat_id)->name; ?></h4>
          <table class="table table-hover">
              <tr class="grey"> 
                  <th>Название</th>
                  <th>Лимит</th>
                  <th>Срок</th>
                  <th>Возраст</th>
                  <th>Процентная ставка</th>
                  <th>Узнать больше</th>
              </tr>
              <?php if (!empty($posts)): ?>
                  <?php foreach ($posts as $item): ?>
                      <tr>
                          <td width="200px"><?php echo $item->post_title; ?></td>
                          <td width="150px"><?php echo get_field('limit',$item->ID); ?></td>
                          <td><?php echo get_field('term',$item->ID); ?></td>
                          <td><?php echo get_field('age',$item->ID); ?></td>
                          <td><?php echo get_field('percentage',$item->ID); ?> годовых</td>
                          <td width="135px"><a id="<?=$item->ID;?>" href="<?php echo get_permalink( $item->ID ); ?>">Узнать больше   <span class="glyphicon glyphicon-list-alt"></span></a></td>
                      </tr>
                  <?php endforeach; ?>
              <?php endif; ?>
          </table>
      <?php endforeach; ?>

  <?php elseif($category_slug == REQUESTS): ?>
      <h2>Список заявок:</h2>
      <?php if($user_role == 'security'): ?>
            <div id="square-white"></div> <span style="padding-left: 10px;">- отмечены запросы от оператора</span>
            <br><br>
            <div id="square-pink"></div> <span style="padding-left: 10px;">- отмечены запросы от члена кредитного комитета</span>
            <br><br>            
      <?php endif; ?>
      <table class="table table-hover">
          <tr class="grey">
              <th>Заявка</th>
              <th>На кредит</th>
              <th>Дата</th>
              <th>Статус</th>
              <th>Узнать больше</th>
          </tr>
          <?php if (!empty($lastPosts)): ?>
              <?php foreach ($lastPosts as $item): ?>
              
              <?php if(    ($user_role == 'operator' && ($item->post_excerpt == 'Processing' || $item->post_excerpt == 'Approved')) || 
                           ($user_role == 'security' && $item->post_excerpt == 'Reviewing by security') ||
                           ($user_role == 'head' && $item->post_excerpt == 'Reviewing by head')  ||
                           $user_role == 'administrator'): 
                  ?>
                <?php
                          switch(end(explode('&&&',$item->comment_status))){
                          case 'Processing':
                              $status_style = 'style="background-color:white;"';
                              break;
                          case 'Reviewing by head':                              
                              $status_style = 'style="background-color:#fcf8e3"';
                              break;
                          default:
                              break;
                      }         
                ?>
          
                  <?php if($item->post_excerpt == 'Approved'): ?>
                  <tr style="color:#04A39C;">
                  <?php else: ?>
                  <tr <?=$status_style;?>>
                  <?php endif; ?>
                      <td width="200px"><?php echo $item->post_title; ?></td>
                      <td width="150px"><?php echo wp_get_post_tags( $item->ID)[0]->name; ?></td>
                      <td><?php echo $item->post_date; ?></td>
                      <td><?php echo $item->post_excerpt; ?></td>
                      <td><a href="<?php echo get_permalink( $item->ID ); ?>">Узнать больше   <span class="glyphicon glyphicon-list-alt"></span></a></td>
                  </tr>
                  
              <?php endif; ?>    
                  
              <?php endforeach; ?>
          <?php endif; ?>
      </table>

  <?php elseif($category_slug == RESPONSES): ?>
      <h2>Список договоров:</h2>
        <ul style="padding-bottom:25px;" class="nav nav-pills pull-right">
            <li><a href="<?=get_category_link( $category_id );?>&status=Approved">Активны</a></li>
            <li><a href="<?=get_category_link( $category_id );?>&status=Extinguished">Закрыты</a></li>
        </ul>      
      
      <table class="table table-hover">
          <tr class="grey">
              <th>Заявка</th>
              <th>На кредит</th>
              <th>Дата</th>
              <th>Статус</th>
              <th>Узнать больше</th>
          </tr>
          <?php if (!empty($lastPosts)): ?>
              <?php foreach ($lastPosts as $item): ?>
                      <?php 
                        if( isset($_GET['status']) && !empty($_GET['status']) ) $status = $_GET['status']; 
                        else $status = 'Approved';
                      ?>
                      <?php if($item->post_excerpt == $status): ?>
                        <td width="200px"><?php echo $item->post_title; ?></td>
                        <td width="150px"><?php echo wp_get_post_tags( $item->ID)[0]->name; ?></td>
                        <td><?php echo $item->post_date; ?></td>
                        <td><?php echo $item->post_excerpt; ?></td>
                        <td><a href="<?php echo get_permalink( $item->ID ); ?>">Узнать больше   <span class="glyphicon glyphicon-list-alt"></span></a></td>
                      <?php endif; ?>
                  </tr>
              <?php endforeach; ?>
          <?php endif; ?>
      </table>
  <?php else: ?>
      <?php if (!empty($lastPosts)): ?>
      <h2><?php echo $category[0]->name;  ?></h2>
          <?php foreach ($lastPosts as $item): ?>
        <div>
              <h2><a href="<?php echo get_permalink( $item->ID ); ?> "><?php echo $item->post_title; ?></a></h2>
              <p><?php echo '<pre>' . implode(' ', array_slice(explode(' ', $item->post_content), 0, 300)) . '</pre>'; ?></p>
        </div>
          <?php endforeach; ?>
      <?php endif; ?>

  <?php endif; ?>
      <?php if($category_slug != RESPONSES && !in_array(LOANS, $cat_array) && $category_slug != LOANS): ?>
        <?php get_pagination_links($category_id, $page_num); ?>
      <?php endif; ?>
  </div>
  </div><!--/span-->
  <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
      <?php get_template_part('login-block'); ?>
  </div><!--/span-->
  <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
      <?php get_template_part('right-sidebar'); ?>
  </div><!--/span-->		
</div><!--/row-->   
<?php get_footer(); ?>
</div>
