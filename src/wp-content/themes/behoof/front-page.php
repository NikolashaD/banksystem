<?php get_header(); ?>
<div class="container">
      <div class="row row1 row-offcanvas row-offcanvas-right">
        <div class="col-xs-12 col-sm-9">
          <p class="pull-right visible-xs">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
          </p>
          <div class="jumbotron">
            <img id="front" src="<?php bloginfo('template_directory'); ?>/images/front.png">
          </div>
          <div class="row">
            <?php get_template_part( 'news-block' ); ?>
          </div><!--/row-->
        </div><!--/span-->
      
        <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
            <?php get_template_part( 'login-block' ); ?>
        </div><!--/span-->
        <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
            <?php get_template_part( 'right-sidebar' ); ?>
        </div><!--/span-->
      </div><!--/row-->
<?php get_footer(); ?>

</div><!--/.container-->

<style>
.jumbotron {
    padding-top: 0;
    padding-bottom: 0;
    padding: 0;
}
.row{
	background-color: #f5f5f5;
}
</style>
