<?php
    $category_id = get_category_by_slug( 'requests' )->term_id;
    $page_num = get_cur_page_num();
    $lastPosts = get_pagination_data($category_id, $page_num);
//    $lastPosts = query_posts('cat='.$category_id);
    $category_slug = $category[0]->slug;
    
    $user_role = get_user_role();
?>

      <h2 style="margin-left:50px;" class="text-muted">Список заявок</h2>
      <?php if($user_role == 'security'): ?>
            <div style="margin-left:50px;" id="square-white"></div> <span style="padding-left: 10px;">- отмечены запросы от оператора</span>
            <br><br>
            <div style="margin-left:50px;" id="square-pink"></div> <span style="padding-left: 10px;">- отмечены запросы от члена кредитного комитета</span>
            <br><br>            
      <?php endif; ?>
      <?php if($user_role == 'head'): ?>
            <div style="margin-left:50px;" id="square-white"></div> <span style="padding-left: 10px;">- отмечены запросы, по поводу которых Вы еще не голосовали</span>
            <br><br>
            <div style="margin-left:50px;" id="square-pink"></div> <span style="padding-left: 10px;">- отмечены запросы, по поводу которых Вы уже проголосовали</span>
            <br><br>            
      <?php endif; ?>            
      <table style="margin-top:50px;" class="table table-hover">
          <tr class="grey">
              <th>Заявка</th>
              <th>На кредит</th>
              <th>Дата</th>
              <th>Статус</th>
              <th>Узнать больше</th>
          </tr>
          <?php if (!empty($lastPosts)): ?>
              <?php foreach ($lastPosts as $item): ?>
              
              <?php if(    ($user_role == 'operator' && ($item->post_excerpt == 'Processing' || $item->post_excerpt == 'Approved')) || 
                           ($user_role == 'security' && $item->post_excerpt == 'Reviewing by security') ||
                           ($user_role == 'head' && $item->post_excerpt == 'Reviewing by head')  ||
                           $user_role == 'administrator'): 
                  ?>
                <?php
                          switch(end(explode('&&&',$item->comment_status))){
                          case 'Processing':
                              $status_style = 'style="background-color:white;"';
                              break;
                          case 'Reviewing by head':                              
                              $status_style = 'style="background-color:#fcf8e3"';
                              break;
                          default:
                              break;
                          }
                          
                    $vote_inf = get_vote_information(get_current_user_id(),$item->ID);      
                    $is_voted = $vote_inf['is_voted'];
                    if($is_voted){
                        $status_style = 'style="background-color:#fcf8e3"';
                    }else{
                        $status_style = 'style="background-color:white;"';
                    }
                ?>
          
                  <?php if($item->post_excerpt == 'Approved'): ?>
                  <tr style="color:#04A39C;">
                  <?php else: ?>
                  <tr <?=$status_style;?>>
                  <?php endif; ?>
                      <td width="200px"><?php echo $item->post_title; ?></td>
                      <td width="400px"><?php echo wp_get_post_tags( $item->ID)[0]->name; ?></td>
                      <td><?php echo $item->post_date; ?></td>
                      <td><?php echo $item->post_excerpt; ?></td>
                      <td><a href="<?php echo get_permalink( $item->ID ); ?>">Узнать больше   <span class="glyphicon glyphicon-list-alt"></span></a></td>
                  </tr>
                  
              <?php endif; ?>    
                  
              <?php endforeach; ?>
          <?php endif; ?>
      </table>
    <?php get_pagination_links($category_id, $page_num); ?>
