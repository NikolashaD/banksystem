<?php get_header(); ?>
<div class="container">

  <div class="row row1 row-offcanvas row-offcanvas-right">
    <div class="col-xs-12 col-sm-9">
      <p class="pull-right visible-xs">
        <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
      </p> 
        <?php get_template_part('content'); ?>
    </div><!--/span-->
    <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
        <?php get_template_part('login-block'); ?>
    </div><!--/span-->
    <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
        <?php get_template_part('right-sidebar'); ?>
    </div><!--/span-->		
  </div><!--/row-->

<?php get_footer(); ?>

</div><!--/.container-->

