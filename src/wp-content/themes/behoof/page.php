<?php
function str_count($str, $source)
{
    $arr=explode($str,$source);
    return(count($arr)-1);
}
$cur_url = 'http://' .$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ;


$match_personal_cabinet = str_count(PERSONAL_CABINET, $cur_url);
$match_about_us = str_count(ABOUT_US, $cur_url);
$match_contact_us = str_count(CONTACT_US, $cur_url);
$match_personal_admin_cabinet = str_count(PERSONAL_ADMIN_CABINET, $cur_url);

if($match_personal_cabinet > 0){
    get_template_part( 'personal_cabinet' );
    exit;
}
if($match_about_us > 0){
    get_template_part( 'about_us' );
    exit;
}
if($match_contact_us > 0){
    get_template_part( 'contact_us' );
    exit;
}
if($match_personal_admin_cabinet > 0){
    get_template_part( 'personal_admin_cabinet' );
    exit;
}

if(isset($_GET['loanId']) && !empty($_GET['loanId'])) $loan_id = $_GET['loanId']; else $loan_id = '';


if(is_user_logged_in()){
    $user_id = get_current_user_id();
    $user = get_userdata( $user_id );
    
    $user_firstname = get_usermeta($user_id,'first_name');
    $user_lastname = get_usermeta($user_id,'last_name');
    $user_email = $user->user_email;
    $user_passport = $user->user_login;   
    $user_passport = str_replace('-', ' ', $user_passport);
}

?>

<?php get_header(); ?>
<div class="container">

<div class="row row1 row-offcanvas row-offcanvas-right">
<div class="col-xs-12 col-sm-9">
<p class="pull-right visible-xs">
    <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
</p>
<div class="row row2">
    <div class="remove">
        <h2>Пожалуйста, заполните все поля формы для получения кредита</h2>    
        <hr />
        <span style=" font-size:13px; color:gray">* Корректное заполнение всех полей позволит вам скорее получить ответ на свою заявку</span>
        <br>
    </div>
<input type="hidden" name="loan-id" id='loan-id'/>
<input type="hidden" name="id" id='id'/>
<div class="wrap">
<hr />
<label class="tit">Данные по выбранному кредиту <span class="glyphicon glyphicon-chevron-down"></span></label>
<hr />
<?php
    $all_fields = get_field_objects($loan_id);
?>
<input type="hidden" name="check-role" id='check-role' value="<?=$all_fields['check_role']['value'];?>"/>
<table class="table table-striped">
    <tr>
        <th>Сумма кредита</th>        
        <th>Процентная ставка</th> 
        <th>Срок</th> 
    </tr>  
    <tr>        
        <td><span id="limit"><?=$all_fields['limit']['value'];?></span></td>       
        <td><?=$all_fields['percentage']['value'];?></td> 
        <td><span id="term"><?=$all_fields['term']['value'];?></span></td>
    </tr> 
    <tr>
        <th>Запрашиваемая сумма кредита<span class="req">*</span></th>        
        <th>Процентная ставка</th> 
        <th>Срок<span class="req">*</span></th> 
    </tr> 
    <tr>        
        <td><input style="text-align:left!important;" alt="decimal" type="text" id="loansumm" class="required" placeholder="$100 000" /></td>       
        <td><span id="loanpercentage"><?=$all_fields['percentage']['value'];?></span></td> 
        <?php if( substr_count($all_fields['term']['value'], 'до') > 0 || substr_count($all_fields['term']['value'], 'от') > 0 ): ?>
            <td><input type="text" alt="cpf" id="loanterm" class="required" placeholder="5 лет" /></td>
        <?php else: ?>
            <td><span id="loanterm"><?=$all_fields['term']['value'];?></span></td>
        <?php endif; ?>
    </tr>     
</table>
    
    
    
    
<hr />
<label class="tit">Личные данные <span class="glyphicon glyphicon-chevron-down"></span></label>
<hr />
<table class="prs-data" width="800px">
    <tr>
        <td width="30%"><label>Фамилия:</label><span class="req">*</span></td>
        <td width="70%"><div class="form-group "><input id="lastname" type="text" class="form-control required"  placeholder="Фамилия" value="<?php echo $user_lastname; ?>"></div></td>
    </tr>
    <tr>
        <td><label>Имя:</label><span class="req">*</span></td>
        <td><div class="form-group "><input id="firstname" type="text" class="form-control required"  placeholder="Имя" value="<?php echo $user_firstname; ?>"></div></td>
    </tr>
    <tr>
        <td><label>Отчество:</label><span class="req">*</span></td>
        <td><div class="form-group "><input id="fathername" type="text" class="form-control required"  placeholder="Отчество"></div></td>
    </tr>
    <tr>
        <td><label>Мобильный телефон:</label><span class="req">*</span></td>
        <td><div  class="form-group "><input alt="phone-us" id="mobilephone" type="text" class="form-control required"  placeholder="29 911 66 09"></div></td>
    </tr>
    <tr>
        <td><label>Email:</label><span class="req">*</span></td>
        <td><div class="form-group "><input id="email" type="text" class="form-control required"  placeholder="Email" value="<?php echo $user_email; ?>"></div></td>
    </tr>
</table>

<hr />
<label class="tit">Пасспортные данные <span class="glyphicon glyphicon-chevron-right"></span></label>
<hr />
<table style="/*display:none;*/" class="psp-data" width="800px">
    <tr>
        <td width="30%"><label>Серия паспорта:</label><span class="req">*</span></td>
        <td width="70%"><div class="form-group "><input id="serialnumber" type="text" class="form-control required"  placeholder="MP 2135454" value="<?php echo $user_passport; ?>"></div></td>
    </tr>
    <tr>
        <td><label>Кем выдан:</label></td>
        <td><div class="form-group "><input id="issuedby" type="text" class="form-control"  placeholder="Полностью, как в пасспорте"></div></td>
    </tr>
    <tr>
        <td><label>Дата выдачи:</label></label><span class="req">*</span></td>
        <td><div class="form-group "><input alt="date" id="issueddate" type="text" class="form-control required"  placeholder="гггг/мм/дд"></div></td>
    </tr>
    <tr>
        <td><label>Дата рождения:</label><span class="req">*</span></td>
        <td><div class="form-group "><input id="birthdate" alt="date" type="text" class="form-control required"  placeholder="гггг/мм/дд"></div></td>
    </tr>
    <tr>
        <td><label>Место рождения:</label></td>
        <td><div class="form-group "><input id="birthplace" type="text" class="form-control"  placeholder="Место рождения"></div></td>
    </tr>
    <tr>
        <td width="30%"></td>
        <td width="70%"><label>Адрес фактического места проживания</label></td>
    </tr>
    <tr>
        <td width="30%"><label>Индекс:</label></td>
        <td width="70%"><div class="form-group "><input id="homeindex" type="text" class="form-control"  placeholder="Индекс"></div></td>
    </tr>
    <tr>
        <td width="30%"><label>Регион:</label></td>
        <td width="70%"><div class="form-group "><input id="homeregion" type="text" class="form-control"  placeholder="Регион"></div></td>
    </tr>
    <tr>
        <td width="30%"><label>Город:</label></td>
        <td width="70%"><div class="form-group "><input id="homecity" type="text" class="form-control"  placeholder="Город"></div></td>
    </tr>
    <tr>
        <td width="30%"><label>Адрес:</label></td>
        <td width="70%"><div class="form-group "><input id="homeaddress" type="text" class="form-control"  placeholder="пр.Партизанский 32-3-61"></div></td>
    </tr>
    <tr>
        <td width="30%"><label>Номер телефона:</label><span class="req">*</span></td>
        <td width="70%"><div class="form-group "><input alt="phone"  id="homephonenumber" type="text" class="form-control required"  placeholder="295 89 24"></div></td>
    </tr>
</table>

<hr />
<label class="tit">Информация по месту работы <span class="glyphicon glyphicon-chevron-right"></span></label>
<hr />
<table style="/*display:none;*/" class="wrk-inf" width="800px">
    <tr>
        <td width="30%"><label>Тип занятости:</label></td>
        <td width="70%"><div class="form-group ">
                <select id="type-of-empl" type="text" class="form-control" >
                    <option value="1">Работаю в организации</option>
                    <option value="2">Собственный бизнес</option>
                    <option value="3">Не работаю</option>
                </select></td>
    </tr>
    <tr>
        <td width="30%"><label>Название организации:</label></td>
        <td width="70%"><div class="form-group "><input id="workname" type="text" class="form-control"  placeholder="Название организации"></div></td>
    </tr>
    <tr>
        <td width="30%"><label>Номер телефона:</label></td>
        <td width="70%"><div class="form-group "><input alt="phone" id="workphonenumber" type="text" class="form-control"  placeholder="Номер телефона"></div></td>
    </tr>
    <tr>
        <td width="30%"><label>Занимаемая должность:</label></td>
        <td width="70%"><div class="form-group "><input id="workposition" type="text" class="form-control"  placeholder="Занимаемая должность"></div></td>
    </tr>
    <tr>
        <td width="30%"><label>Срок работы:</label></td>
        <td width="70%"><div class="form-group "><input id="workduration" type="text" class="form-control"  placeholder="гг/мм"></div></td>
    </tr>
    <tr>
        <td width="30%"></td>
        <td width="70%"><label>Рабочий адрес</label></td>
    </tr>
    <tr>
        <td width="30%"><label>Индекс:</label></td>
        <td width="70%"><div class="form-group "><input id="workindex" type="text" class="form-control"  placeholder="Индекс"></div></td>
    </tr>
    <tr>
        <td width="30%"><label>Регион:</label></td>
        <td width="70%"><div class="form-group "><input id="workregion" type="text" class="form-control"  placeholder="Регион"></div></td>
    </tr>
    <tr>
        <td width="30%"><label>Город:</label></td>
        <td width="70%"><div class="form-group "><input id="workcity" type="text" class="form-control"  placeholder="Город"></div></td>
    </tr>
    <tr>
        <td width="30%"><label>Адрес:</label></td>
        <td width="70%"><div class="form-group "><input id="workaddress" type="text" class="form-control"  placeholder="Адрес"></div></td>
    </tr>
</table>

<hr />
<label class="tit">Личная информация <span class="glyphicon glyphicon-chevron-right"></span></label>
<hr />
<table style="/*display:none;*/" class="more-prs-data" width="800px">
    <tr>
        <td width="30%"><label>Количество детей:</label></td>
        <td width="70%"><div class="form-group ">
                <select id="children" type="text" class="form-control" >
                    <option value="0">нет</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">более</option>
                </select></td>
    </tr>
    <tr>
        <td width="30%"><label>Семейное положение:</label></td>
        <td width="70%"><div class="form-group ">
                <select id="status" type="text" class="form-control" >
                    <option value="1">Не женат/замужем</option>
                    <option value="2">Состою в браке</option>
                    <option value="3">Разведен/а</option>
                </select></td>
    </tr>
    <tr>
        <td width="30%"><label>Образование:</label></td>
        <td width="70%"><div class="form-group ">
                <select id="education" type="text" class="form-control" >
                    <option value="1">Начальное</option>
                    <option value="2">Среднее</option>
                    <option value="3">Высшее</option>
                    <option value="4">Магистратура</option>
                    <option value="5">Аспирантура</option>
                </select></td>
    </tr>
    <tr>
        <td width="30%"><label>Личный автомобиль:</label></td>
        <td width="70%"><div class="form-group ">
                <select id="personal-car" type="text" class="form-control" >
                    <option value="1">есть</option>
                    <option value="2">не имею</option>
                </select></td>
    </tr>
    <tr>
        <td width="30%"><label>Персональный доход:</label><span class="req">*</span></td>
        <td width="70%"><div class="form-group "><input alt="decimal" style="text-align:left!important;" id="personalincome" type="text" class="form-control required"  placeholder="руб/мес"></div></td>
    </tr>
    <tr>
        <td width="30%"><label>Сумма аренды квартиры:</label></td>
        <td width="70%"><div class="form-group "><input alt="decimal" style="text-align:left!important;" id="amount" type="text" class="form-control"  placeholder="руб/мес"></div></td>
    </tr>
    <tr>
        <td width="30%"><label>Сумма платежей по кредитам других банков:</label></td>
        <td width="70%"><div class="form-group "><input alt="decimal" style="text-align:left!important;" id="otherpayments" type="text" class="form-control"  placeholder="руб/мес"></div></td>
    </tr>
</table>


<hr />
<label class="tit">Поручители <span class="glyphicon glyphicon-chevron-right"></span></label>
<hr />
<table style="/*display:none;*/" class="bails" width="800px">
    <tr>
        <td width="30%"><label>1. Фамилия:</label></td>
        <td width="70%"><div class="form-group "><input id="baillastname1" type="text" class="form-control"  placeholder="Фамилия"></div></td>
    </tr>
    <tr>
        <td width="30%"><label>1. Имя:</label></td>
        <td width="70%"><div class="form-group "><input id="bailfirstname1" type="text" class="form-control"  placeholder="Имя"></div></td>
    </tr>
    <tr>
        <td width="30%"><label>1. Отчество:</label></td>
        <td width="70%"><div class="form-group "><input id="bailfathername1" type="text" class="form-control"  placeholder="Отчество"></div></td>
    </tr>
    <tr>
        <td width="30%"><label>1. Домашний адрес:</label></td>
        <td width="70%"><div class="form-group "><input id="bailaddress1" type="text" class="form-control"  placeholder="г.Минск пр.Партизанский 32-3-61"></div></td>
    </tr>
    <tr>
        <td width="30%"><label>1. Мобильный телефон:</label></td>
        <td width="70%"><div class="form-group "><input alt="phone-us" id="bailmobilephone1" type="text" class="form-control"  placeholder="Мобильный телефон"></div></td>
    </tr>
    <tr>
        <td width="30%"><label>1. Домашний телефон:</label></td>
        <td width="70%"><div class="form-group "><input alt="phone" id="bailhomephone1" type="text" class="form-control"  placeholder="Домашний телефон"></div></td>
    </tr>



    <tr>
        <td width="30%"></td>
        <td width="70%"><label>Необязательно заполнять, но рекомендуется</label></td>
    </tr>


    <tr>
        <td width="30%"><label>2. Фамилия:</label></td>
        <td width="70%"><div class="form-group "><input id="baillastname2" type="text" class="form-control"  placeholder="Фамилия"></div></td>
    </tr>
    <tr>
        <td width="30%"><label>2. Имя:</label></td>
        <td width="70%"><div class="form-group "><input id="bailfirstname2" type="text" class="form-control"  placeholder="Имя"></div></td>
    </tr>
    <tr>
        <td width="30%"><label>2. Отчество:</label></td>
        <td width="70%"><div class="form-group "><input id="bailfathername2" type="text" class="form-control"  placeholder="Отчество"></div></td>
    </tr>
    <tr>
        <td width="30%"><label>2. Домашний адрес:</label></td>
        <td width="70%"><div class="form-group "><input id="bailaddress2" type="text" class="form-control"  placeholder="г.Минск пр.Партизанский 32-3-61"></div></td>
    </tr>
    <tr>
        <td width="30%"><label>2. Мобильный телефон:</label></td>
        <td width="70%"><div class="form-group "><input alt="phone-us" id="bailmobilephone2" type="text" class="form-control"  placeholder="Мобильный телефон"></div></td>
    </tr>
    <tr>
        <td width="30%"><label>2. Домашний телефон:</label></td>
        <td width="70%"><div class="form-group "><input alt="phone" id="bailhomephone2" type="text" class="form-control"  placeholder="Домашний телефон"></div></td>
    </tr>
</table>


<button style="width:180px;margin-left: 632px;" type="button" class="btn btn-lg btn-primary btn-block send-request">Отправить заявку <span class="glyphicon glyphicon-chevron-right"></span></button>
</div>
</div><!--/row-->
</div><!--/span-->
<div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
    <?php get_template_part('login-block'); ?>
</div><!--/span-->
<div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
    <?php get_template_part('right-sidebar'); ?>
</div><!--/span-->
</div><!--/row-->

<?php get_footer(); ?>

</div><!--/.container-->


