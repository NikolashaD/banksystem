<?php
define('LOANS', 'loans');
define('REQUESTS', 'requests');
define('RESPONSES', 'responses');
define('REQUEST', 'request-form');
define('ADMIN', 'administrator');
define('PERSONAL_CABINET', get_permalink(349));
define('PERSONAL_ADMIN_CABINET', get_permalink(377));
define('ABOUT_US', get_permalink(369));
define('CONTACT_US', get_permalink(371));

define('PER_PAGE', 1);
define('PAG_LINKS_NUM', 3);

define('CUR_URL',$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] );


function list_all($item){
    ?>
    <div class="row row2">
        <h2><a href="<?php echo get_permalink( $item->ID ); ?> "><?php echo $item->post_title; ?></a></h2>
        <p><?php echo implode(' ', array_slice(explode(' ', $item->post_content), 0, 300)); ?></p>
        <br><br>
    </div><!--/row-->
<?php
}

function shorten_string($string, $wordsreturned)
    /*  Returns the first $wordsreturned out of $string.  If string
    contains fewer words than $wordsreturned, the entire string
    is returned.
    */
{
    $retval = $string;      //  Just in case of a problem

    $array = explode(" ", $string);
    if (count($array)<=$wordsreturned)
        /*  Already short enough, return the whole thing
        */
    {
        $retval = $string;
    }
    else
        /*  Need to chop of some words
        */
    {
        array_splice($array, $wordsreturned);
        $retval = implode(" ", $array)." ...";
    }
    return $retval;
}


add_action('wp_ajax_redirect', 'prefix_ajax_redirect');
add_action('wp_ajax_nopriv_redirect', 'prefix_ajax_redirect');

function prefix_ajax_redirect(){
    $post_id = $_POST['id'];
    global $wpdb;
    $date = date('Y-m-d H:i:s', time());


    $request_form_page = get_page_by_title('Request Form', $output = OBJECT, $post_type = 'page' );

    if(is_null($request_form_page)){
        $post_form= array(
            'post_title' => 'Request Form',
            'post_content' => '',
            'post_author' => 1,
            'post_type' => 'page',
            'post_name' => REQUEST,
            'to_ping' => '',
            'pinged' => '',
            'post_content_filtered' => '',
            'post_excerpt' => '',
            'post_date' => $date,
            'post_date_gmt' => $date,
            'post_modified_gmt' => $date,
            'post_modified' => $date,
            'comment_count' => $post_id,
        );

        $wpdb->insert($wpdb->prefix . 'posts', $post_form);
    }

    $page = get_page_by_path( REQUEST );
    $url = get_permalink($page->ID);

    header("Content-type: application/json; charset=UTF-8");
    exit(json_encode(array(
            'url' => $url,
            'loan_id' => $_POST['loan_id']
        )
    ));
}

add_action('wp_ajax_handling', 'prefix_ajax_handling');
add_action('wp_ajax_nopriv_handling', 'prefix_ajax_handling');

function prefix_ajax_handling(){
    global $wpdb;
    $category_id = get_category_by_slug( 'requests' )->term_id;
    $date = date('Y-m-d H:i:s', time());
    $message = "";
    $success = false;
    $page_url = $_POST['url'];
    $page_id = url_to_postid( $page_url );
    $loan_id = $_POST['loan_id'];
    $loan_post = get_post($loan_id);
    $loan_title = $loan_post->post_title;

    $loan_data = $_POST['loanData'];
    $personal_data = $_POST['personalData'];
    $passport_data = $_POST['passportData'];
    $work_inf = $_POST['workInformation'];
    $more_personal_data = $_POST['morePersonalData'];
    $bails_data = $_POST['bailsData'];

    $user_full_name = $personal_data['lastName'] . ' ' . $personal_data['firstName'];

    $user_ID = str_replace(' ','-',$passport_data['passport']);
    
    
    
    if(!form_validation($user_ID)){
        $success = false;
        $message = 'Извините, но Ваши данные не соответствуют действительности или Вы находитесь в черном списке нашей компании ';
        $message .= '<a href="'. get_home_url() .'">Вернуться на домашнюю страницу.</a>';


        header("Content-type: application/json; charset=UTF-8");
        exit(json_encode(array(
                'success' => $success,
                'message' => $message
            )
        ));        
    }

// формируем контент страницы

    $content = '<h2>Заявка на кредит:<a href="'.  get_permalink($loan_id) .'"> '.$loan_title.'</a></h2><br>';
    $content .= '<input class="user-id" type="hidden" value='. $user_ID .'>';
    $content .= '<input class="loan-id" type="hidden" value='. $loan_id .'>';
    $content .= '<input class="user-email" type="hidden" value='. $personal_data['email'] .'>';
    
    $content .= '<h3>Данные по кредиту</h3>';
    $content .= 'Сумма кредита : <span id="limit">' . $loan_data['limit'] . '</span><br>';
    $content .= 'Процентная ставка : <span id="loanPercentage">' . $loan_data['loanPercentage'] . '</span><br>';
    $content .= 'Срок : <span id="term">' . $loan_data['term'] . '</span><hr>';
    
    $content .= 'Запрашиваемая сумма кредита : <span id="loanSumm">' . $loan_data['loanSumm'] . '</span><br>';
    $content .= 'Процентная ставка : <span id="loanPercentage">' . $loan_data['loanPercentage'] . '</span><br>';
    $content .= 'Срок : <span id="loanTerm">' . $loan_data['loanTerm'] . '</span><br>';
    
    $content .= '<h3>Личные данные</h3><br>';
    $content .= 'Фамилия : <span id="lastName">' . $personal_data['lastName'] . '</span><br>';
    $content .= 'Имя : <span id="firstName">' . $personal_data['firstName']. '</span><br>';
    $content .= 'Отчество : <span id="fatherName">' . $personal_data['fatherName']. '</span><br>';
    $content .= 'Мобильный телефон : <span id="mobilePhone">' . $personal_data['mobilePhone']. '</span><br>';
    $content .= 'Email : <span id="email">' . $personal_data['email']. '</span><br>';

    $content .= '<h3>Пасспортные данные</h3><br>';

    $content .= 'Серия пасспорта : <span id="passport">' . $passport_data['passport']. '</span><br>';
    $content .= 'Кем выдан : <span id="issuedBy">' . $passport_data['issuedBy']. '</span><br>';
    $content .= 'Дата выдачи : <span id="issuedDate">' . $passport_data['issuedDate']. '</span><br>';
    $content .= 'Дата рождения : <span id="birthDate">' . $passport_data['birthDate']. '</span><br>';
    $content .= 'Место рождения : <span id="birthPlace">' . $passport_data['birthPlace']. '</span><br>';
    $content .= 'Индекс : <span id="homeIndex">' . $passport_data['index']. '</span><br>';
    $content .= 'Регион : <span id="homeRegion">' . $passport_data['region']. '</span><br>';
    $content .= 'Город : <span id="homeCity">' . $passport_data['city']. '</span><br>';
    $content .= 'Адрес : <span id="homeAddress">' . $passport_data['address']. '</span><br>';
    $content .= 'Номер телефона : <span id="homePhoneNumber">' . $passport_data['phoneNumber']. '</span><br>';

    $content .= '<h3>Информация по месту работы</h3><br>';

    $content .= 'Тип занятости : <span id="typeOfEmploymentText">' . $work_inf['typeOfEmploymentText']. '</span><br>';
    $content .= 'Название организации : <span id="nameOfOrganisation">' . $work_inf['nameOfOrganisation']. '</span><br>';
    $content .= 'Номер телефона : <span id="workPhoneNumber">' . $work_inf['phoneNumber']. '</span><br>';
    $content .= 'Занимаемая должность : <span id="heldPosition">' . $work_inf['heldPosition']. '</span><br>';
    $content .= 'Срок работы : <span id="workDuration">' . $work_inf['workDuration']. '</span><br>';
    $content .= 'Индекс : <span id="workIndex">' . $work_inf['index']. '</span><br>';
    $content .= 'Регион : <span id="workRegion">' . $work_inf['region']. '</span><br>';
    $content .= 'Город : <span id="workCity">' . $work_inf['city']. '</span><br>';
    $content .= 'Адрес : <span id="workAddress">' . $work_inf['address']. '</span><br>';

    $content .= '<h3>Личная информация</h3><br>';

    $content .= 'Количество детей : <span id="childrenText">' . $more_personal_data['childrenText']. '</span><br>';
    $content .= 'Семейное положение : <span id="statusText">' . $more_personal_data['statusText']. '</span><br>';
    $content .= 'Образование : <span id="educationText">' . $more_personal_data['educationText']. '</span><br>';
    $content .= 'Персональный автомобиль : <span id="personalCarText">' . $more_personal_data['personalCarText']. '</span><br>';
    $content .= 'Персональный доход : <span id="personalIncome">' . $more_personal_data['personalIncome']. '</span><br>';
    $content .= 'Сумма аренды квартиры : <span id="amount">' . $more_personal_data['amount']. '</span><br>';
    $content .= 'Сумма платежей по кредитам других банков : <span id="payments">' . $more_personal_data['payments']. '</span><br>';

    $content .= '<h3>Информация о поручителях</h3><br>';

    $content .= '1. Фамилия : <span id="bailLastName1">' . $bails_data['bailLastName1']. '</span><br>';
    $content .= '1. Имя : <span id="bailFirstName1">' . $bails_data['bailFirstName1']. '</span><br>';
    $content .= '1. Отчество : <span id="bailFatherName1">' . $bails_data['bailFatherName1']. '</span><br>';
    $content .= '1. Домашний адрес : <span id="bailAddress1">' . $bails_data['bailAddress1']. '</span><br>';
    $content .= '1. Мобильный телефон : <span id="bailMobilePhone1">' . $bails_data['bailMobilePhone1']. '</span><br>';
    $content .= '1. Домашний телефон : <span id="bailHomePhone1">' . $bails_data['bailHomePhone1']. '</span><br>';
    $content .= '<br>';
    $content .= '2. Фамилия : <span id="bailLastName2">' . $bails_data['bailLastName2']. '</span><br>';
    $content .= '2. Имя : <span id="bailFirstName2">' . $bails_data['bailFirstName2']. '</span><br>';
    $content .= '2. Отчество : <span id="bailFatherName2">' . $bails_data['bailFatherName2']. '</span><br>';
    $content .= '2. Домашний адрес : <span id="bailAddress2">' . $bails_data['bailAddress2']. '</span><br>';
    $content .= '2. Мобильный телефон : <span id="bailMobilePhone2">' . $bails_data['bailMobilePhone2']. '</span><br>';
    $content .= '2. Домашний телефон : <span id="bailHomePhone2">' . $bails_data['bailHomePhone2']. '</span><br>';



// закончили оформлять страницу

    $check_role =  $loan_data['checkRole'];
    switch($check_role){
        case '0':
            $post_excerpt = 'Processing';
            break;
        case '1':
            $post_excerpt = 'Reviewing by head';
            break;    
        default:
            $post_excerpt = 'Processing';
            break;
    }

// добавляем новую страницу название - id + имя фамилия пользователя в категории request
    $request= array(
        'post_title' => 'Заявка ' . $user_ID . ' ' . $user_full_name,
        'post_content' => $content,
        'post_author' => 1,
        'post_type' => 'post',
        'post_name' => REQUEST . '-' . $loan_id . '-' . $user_ID,
        'to_ping' => '',
        'pinged' => '',
        'post_content_filtered' => '',
        'post_excerpt' => $post_excerpt,
        'post_status'    => 'publish',
        'post_date' => $date,
        'post_date_gmt' => $date,
        'post_modified_gmt' => $date,
        'post_modified' => $date
    );

    $wpdb->insert($wpdb->prefix . 'posts', $request);

    // добавляем этому посту нужную категорию!
    $post_ID = get_post_by_name(REQUEST . '-' . $loan_id . '-' . $user_ID)->ID;
    wp_set_post_categories( $post_ID, $category_id );
    wp_set_post_tags( $post_ID, $loan_title );


    $success = true;
    $message = 'Спасибо за обращение в наш банк. Ваша заявка будет рассмотрена в ближайшее время. Ответ будет выслан Вам по email.';
    $message .= '<a href="'. get_home_url() .'">Вернуться на домашнюю страницу.</a>';


    header("Content-type: application/json; charset=UTF-8");
    exit(json_encode(array(
            'success' => $success,
            'message' => $message
        )
    ));
}


add_action('wp_ajax_remove_approve_request', 'prefix_ajax_remove_approve_request');
add_action('wp_ajax_nopriv_remove_approve_request', 'prefix_ajax_remove_approve_request');

function prefix_ajax_remove_approve_request(){
    if(isset($_POST['url']) && !empty($_POST['url'])) $url = $_POST['url']; else $url = '';
    if(isset($_POST['user_email']) && !empty($_POST['user_email'])) $user_email = $_POST['user_email']; else $user_email = '';
    if(isset($_POST['func']) && !empty($_POST['func'])) $func = $_POST['func']; else $func = '';
    global $wpdb;
    $success = true;

    $post_id = url_to_postid( $url );

    if($func == 'remove'){
        wp_delete_post( $post_id, true );
        $email_message = 'Здравствуйте. К сожалению, проанализировав Ваши данные, мы вынуждены Вам отказать в получении кредита.';
        $message = 'Заявка была успешно отклонена.  ';
        $message .= '<a href="'. get_home_url() .'">Вернуться на домашнюю страницу.</a>';
    }
    if($func == 'approve'){
        $query = 'UPDATE wp_posts SET post_excerpt = "Approved"  WHERE ID = "' . $post_id .'"';

        $wpdb->query($query);
        $email_message = 'Здравствуйте. Проанализировав Ваши данные, мы приняли решение о предоставлении Вам кредита. Для этого Вы должны в срок до 7-ми рабочих дней подойти в отделение Behoof банка для заполнения договора со следующими обязательными документами: 1) копия пасспорта 2) справка о доходах с места работы 3) справка о составе семьи';
        $message = 'Заявка была успешна одобрена. ';
        $message .= '<a href="'. get_home_url() .'">Вернуться на домашнюю страницу.</a>';
    }
    wp_create_user( $user_email, '123456789', $user_email );
    $user_id = username_exists( $user_email );

    $recipients = mailusers_get_recipients_from_ids($user_id, '');
    $subject = 'Behoof Loans';
    $type='plaintext';
    $sender_name = 'Behoof company';
    $sender_email = 'behoof@gmail.com';

    mailusers_send_mail($recipients, $subject, $email_message, $type, $sender_name, $sender_email);

    wp_delete_user( $user_id, 1 );

    header("Content-type: application/json; charset=UTF-8");
    exit(json_encode(array(
            'success' => $success,
            'message' => $message
        )
    ));
}



add_action('wp_ajax_change_status', 'prefix_ajax_change_status');
add_action('wp_ajax_nopriv_change_status', 'prefix_ajax_change_status');

function prefix_ajax_change_status(){
    global $wpdb;


    if(isset($_POST['status']) && !empty($_POST['status'])) $status = $_POST['status'];
    else $status = '';

    if(isset($_POST['comment']) && !empty($_POST['comment'])) $comment = $_POST['comment'];
    else $comment = '';

    if(isset($_POST['user_id']) && !empty($_POST['user_id'])) $user_ID = $_POST['user_id'];
    else $user_ID = '';

    if(isset($_POST['loan_id']) && !empty($_POST['loan_id'])) $loan_id = $_POST['loan_id'];
    else $loan_id = '';

    $post = get_post_by_name(REQUEST . '-' . $loan_id . '-' . $user_ID);

    $current_status = $post->post_excerpt;

    if($status != '' && $status != 'undefined'){
        switch($status){
            case 'processing':
                $query_status = 'Processing';
                break;
            case 'security-review':
                $query_status = 'Reviewing by security';
                break;
            case 'head-review':
                $query_status = 'Reviewing by head';
                break;
            default:
                break;
        }
        $query = 'UPDATE wp_posts SET post_excerpt = "'.$query_status.'", comment_status = "'.$comment. '&&&' .$current_status.'" WHERE ID = "' . $post->ID .'"';

        $wpdb->query($query);
    }

    return true;
}



add_action('wp_ajax_export_to_word_doc', 'prefix_ajax_export_to_word_doc');
add_action('wp_ajax_nopriv_export_to_word_doc', 'prefix_ajax_export_to_word_doc');

function prefix_ajax_export_to_word_doc(){
    if(isset($_POST['first_name']) && !empty($_POST['first_name'])) $first_name = $_POST['first_name']; else $first_name = '';
    if(isset($_POST['last_name']) && !empty($_POST['last_name'])) $last_name = $_POST['last_name']; else $last_name = '';
    if(isset($_POST['father_name']) && !empty($_POST['father_name'])) $father_name = $_POST['father_name']; else $father_name = '';
    if(isset($_POST['passport']) && !empty($_POST['passport'])) $passport = $_POST['passport']; else $passport = '';
    if(isset($_POST['loan_id']) && !empty($_POST['loan_id'])) $loan_id = $_POST['loan_id']; else $loan_id = '';

    $loan_post = get_post($loan_id);
    $loan_title = $loan_post->post_title;




    if (!defined('DS')) { define('DS', DIRECTORY_SEPARATOR); }
    error_reporting(E_ALL);
// Include the PHPWord.php, all other classes were loaded by an autoloader
    ini_set('include_path', ABSPATH . 'wp-content'. DS .'themes'. DS .'behoof'. DS .'lib');
    require_once 'PHPWord.php';

// Create a new PHPWord Object
    $PHPWord = new PHPWord();



// Every element you want to append to the word document is placed in a section. So you need a section:
    $section = $PHPWord->createSection();

// You can directly style your text by giving the addText function an array:
    $section->addText('Бланки: Кредитный договор.', array('name'=>'Times New Roman', 'size'=>18, 'bold'=>true));
    $PHPWord->addFontStyle('myOwnStyle', array('name'=>'Courier New', 'size'=>10));
    $myOwnStyle = array('name'=>'Courier New', 'size'=>9);

/* page 1 */
    $section->addText(' ',$myOwnStyle);
    $section->addText('                       КРЕДИТНЫЙ ДОГОВОР № ___',$myOwnStyle);
    $section->addText(' ',$myOwnStyle);
    $section->addText(' "БехувБанк        ",  именуемый в дальнейшем «Банк» или «Кредитор»,',$myOwnStyle);
    $section->addText('(наименование банка)',$myOwnStyle);
    $section->addText('в лице Ниолаенко Д.А, руководитель кредитного отдела,  действующего на основании',$myOwnStyle);
    $section->addText('       (Ф.И.О., должность руководителя)                          ',$myOwnStyle);
    $section->addText('_______________________________________________,  с одной стороны, и',$myOwnStyle);
    $section->addText('      (название, номер, дата документа)                      ',$myOwnStyle);
    $section->addText(''.$last_name.' ' .$first_name. ' ' .$father_name .', ',$myOwnStyle);
    $section->addText('     (полное наименование юридического лица или Ф.И.О. и другие ',$myOwnStyle);
    $section->addText('                паспортные данные физического лица)',$myOwnStyle);
    $section->addText('именуемый в дальнейшем «Кредитополучатель»  или  «Заемщик», ',$myOwnStyle);
    $section->addText('с  другой   стороны,   именуемые  в дальнейшем «стороны»,  заключили ',$myOwnStyle);
    $section->addText('настоящий договор о нижеследующем.',$myOwnStyle);
    $section->addText(' ',$myOwnStyle);
    $section->addText('                        I. Предмет договора',$myOwnStyle);
    $section->addText(' ',$myOwnStyle);
    $section->addText('     1а.  Банк (Кредитор) предоставляет Заемщику (Кредитополучателю)',$myOwnStyle);
    $section->addText('кредит в сумме __________.',$myOwnStyle);
    $section->addText('     1б.   Банк   обязуется   предоставить   Заемщику   в  2013  г.',$myOwnStyle);
    $section->addText('долгосрочный   (краткосрочный)   кредит   в  сумме  ________  рублей',$myOwnStyle);
    $section->addText('(долларов  США),  который  используется Заемщиком на мероприятия и в',$myOwnStyle);
    $section->addText('сроки, указанные в разделах II и III настоящего договора.',$myOwnStyle);
    $section->addText('     1в.  Банк  открывает Заемщику невозобновляемую  кредитную линию',$myOwnStyle);
    $section->addText('для  оплаты  за  полученное  ____________________ согласно договорам',$myOwnStyle);
    $section->addText('                            (оборудование и т.п.)',$myOwnStyle);
    $section->addText('поставки, прилагаемым к кредитному договору, в размере _____________',$myOwnStyle);
    $section->addText('сроком на _____ месяца (месяцев) на период с ________2013 г.  по ___',$myOwnStyle);
    $section->addText('20__ г.,  за что Заемщик уплачивает Банку _% годовых от  фактической ',$myOwnStyle);
    $section->addText('суммы задолженности.',$myOwnStyle);
    $section->addText('     2.  Заемщик  обязуется  принять  сумму, указанную в п.1а (п.1б)',$myOwnStyle);
    $section->addText('настоящего договора. (примечание)',$myOwnStyle);
    $section->addText('     3.  За  счет  выделенных  кредитов Банк своевременно производит',$myOwnStyle);
    $section->addText('оплату акцептованных документов за выполненные для Заемщика работы и',$myOwnStyle);
    $section->addText('произведенные им затраты по кредитуемым мероприятиям. (примечание)',$myOwnStyle);
    $section->addText('     4.  Кредитор  в  _____-дневный  срок  после подписания договора',$myOwnStyle);
    $section->addText('перечисляет  оговоренную  в п.1а (п.1.б) договора сумму на расчетный',$myOwnStyle);
    $section->addText('счет Кредитополучателя _______________________________. (примечание)',$myOwnStyle);
    $section->addText('                           (номер расчетного счета,',$myOwnStyle);
    $section->addText('                          наименование и код банка)',$myOwnStyle);
    $section->addText('     5. Кредитополучатель обязуется возвратить кредит в ____________',$myOwnStyle);
    $section->addText('_____________________________________________________. (примечание)',$myOwnStyle);
    $section->addText('   (указываются конкретные сроки, периодичность',$myOwnStyle);
    $section->addText('                возврата кредита)',$myOwnStyle);
    $section->addText(' ',$myOwnStyle);
    $section->addText('                              II. Цели',$myOwnStyle);
    $section->addText(' ',$myOwnStyle);
    $section->addText('     1а.     Кредит     предоставляется     на    следующие    цели:',$myOwnStyle);
    $section->addText('_____________________________.',$myOwnStyle);
    $section->addText('     1б. Банк предоставляет Заемщику кредит на реализацию следующего',$myOwnStyle);
    $section->addText('мероприятия:  _________  в  сумме  _______  на  срок  до ______ и на',$myOwnStyle);
    $section->addText('условиях, определенных настоящим договором.',$myOwnStyle);
    $section->addText('     2.  Заемщик  вправе использовать кредит только в соответствии с',$myOwnStyle);
    $section->addText('его целевым назначением и обязан обеспечивать своевременное и полное',$myOwnStyle);




/* page 2 */
    $section->addText('погашение полученного кредита и уплату процентов за пользование им в',$myOwnStyle);
    $section->addText('установленный срок.',$myOwnStyle);
    $section->addText(' ',$myOwnStyle);
    $section->addText('           III. Порядок предоставления и погашения кредита',$myOwnStyle);
    $section->addText(' ',$myOwnStyle);
    $section->addText('     1. Заемщик предоставляет Банку в срок до «___» _______ 20__ г. ',$myOwnStyle);
    $section->addText('следующие документы: (примечание)',$myOwnStyle);
    $section->addText('     - инвестиционный проект;',$myOwnStyle);
    $section->addText('     - технико-экономическое обоснование;',$myOwnStyle);
    $section->addText('     - бизнес-план реализации инвестиционного проекта;',$myOwnStyle);
    $section->addText('     - договоры, заключенные Заемщиком со сторонними организациями;',$myOwnStyle);
    $section->addText('     - другие необходимые документы.',$myOwnStyle);
    $section->addText('     2.  Заемщик  предоставляет  Банку  в  срок до «____» __________',$myOwnStyle);
    $section->addText('20__ г. следующие документы и сведения: (примечание)',$myOwnStyle);
    $section->addText('     - годовой бухгалтерский баланс со всеми приложениями к нему;',$myOwnStyle);
    $section->addText('     - бухгалтерский баланс на последнюю квартальную дату;',$myOwnStyle);
    $section->addText('     - отчет о прибылях и убытках;',$myOwnStyle);
    $section->addText('     -   копии   договоров  (контрактов)  или  других  документов  в',$myOwnStyle);
    $section->addText('подтверждение кредитуемой сделки при кредитовании по ссудным счетам;',$myOwnStyle);
    $section->addText('     -  данные о предполагаемом поступлении и использовании валютных',$myOwnStyle);
    $section->addText('средств (при выдаче кредита в иностранной валюте);',$myOwnStyle);
    $section->addText('     -   копии   учредительных   документов   (устав,  учредительный',$myOwnStyle);
    $section->addText('договор), удостоверенные нотариально, либо вышестоящим органом, либо',$myOwnStyle);
    $section->addText('органом, производившим регистрацию;',$myOwnStyle);
    $section->addText('     -   копию   документа   о  его  регистрации  (перерегистрации),',$myOwnStyle);
    $section->addText('удостоверенную нотариально или регистрирующим органом;',$myOwnStyle);
    $section->addText('     - карточку с образцами подписей, удостоверенную нотариально или',$myOwnStyle);
    $section->addText('вышестоящим  органом,  и  оттиском  его  печати  (последнее возможно',$myOwnStyle);
    $section->addText('только для резидентов Республики Беларусь).',$myOwnStyle);
    $section->addText('     3а. Банк предоставляет Заемщику кредит на срок с «___» ________',$myOwnStyle);
    $section->addText('20__ г. по «____» ___________ 20__ г.',$myOwnStyle);
    $section->addText('     3б.    Банк    предоставляет    кредит   в   следующие   сроки:',$myOwnStyle);
    $section->addText('___________________________________________.',$myOwnStyle);
    $section->addText(' (указывается сумма кредита и дата выдачи)',$myOwnStyle);
    $section->addText('     Под  датой  выдачи понимается срок, когда соответствующая сумма',$myOwnStyle);
    $section->addText('списана  со  счета  банка и перечислена (зачислена) на счет Заемщика',$myOwnStyle);
    $section->addText('или оплачен счет (п.4.3 настоящего раздела).',$myOwnStyle);
    $section->addText('     4.  Кредит предоставляется в течение ____ банковских дней путем',$myOwnStyle);
    $section->addText('перечисления по заявлению Заемщика: (примечание)',$myOwnStyle);
    $section->addText('     4.1. на его расчетный счет в банке ___________________________;',$myOwnStyle);
    $section->addText('     4.2. на открытый ему ссудный счет;',$myOwnStyle);
    $section->addText('     4.3. на оплату счетов поставщиков (расчетных документов);',$myOwnStyle);
    $section->addText('     4.4. на счет ________ Заемщика в ______________________________',$myOwnStyle);
    $section->addText('                                    (указывается наименование банка)',$myOwnStyle);
    $section->addText('в г._________ не позднее «___» __________ 20__ г.',$myOwnStyle);
    $section->addText('     5. В случае невыполнения обязательства (п.4 настоящего раздела)',$myOwnStyle);
    $section->addText('Банк  выплачивает  Заемщику  ______% от суммы кредита за каждый день',$myOwnStyle);
    $section->addText('задержки перечисления суммы кредита.',$myOwnStyle);
    $section->addText(' ',$myOwnStyle);
    $section->addText('                     IV. Права и обязанности сторон',$myOwnStyle);
    $section->addText(' ',$myOwnStyle);
    $section->addText('     1. Банк имеет право:',$myOwnStyle);
    $section->addText('     1.1.    отказаться   от   предоставления   кредита   в   случае',$myOwnStyle);
    $section->addText('непоступления    до   начала   кредитования   обеспечения   возврата',$myOwnStyle);
    $section->addText('банковского кредита (примечание);',$myOwnStyle);
    $section->addText('     1.2.  не  предоставлять  Заемщику  оговоренные  суммы  в случае',$myOwnStyle);
    $section->addText('признания    Заемщика    неплатежеспособным    либо    при   наличии',$myOwnStyle);
    $section->addText('доказательств,  свидетельствующих  о  том,  что  Заемщик  не  сможет',$myOwnStyle);
    $section->addText('выполнить    обязательство   по   возврату   кредитных   средств   и',$myOwnStyle);
    $section->addText('установленных процентов (примечание);',$myOwnStyle);
    $section->addText('     1.3.  увеличить  размер  процентов  за  пользование  кредитными',$myOwnStyle);
    $section->addText('средствами   в  случае  увеличения  Национальным  банком  Республики',$myOwnStyle);
    $section->addText('Беларусь   ставки  рефинансирования  в  период  действия  настоящего',$myOwnStyle);
    $section->addText('договора в одностороннем порядке (примечание);',$myOwnStyle);




/* page 3 */
    $section->addText('     1.4.   получать   от  Заемщика  отчеты,  балансы  и  документы,',$myOwnStyle);
    $section->addText('подтверждающие   его   платежеспособность,  а  также  обеспеченность',$myOwnStyle);
    $section->addText('предоставленных кредитов (примечание) ;',$myOwnStyle);
    $section->addText('     1.5.   требовать   при   долгосрочном  кредитовании  проведения',$myOwnStyle);
    $section->addText('экспертизы   проектных   решений   по   строительству,   расширению,',$myOwnStyle);
    $section->addText('реконструкции  и  техническому  перевооружению  объектов, на которые',$myOwnStyle);
    $section->addText('выдается кредит (примечание);',$myOwnStyle);
    $section->addText('     1.6.  прекращать дальнейшую выдачу кредита, досрочно взыскивать',$myOwnStyle);
    $section->addText('выданные    суммы    кредита    при   возникновении   необеспеченной',$myOwnStyle);
    $section->addText('задолженности  Заемщика  или использовании им выданных средств не по',$myOwnStyle);
    $section->addText('целевому       назначению,       неудовлетворительном       хранении',$myOwnStyle);
    $section->addText('товарно-материальных  ценностей,  являющихся  залоговым обеспечением',$myOwnStyle);
    $section->addText('кредита,  а  также  в  других  случаях  нарушения условий настоящего',$myOwnStyle);
    $section->addText('договора (примечание);',$myOwnStyle);
    $section->addText('     1.7.  повышать  размер процентной ставки за долгосрочный кредит',$myOwnStyle);
    $section->addText('до  ___%  при нарушении нормативных сроков осуществления кредитуемых',$myOwnStyle);
    $section->addText('мероприятий Заемщиком (примечание);',$myOwnStyle);
    $section->addText('     1.8.  при  проведении  мероприятий по оздоровлению деятельности',$myOwnStyle);
    $section->addText('Кредитополучателя: (примечание)',$myOwnStyle);
    $section->addText('     -  понижать  процентные  ставки  либо  не  начислять проценты в',$myOwnStyle);
    $section->addText('течение  определенного  времени  на  всю  или  часть  имеющей  место',$myOwnStyle);
    $section->addText('кредитной  задолженности в соответствии с дополнительным соглашением',$myOwnStyle);
    $section->addText('сторон;',$myOwnStyle);
    $section->addText('     -  отсрочить  уплату  процентов,  аннулировать  часть  или  всю',$myOwnStyle);
    $section->addText('задолженность   по   процентам   в   соответствии  с  дополнительным',$myOwnStyle);
    $section->addText('соглашением сторон;',$myOwnStyle);
    $section->addText('     -   принять   решение   об  обмене  части  или  всей  кредитной',$myOwnStyle);
    $section->addText('задолженности  на акции либо долю в уставном фонде Кредитополучателя',$myOwnStyle);
    $section->addText('в  порядке,  установленном  законодательством Республики Беларусь, в',$myOwnStyle);
    $section->addText('соответствии с дополнительным соглашением сторон;',$myOwnStyle);
    $section->addText('     -  предоставлять рассрочку погашения просроченной задолженности',$myOwnStyle);
    $section->addText('по   кредиту   на   срок   не  более  __________  в  соответствии  с',$myOwnStyle);
    $section->addText('дополнительным соглашением сторон;',$myOwnStyle);
    $section->addText('     1.9.  при  непоступлении средств в погашение кредита и неуплате',$myOwnStyle);
    $section->addText('процентов  за  пользование  кредитом в установленный срок взыскивать',$myOwnStyle);
    $section->addText('кредит  с  начислением  причитающихся  по  нему процентов инкассовым',$myOwnStyle);
    $section->addText('поручением  в  бесспорном  порядке  в  соответствии  с установленной',$myOwnStyle);
    $section->addText('законодательством Республики Беларусь очередностью платежей:',$myOwnStyle);
    $section->addText('     1.9.1.  путем списания денежных средств с текущего (расчетного)',$myOwnStyle);
    $section->addText('счета  Кредитополучателя  -  юридического лица платежным требованием',$myOwnStyle);
    $section->addText('или  мемориальным  ордером  Банка,  а  при  отсутствии  средств  - с',$myOwnStyle);
    $section->addText('текущего (расчетного) счета обособленного подразделения юридического',$myOwnStyle);
    $section->addText('лица (примечание);',$myOwnStyle);
    $section->addText('     1.9.2.  путем списания денежных средств с текущего (расчетного)',$myOwnStyle);
    $section->addText('счета  Кредитополучателя  - обособленного подразделения юридического',$myOwnStyle);
    $section->addText('лица  платежным  требованием  или  мемориальным ордером Банка, а при',$myOwnStyle);
    $section->addText('отсутствии  или недостаточности  средств  -  с текущего (расчетного)',$myOwnStyle);
    $section->addText('счета юридического лица (примечание);',$myOwnStyle);
    $section->addText('     1.9.3.  путем списания денежных средств с текущего (расчетного)',$myOwnStyle);
    $section->addText('счета  Кредитополучателя  в бесспорном порядке платежным требованием',$myOwnStyle);
    $section->addText('Банка  на  основании  исполнительных документов в случаях и порядке,',$myOwnStyle);
    $section->addText('предусмотренных законодательством Республики Беларусь (примечание);',$myOwnStyle);
    $section->addText('     1.9.4.   путем   перечисления   денежных   средств  с  текущего',$myOwnStyle);
    $section->addText('(расчетного)  счета  Кредитополучателя  на  основании  его платежной',$myOwnStyle);
    $section->addText('инструкции (примечание);',$myOwnStyle);
    $section->addText('     1.9.5.  путем  списания  денежных средств платежным требованием',$myOwnStyle);
    $section->addText('или мемориальным ордером Банка с гарантийного депозита, или денежных',$myOwnStyle);
    $section->addText('средств (примечание) ;',$myOwnStyle);
    $section->addText('     1.10.  при  несвоевременном возврате Кредитополучателем кредита',$myOwnStyle);
    $section->addText('или  процентов  по  кредиту  без  акцепта списать с расчетного счета',$myOwnStyle);
    $section->addText('Кредитополучателя непогашенную сумму кредита (примечание) ;',$myOwnStyle);
    $section->addText('     1.11.  приостанавливать  выдачу кредита и/или досрочно взыскать',$myOwnStyle);
    $section->addText('ранее   выданный   кредит   и   начисленные   на   него  проценты  в',$myOwnStyle);
    $section->addText('случае: (примечание)',$myOwnStyle);




/* page 4 */
    $section->addText('     1.11.1. ухудшения финансово-хозяйственного положения Заемщика;',$myOwnStyle);
    $section->addText('     1.11.2. нарушения Заемщиком условий данного договора, договоров',$myOwnStyle);
    $section->addText('залога (поручительства);',$myOwnStyle);
    $section->addText('     1.11.3.  если  кредит  окажется  фактически не обеспеченным или',$myOwnStyle);
    $section->addText('валютонеокупаемым (при выдаче кредита в иностранной валюте);',$myOwnStyle);
    $section->addText('     1.12.  пролонгировать,  соблюдая предельные сроки кредитования,',$myOwnStyle);
    $section->addText('срок возврата кредита (за исключением кредитов на выплату заработной',$myOwnStyle);
    $section->addText('платы),  обеспеченного залогом, гарантией, поручительством и др., на',$myOwnStyle);
    $section->addText('условиях,   определяемых  дополнительным  соглашением  к  настоящему',$myOwnStyle);
    $section->addText('кредитному  договору,  с отнесением данного кредита на счет по учету',$myOwnStyle);
    $section->addText('пролонгированной задолженности;',$myOwnStyle);
    $section->addText('     1.13.   изменить  промежуточные  сроки  погашения  кредита  без',$myOwnStyle);
    $section->addText('совершения  операций  по  переносу  соответствующей части кредита на',$myOwnStyle);
    $section->addText('счет по учету пролонгированной задолженности.',$myOwnStyle);
    $section->addText('     2. Банк обязуется:',$myOwnStyle);
    $section->addText('     2.1.  представлять  сведения  ежемесячно  до ____ числа гаранту',$myOwnStyle);
    $section->addText('(поручителю, залогодателю) и Заемщику о размере процентной ставки по',$myOwnStyle);
    $section->addText('кредиту, выданному Заемщику;',$myOwnStyle);
    $section->addText('     2.2.    сообщить    об    изменении   ставки   рефинансирования',$myOwnStyle);
    $section->addText('Национального   банка  Республики  Беларусь  или  размера  платы  за',$myOwnStyle);
    $section->addText('кредитные  ресурсы  Банка  в  письменном  виде Заемщику для принятия',$myOwnStyle);
    $section->addText('решений   по  изменению  условий  данного  договора  в  течение ____',$myOwnStyle);
    $section->addText('банковских дней;',$myOwnStyle);
    $section->addText('     2.3.  передать  гаранту (поручителю, залогодателю) и Заемщику в',$myOwnStyle);
    $section->addText('течение  ____ дней после заключения данного договора график платежей',$myOwnStyle);
    $section->addText('по  кредитному  договору  №___ от «____» _________» 20__ г. (или по',$myOwnStyle);
    $section->addText('настоящему кредитному договору);',$myOwnStyle);
    $section->addText('     2.4.  проинформировать  гаранта  (поручителя,  залогодателя)  в',$myOwnStyle);
    $section->addText('случае  выявления  нарушений  условий кредитного договора со стороны',$myOwnStyle);
    $section->addText('Заемщика в _____-дневный срок.',$myOwnStyle);
    $section->addText('     3. Заемщик имеет право:',$myOwnStyle);
    $section->addText('     3.1. распоряжаться кредитными средствами по своему усмотрению в',$myOwnStyle);
    $section->addText('соответствии с целями предоставления кредита (примечание);',$myOwnStyle);
    $section->addText('     3.2.  отказаться  от  кредита, предупредив Кредитора об этом не',$myOwnStyle);
    $section->addText('позднее  чем  за _____ дней до срока предоставления денежных средств',$myOwnStyle);
    $section->addText('(примечание);',$myOwnStyle);
    $section->addText('     3.3. вернуть досрочно заемные средства и установленные проценты',$myOwnStyle);
    $section->addText('исходя  из  фактического  срока  пользования средствами Кредитора по',$myOwnStyle);
    $section->addText('соглашению с Кредитором (примечание);',$myOwnStyle);
    $section->addText('     3.4.  использовать  кредит  только в соответствии с его целевым',$myOwnStyle);
    $section->addText('назначением   и   обеспечить   своевременное  погашение  полученного',$myOwnStyle);
    $section->addText('кредита,  а также уплату процентов за пользование им в установленный',$myOwnStyle);
    $section->addText('договором срок;',$myOwnStyle);
    $section->addText('     3.5.  производить  досрочное  погашение  кредита  на  основании',$myOwnStyle);
    $section->addText('платежных инструкций;',$myOwnStyle);
    $section->addText('     3.6.  потребовать  от Банка уплаты неустойки в размере штрафных',$myOwnStyle);
    $section->addText('санкций,    уплаченных    Заемщиком    сторонним   организациям   за',$myOwnStyle);
    $section->addText('несвоевременные расчеты с указанными организациями, допущенные из-за',$myOwnStyle);
    $section->addText('неполного  и несвоевременного предоставления обусловленных настоящим',$myOwnStyle);
    $section->addText('договором кредитов;',$myOwnStyle);
    $section->addText('     3.7.   досрочно   расторгнуть   договор   с   Банком,   погасив',$myOwnStyle);
    $section->addText('задолженность по кредиту и уплатив проценты за пользование кредитом.',$myOwnStyle);
    $section->addText('     4. Заемщик обязуется:',$myOwnStyle);
    $section->addText('     4.1.  погасить  кредит  в сумме ___________________ в следующие',$myOwnStyle);
    $section->addText('сроки: __________________________.',$myOwnStyle);
    $section->addText('         (дата погашения)',$myOwnStyle);
    $section->addText('     Под  датой  погашения  понимается  срок,  когда соответствующая',$myOwnStyle);
    $section->addText('сумма списана со счета Заемщика (примечание);',$myOwnStyle);
    $section->addText('     4.2.  возвратить  сумму  кредита  и  установленные  проценты не',$myOwnStyle);
    $section->addText('позднее «____» _________ 20__ г. (примечание);',$myOwnStyle);
    $section->addText('     4.3. уплатить Кредитору следующее вознаграждение за пользование',$myOwnStyle);




/* page 5 */
    $section->addText('заемными средствами: (примечание)',$myOwnStyle);
    $section->addText('     - в пределах установленного настоящим договором срока погашения',$myOwnStyle);
    $section->addText('кредита - _____% годовых;',$myOwnStyle);
    $section->addText('     - при нарушении срока возврата кредита - _____% годовых за весь',$myOwnStyle);
    $section->addText('период просрочки (до момента фактического возврата кредита);',$myOwnStyle);
    $section->addText('     4.4.  предоставить  обеспечение  возврата банковского кредита в',$myOwnStyle);
    $section->addText('соответствии с положениями п.12 (п.13) раздела V настоящего договора',$myOwnStyle);
    $section->addText('не  позднее  _________  с  момента  подписания  настоящего  договора',$myOwnStyle);
    $section->addText('(примечание);',$myOwnStyle);
    $section->addText('     4.5. завершить мероприятие до ______ и погасить предоставленный',$myOwnStyle);
    $section->addText('Банком кредит в соответствии с требованиями настоящего договора и со',$myOwnStyle);
    $section->addText('срочными обязательствами (примечание);',$myOwnStyle);
    $section->addText('     4.6.  использовать  полученный кредит исключительно по целевому',$myOwnStyle);
    $section->addText('назначению  в  соответствии  с  п.1а  (1.б)  раздела  II  настоящего',$myOwnStyle);
    $section->addText('договора;',$myOwnStyle);
    $section->addText('     4.7.   производить   своевременно  все  расчеты  и  платежи  по',$myOwnStyle);
    $section->addText('кредитному договору;',$myOwnStyle);
    $section->addText('     4.8.  обеспечить ежеквартально до ____ числа месяца, следующего',$myOwnStyle);
    $section->addText('за  отчетным  кварталом,  представление  достоверных сведений о ходе',$myOwnStyle);
    $section->addText('реализации  инвестиционного  проекта,  своей хозяйственно-финансовой',$myOwnStyle);
    $section->addText('деятельности,  балансов  и  приложений к балансу по формам 2, 3 и 5,',$myOwnStyle);
    $section->addText('формы  №  1-МП  и  других  бухгалтерских  документов, подтверждающих',$myOwnStyle);
    $section->addText('целевое   использование   кредита   и   наличие   его  материального',$myOwnStyle);
    $section->addText('обеспечения;',$myOwnStyle);
    $section->addText('     4.9.   предоставлять   возможность   проверки   на  месте  хода',$myOwnStyle);
    $section->addText('реализации инвестиционного проекта.',$myOwnStyle);
    $section->addText(' ',$myOwnStyle);
    $section->addText('                    V. Условия кредитования',$myOwnStyle);
    $section->addText(' ',$myOwnStyle);
    $section->addText('     1.  Заемщик  за  пользование  кредитом  уплачивает  Банку ____%',$myOwnStyle);
    $section->addText('годовых,  что  составляет  _______________ рублей; при возникновении',$myOwnStyle);
    $section->addText('просроченной  задолженности  начиная  с  даты,  следующей  за датой,',$myOwnStyle);
    $section->addText('указанной в п.3а раздела III настоящего договора (по «____» ________',$myOwnStyle);
    $section->addText('200__ г.), и до фактического возврата - ___% годовых (примечание).',$myOwnStyle);
    $section->addText('     2.  Банк  взимает  за  пользование долгосрочным (краткосрочным)',$myOwnStyle);
    $section->addText('кредитом вознаграждение в размере _____% годовых по срочному кредиту',$myOwnStyle);
    $section->addText('и   ___%   при   возникновении просроченной  задолженности  по  этим',$myOwnStyle);
    $section->addText('кредитам.',$myOwnStyle);
    $section->addText('     3.  Процентная  ставка, указанная в п.1 настоящего раздела, при',$myOwnStyle);
    $section->addText('изменении  конъюнктуры  рынка кредитных ресурсов может быть изменена',$myOwnStyle);
    $section->addText('по   дополнительному  соглашению  сторон,  которое  оформляется  как',$myOwnStyle);
    $section->addText('неотъемлемая часть настоящего договора. (примечание)',$myOwnStyle);
    $section->addText('     3.1.  Об  увеличении  процентной  ставки  Банк обязан письменно',$myOwnStyle);
    $section->addText('уведомить Заемщика за ______ дней. (примечание)',$myOwnStyle);
    $section->addText('     3.2.  В  случае  непринятия  письменного  предложения  Заемщика',$myOwnStyle);
    $section->addText('уменьшить процентную ставку Банк обязан письменно уведомить Заемщика',$myOwnStyle);
    $section->addText('в течение _______ дней после получения предложения.',$myOwnStyle);
    $section->addText('     3.3.  В  случае  несогласия  Заемщик  обязан полностью погасить',$myOwnStyle);
    $section->addText('задолженность  по  кредиту,  включая  проценты,  в течение ____ дней',$myOwnStyle);
    $section->addText('после  указанной  в  уведомлении  Банка  даты  (вариант:  «за  время',$myOwnStyle);
    $section->addText('фактического пользования кредитом»).',$myOwnStyle);
    $section->addText('     3.4.  При  возникновении  просроченной задолженности процентная',$myOwnStyle);
    $section->addText('ставка не подлежит уменьшению. (примечание)',$myOwnStyle);
    $section->addText('     4. Начисление процентов начинается:',$myOwnStyle);
    $section->addText('     4.1. с момента заключения договора (примечание);',$myOwnStyle);
    $section->addText('     4.2.  с момента списания и перечисления суммы со счета Банка на',$myOwnStyle);
    $section->addText('счет Заемщика (примечание);',$myOwnStyle);
    $section->addText('     4.3.  с  момента фактического зачисления перечисленной суммы на',$myOwnStyle);
    $section->addText('счет Заемщика (примечание);',$myOwnStyle);
    $section->addText('     4.4. с момента открытия ссудного счета Заемщику (примечание).',$myOwnStyle);
    $section->addText('     5. Начисление процентов заканчивается:',$myOwnStyle);
    $section->addText('     5.1.  на  момент  полного погашения всей суммы задолженности по',$myOwnStyle);
    $section->addText('кредиту (примечание);',$myOwnStyle);
    $section->addText('     5.2. с момента закрытия ссудного счета Заемщика (примечание);',$myOwnStyle);
    $section->addText('     6. Проценты за пользование кредитом начисляются:',$myOwnStyle);
    $section->addText('     6.1. поденно, подекадно, ежемесячно, поквартально (примечание);',$myOwnStyle);
    $section->addText('     6.2. согласно графику (примечание):',$myOwnStyle);





/* page 6 */
    $section->addText('     5.2. с момента закрытия ссудного счета Заемщика (примечание);',$myOwnStyle);
    $section->addText('     6. Проценты за пользование кредитом начисляются:',$myOwnStyle);
    $section->addText('     6.1. поденно, подекадно, ежемесячно, поквартально (примечание);',$myOwnStyle);
    $section->addText('     6.2. согласно графику (примечание):',$myOwnStyle);
    $section->addText('     с  «___» ________ 200_ г. _______________ (сумма в руб.) _____%',$myOwnStyle);
    $section->addText('     по «___» ________ 200_ г.',$myOwnStyle);
    $section->addText('     с  «___» ________ 200_ г. _______________ (сумма в руб.) _____%',$myOwnStyle);
    $section->addText('     по «___» ________ 200_ г.',$myOwnStyle);
    $section->addText('     7.  Первая  выплата  в  сумме _________ руб. (________%) должна',$myOwnStyle);
    $section->addText('быть произведена не позднее «____» ___________ 200_ г.',$myOwnStyle);
    $section->addText('     Последующая выплата производится не позднее «_____» ___________',$myOwnStyle);
    $section->addText('200_ г. или по графику:',$myOwnStyle);
    $section->addText('     срок перечисления ___________, сумма ________ руб. (примечание)',$myOwnStyle);
    $section->addText('     8а.  В  случае досрочного (частичного) возврата кредита Заемщик',$myOwnStyle);
    $section->addText('обязан за ____ дней предупредить об этом Банк, при этом:',$myOwnStyle);
    $section->addText('     -    перерасчет    начисленных    процентов   не   производится',$myOwnStyle);
    $section->addText('(примечание);',$myOwnStyle);
    $section->addText('     -    производится    перерасчет    начисленных    процентов   с',$myOwnStyle);
    $section->addText('соответствующим   уменьшением  суммы,  указанной  в  п.1  раздела  V',$myOwnStyle);
    $section->addText('(примечание);',$myOwnStyle);
    $section->addText('     -   в  случае  если  в  процессе  исполнения  данного  договора',$myOwnStyle);
    $section->addText('обнаружится,  что Заемщик выплатил Банку излишние денежные средства,',$myOwnStyle);
    $section->addText('то излишне выплаченное подлежит возврату в течение ______ банковских',$myOwnStyle);
    $section->addText('дней (примечание);',$myOwnStyle);
    $section->addText('     8б.   Досрочный   (частичный)   возврат  Заемщиком  кредита  не',$myOwnStyle);
    $section->addText('допускается.',$myOwnStyle);
    $section->addText('     9.  Проценты  за  пользование  кредитом  рассчитываются Банком.',$myOwnStyle);
    $section->addText('Расчет заблаговременно (предварительно за _____ дней) представляется',$myOwnStyle);
    $section->addText('Заемщику.',$myOwnStyle);
    $section->addText('     9.1а.  Банк  вправе изменять  размер  процентов  за пользование',$myOwnStyle);
    $section->addText('кредитом, сообщая об этом Кредитополучателю не позднее _____ дней до',$myOwnStyle);
    $section->addText('принятия изменений. (примечание)',$myOwnStyle);
    $section->addText('     9.1б.  Банк  не вправе изменять размер процентов за пользование',$myOwnStyle);
    $section->addText('кредитом. (примечание)',$myOwnStyle);
    $section->addText('     9.1в.  Банк  вправе  изменять  размер  процентов за пользование',$myOwnStyle);
    $section->addText('кредитом только с согласия Заемщика. (примечание)',$myOwnStyle);
    $section->addText('     10.  Если  вносимая  Заемщиком сумма недостаточна для погашения',$myOwnStyle);
    $section->addText('денежного  обязательства  полностью,  то в первую очередь погашаются',$myOwnStyle);
    $section->addText('издержки  Кредитора  по  получению Заемщиком кредита и по исполнению',$myOwnStyle);
    $section->addText('настоящего договора, затем проценты, а оставшаяся сумма обращается в',$myOwnStyle);
    $section->addText('погашение кредита.',$myOwnStyle);
    $section->addText('     11.  При  реорганизации  или  ликвидации  Заемщика  последний в',$myOwnStyle);
    $section->addText('__________ срок  со  дня  принятия решения обязан полностью погасить',$myOwnStyle);
    $section->addText('задолженность,  независимо  от  срока,  на  который был предоставлен',$myOwnStyle);
    $section->addText('кредит.',$myOwnStyle);
    $section->addText('     12. Кредит, предоставленный по данному договору, обеспечивается',$myOwnStyle);
    $section->addText('залогом  (договор  залога  от  «____»  __________  200__  г.)  и/или',$myOwnStyle);
    $section->addText('договором  поручительства  (от  «____» ________ 200__ г.). Указанные',$myOwnStyle);
    $section->addText('договоры   являются   неотъемлемой   частью   настоящего   договора.',$myOwnStyle);
    $section->addText('(примечание)',$myOwnStyle);
    $section->addText('     13.  Возврат сумм, полученных Заемщиком по настоящему договору,',$myOwnStyle);
    $section->addText('обеспечивается поручительством (залогом) ___________________________',$myOwnStyle);
    $section->addText('                                          (наименование поручителя)',$myOwnStyle);
    $section->addText('в соответствии с договором поручительства (залога) ______________ от ',$myOwnStyle);
    $section->addText('«____» ___________ 200__ г.,  заключенного поручителем  с Кредитором ',$myOwnStyle);
    $section->addText('(залогодателем с Кредитором). (примечание)',$myOwnStyle);
    $section->addText('     14.   Кредит   предоставляется   Заемщику  под  обеспечение  (в',$myOwnStyle);
    $section->addText('обеспечение) __________. (примечание)',$myOwnStyle);
    $section->addText('     15.   Отношения   по  залоговому  обеспечению  предоставленного',$myOwnStyle);
    $section->addText('кредита   регулируются   залоговым   соглашением,  который  является',$myOwnStyle);
    $section->addText('неотъемлемой частью настоящего договора. Предметом залога может быть',$myOwnStyle);
    $section->addText('только  имущество,  свободное  от  залога  по  другим обязательствам',$myOwnStyle);
    $section->addText('Заемщика. (примечание)',$myOwnStyle);
    $section->addText('     16.    Кредит,    предоставленный   по   настоящему   договору,',$myOwnStyle);
    $section->addText('обеспечивается залогом (имущества заемщика). (примечание)',$myOwnStyle);
    $section->addText('     17.   Банк   имеет   право   проверять  финансово-хозяйственную',$myOwnStyle);
    $section->addText('деятельность Заемщика на предмет соблюдения:',$myOwnStyle);
    $section->addText('     - условий данного договора;',$myOwnStyle);




/* page 7 */
    $section->addText('     - условий соблюдения договоров акцессорного обеспечения.',$myOwnStyle);
    $section->addText('     18.  Заемщик  обязан  незамедлительно  предоставлять  Банку все',$myOwnStyle);
    $section->addText('документы, необходимые для реализации п.17 настоящего раздела.',$myOwnStyle);
    $section->addText(' ',$myOwnStyle);
    $section->addText('                     VI. Срок действия договора',$myOwnStyle);
    $section->addText(' ',$myOwnStyle);
    $section->addText('     1а. Настоящий договор вступает в силу с момента его подписания.',$myOwnStyle);
    $section->addText('     1б.  Настоящий  договор  вступает  в  силу с момента зачисления',$myOwnStyle);
    $section->addText('кредита  на  расчетный  (ссудный) счет Заемщика или с момента оплаты',$myOwnStyle);
    $section->addText('счета, указанного Заемщиком.',$myOwnStyle);
    $section->addText('     1в. Настоящий договор вступает в силу с момента его заключения.',$myOwnStyle);
    $section->addText('     2а.  Настоящий договор оканчивается с момента полного погашения',$myOwnStyle);
    $section->addText('кредита и уплаты начисленных процентов.',$myOwnStyle);
    $section->addText('     2б.   Настоящий   договор   действует   до  полного  исполнения',$myOwnStyle);
    $section->addText('Кредитополучателем  своих  обязательств по возврату кредита и уплате',$myOwnStyle);
    $section->addText('процентов  за  пользование им, а также иных обязательств, вытекающих',$myOwnStyle);
    $section->addText('из настоящего договора.',$myOwnStyle);
    $section->addText('     2в. Настоящий договор действует до полного исполнения сторонами',$myOwnStyle);
    $section->addText('своих обязательств, вытекающих из положений настоящего договора.',$myOwnStyle);
    $section->addText('     3.   Изменение  условий,  расторжение  и  продление  настоящего',$myOwnStyle);
    $section->addText('договора  может  быть  осуществлено  по  дополнительному  соглашению',$myOwnStyle);
    $section->addText('сторон, оформляемому приложением к настоящему договору.',$myOwnStyle);
    $section->addText(' ',$myOwnStyle);
    $section->addText('                       VII. Дополнительные условия',$myOwnStyle);
    $section->addText(' ',$myOwnStyle);
    $section->addText('     1.    При    недостаточности    средств    на   текущем   счете',$myOwnStyle);
    $section->addText('Кредитополучателя для исполнения обязательств по погашению кредита и',$myOwnStyle);
    $section->addText('уплате  процентов  за  пользование  им  в  первую очередь погашается',$myOwnStyle);
    $section->addText('____________________________________________________.',$myOwnStyle);
    $section->addText('        (указывается основной долг по кредиту',$myOwnStyle);
    $section->addText('          или проценты за пользование им)',$myOwnStyle);
    $section->addText('     2.  При  расчете  процентов  количество дней в году принимается',$myOwnStyle);
    $section->addText('равным ________.',$myOwnStyle);
    $section->addText('     3.  В  случаях, не предусмотренных настоящим договором, стороны',$myOwnStyle);
    $section->addText('руководствуются    Банковским   кодексом   Республики   Беларусь   и',$myOwnStyle);
    $section->addText('Гражданским  кодексом  Республики Беларусь, а также иным действующим',$myOwnStyle);
    $section->addText('(соответствующим) законодательством Республики Беларусь.',$myOwnStyle);
    $section->addText('     4. Стороны обязуются хранить тайну сделки (банковскую тайну).',$myOwnStyle);
    $section->addText('     5.    Изменение   условий   настоящего   договора   оформляется',$myOwnStyle);
    $section->addText('дополнительным  соглашением  сторон,  которое  подписывается  обеими',$myOwnStyle);
    $section->addText('сторонами и является неотъемлемой частью настоящего договора.',$myOwnStyle);
    $section->addText('     6.   Любые   изменения   и  дополнения  к  настоящему  договору',$myOwnStyle);
    $section->addText('действительны,  если  они  совершены  в письменной форме и подписаны',$myOwnStyle);
    $section->addText('уполномоченными на то лицами.',$myOwnStyle);
    $section->addText(' ',$myOwnStyle);
    $section->addText('                    VIII. Ответственность сторон ',$myOwnStyle);
    $section->addText(' ',$myOwnStyle);
    $section->addText('     1. Банк уплачивает Кредитополучателю: (примечание)',$myOwnStyle);
    $section->addText('     1.1.   неустойку  в  размере  _____%  от  суммы  несвоевременно',$myOwnStyle);
    $section->addText('выданного кредита за каждый день просрочки;',$myOwnStyle);
    $section->addText('     1.2. штраф в размере ______% от суммы неправильно начисленных и',$myOwnStyle);
    $section->addText('взысканных процентов за пользование кредитом.',$myOwnStyle);
    $section->addText('     2. Кредитополучатель уплачивает Банку: (примечание)',$myOwnStyle);
    $section->addText('     2.1.  штраф в размере ______% от суммы кредита, использованного',$myOwnStyle);
    $section->addText('не по целевому назначению;',$myOwnStyle);
    $section->addText('     2.2.  повышенную  ставку  за  пользование  кредитом  в  размере',$myOwnStyle);
    $section->addText('______%, начисленную от суммы просроченной задолженности по кредиту.',$myOwnStyle);
    $section->addText('     2.3.   Кредитополучатель   возмещает  Банку  убытки  вследствие',$myOwnStyle);
    $section->addText('неисполнения  (ненадлежащего  исполнения) обязательств по настоящему',$myOwnStyle);
    $section->addText('договору, включая судебные издержки и др. (примечание)',$myOwnStyle);
    $section->addText('     3.  В  случае  невозврата заемных средств в установленные сроки',$myOwnStyle);
    $section->addText('Заемщик   возмещает   Кредитору   причиненные   нарушением   условий',$myOwnStyle);
    $section->addText('кредитного  договора  убытки  в  части,  не  покрытой  процентами за',$myOwnStyle);
    $section->addText('пользование  чужими  денежными  средствами  после  наступления срока',$myOwnStyle);



/* page 8 */
    $section->addText('возврата кредита. (примечание)',$myOwnStyle);
    $section->addText('     4.  В  случае  непоступления  на  счет  Заемщика сумм кредита в',$myOwnStyle);
    $section->addText('установленные настоящим договором сроки Кредитор уплачивает Заемщику',$myOwnStyle);
    $section->addText('неустойку в размере _____% за каждый день просрочки. (примечание)',$myOwnStyle);
    $section->addText('     5. При нарушении сроков погашения кредита и начисленных по нему',$myOwnStyle);
    $section->addText('процентов   Заемщик   платит   Банку   штраф  в  сумме  непогашенной',$myOwnStyle);
    $section->addText('задолженности. (примечание)',$myOwnStyle);
    $section->addText('     6.  В  случае  нарушения  Заемщиком условий настоящего договора',$myOwnStyle);
    $section->addText('взыскание Банком предоставленного кредита и начисленных процентов за',$myOwnStyle);
    $section->addText('пользование  им в соответствии с залоговым правом Банка обращается в',$myOwnStyle);
    $section->addText('судебном порядке на ___________.',$myOwnStyle);
    $section->addText('     7.1.   В   случае   несвоевременного   перечисления   Кредитору',$myOwnStyle);
    $section->addText('задолженности   по   кредиту   и   процентов   за   пользование   им',$myOwnStyle);
    $section->addText('Кредитополучатель  уплачивает  Кредитору  пеню  в размере ______% от',$myOwnStyle);
    $section->addText('несвоевременно   внесенной   суммы   за   каждый   день   просрочки.',$myOwnStyle);
    $section->addText('(примечание)',$myOwnStyle);
    $section->addText('     7.2.  В  случае  задержки перечисления Кредитором оговоренной в',$myOwnStyle);
    $section->addText('п.1а  (п.1б) раздела I настоящего договора суммы Кредитор уплачивает',$myOwnStyle);
    $section->addText('Кредитополучателю   _____________  в  размере  _______________.  При',$myOwnStyle);
    $section->addText('               (штраф, пеню)',$myOwnStyle);
    $section->addText('задержке погашения задолженности  по  кредиту  и  процентам  за него ',$myOwnStyle);
    $section->addText('более чем на _____ дней Кредитор без предварительного предупреждения ',$myOwnStyle);
    $section->addText('взыскивает просроченную задолженность ___________________________ (с ',$myOwnStyle);
    $section->addText('Кредитополучателя, а при отсутствии у него денег - со счета ________ ',$myOwnStyle);
    $section->addText('поручителя  (гаранта) Кредитополучателя   или обращает взыскание  на ',$myOwnStyle);
    $section->addText('заложенное имущество). (примечание)',$myOwnStyle);
    $section->addText(' ',$myOwnStyle);
    $section->addText('           IX. Прочие условия и порядок разрешения споров',$myOwnStyle);
    $section->addText(' ',$myOwnStyle);
    $section->addText('     1.  Перечисление  средств на ссудный счет Заемщика производится',$myOwnStyle);
    $section->addText('Банком   только   после   оформления   акцессорных  обязательств  по',$myOwnStyle);
    $section->addText('обеспечению возврата кредита. (примечание)',$myOwnStyle);
    $section->addText('     2. Стороны обязаны в течение _____ дней письменно сообщать друг',$myOwnStyle);
    $section->addText('другу  обо  всех  обстоятельствах,  могущих  повлиять  на исполнение',$myOwnStyle);
    $section->addText('настоящего   договора,   а   также  об  изменениях  в  учредительных',$myOwnStyle);
    $section->addText('документах,   руководящих   органах  юридического  лица,  адресах  и',$myOwnStyle);
    $section->addText('реквизитах.',$myOwnStyle);
    $section->addText('     3.  Возникающие  в период действия данного договора споры между',$myOwnStyle);
    $section->addText('сторонами разрешаются путем переговоров и консультаций.',$myOwnStyle);
    $section->addText('     4.  В  случае  недостижения согласия спор подлежит рассмотрению',$myOwnStyle);
    $section->addText('хозяйственным     судом     в    соответствии    с    процессуальным',$myOwnStyle);
    $section->addText('законодательством Республики Беларусь.',$myOwnStyle);
    $section->addText('     5.  Для  рассмотрения претензий установлен ____-дневный срок со',$myOwnStyle);
    $section->addText('дня получения претензии.',$myOwnStyle);
    $section->addText('     6.  Настоящий  договор  составлен в двух экземплярах на русском',$myOwnStyle);
    $section->addText('языке. Оба экземпляра имеют одинаковую юридическую силу. У каждой из',$myOwnStyle);
    $section->addText('сторон находится один экземпляр настоящего договора.',$myOwnStyle);
    $section->addText(' ',$myOwnStyle);
    $section->addText('          X. Юридические адреса и банковские реквизиты сторон',$myOwnStyle);
    $section->addText(' ',$myOwnStyle);
    $section->addText('     Банк',$myOwnStyle);
    $section->addText(' ',$myOwnStyle);
    $section->addText('     Наименование: __________________________',$myOwnStyle);
    $section->addText('     Адрес: _________________________________',$myOwnStyle);
    $section->addText('     Банковские реквизиты: __________________',$myOwnStyle);
    $section->addText('     Тел. (факс): ___________________________',$myOwnStyle);
    $section->addText(' ',$myOwnStyle);
    $section->addText('     Заемщик',$myOwnStyle);
    $section->addText(' ',$myOwnStyle);
    $section->addText('     Наименование: __________________________',$myOwnStyle);
    $section->addText('     Адрес: _________________________________',$myOwnStyle);
    $section->addText('     Банковские реквизиты: __________________',$myOwnStyle);
    $section->addText('     Тел. (факс): ___________________________',$myOwnStyle);
    $section->addText(' ',$myOwnStyle);




/* page 9 */
    $section->addText(' ',$myOwnStyle);
    $section->addText('     Банк   _______________         Заемщик ______________',$myOwnStyle);
    $section->addText('    (подпись)                       (подпись)',$myOwnStyle);
    $section->addText('М.П.                             М.П.',$myOwnStyle);
    $section->addText(' ',$myOwnStyle);
    $section->addText('     Примечание.   Данный   пункт  (данные  пункты)  применяется  по',$myOwnStyle);
    $section->addText('усмотрению составителей договора (сторон).',$myOwnStyle);
    $section->addText(' ',$myOwnStyle);
    $section->addText('                           КОММЕНТАРИЙ',$myOwnStyle);
    $section->addText(' ',$myOwnStyle);
    $section->addText('     Предметом   кредитного  договора  могут  быть  только  денежные',$myOwnStyle);
    $section->addText('средства, выраженные как в национальной, так и в иностранной валюте.',$myOwnStyle);
    $section->addText('При  этом  кредиты  в иностранной валюте предоставляются юридическим',$myOwnStyle);
    $section->addText('лицам    преимущественно   на   валютоокупаемые   проекты.   Кредит,',$myOwnStyle);
    $section->addText('предоставленный   в   иностранной   валюте,  должен  соответствовать',$myOwnStyle);
    $section->addText('требованиям  действующего  на  момент  заключения договора валютного',$myOwnStyle);
    $section->addText('законодательства  Республики  Беларусь.  Валютный кредит юридическим',$myOwnStyle);
    $section->addText('лицам  предоставляется,  как  правило,  в  безналичном порядке через',$myOwnStyle);
    $section->addText('текущие валютные счета типа «Т».',$myOwnStyle);
    $section->addText('     При  наличии  у  банка  ресурсов для кредитования в иностранной',$myOwnStyle);
    $section->addText('валюте   и  необходимости  использования  кредита  для  расчетов  за',$myOwnStyle);
    $section->addText('кредитуемые  ценности  в другой валюте допускаются за счет кредитных',$myOwnStyle);
    $section->addText('средств  купля-продажа  иностранной валюты или белорусских рублей, а',$myOwnStyle);
    $section->addText('также  конверсия  иностранной  валюты  в  иную  иностранную валюту в',$myOwnStyle);
    $section->addText('соответствии  с законодательством Республики Беларусь с направлением',$myOwnStyle);
    $section->addText('денежных средств на цели, предусмотренные в кредитном договоре.',$myOwnStyle);
    $section->addText('     Форма   кредитного  договора  всегда  письменная.  Если  кредит',$myOwnStyle);
    $section->addText('предоставляется  под залог недвижимости, то кредитный договор должен',$myOwnStyle);
    $section->addText('быть    нотариально   удостоверен   и   зарегистрирован,   поскольку',$myOwnStyle);
    $section->addText('Гражданский  кодекс  Республики  Беларусь (ГК) требует нотариального',$myOwnStyle);
    $section->addText('удостоверения договоров подобного типа, а именно:',$myOwnStyle);
    $section->addText('     - договоров об ипотеке (ст.320 ГК);',$myOwnStyle);
    $section->addText('     -  договоров залога движимого имущества или прав на имущество в',$myOwnStyle);
    $section->addText('обеспечение   обязательств   по   договору,   который   должен  быть',$myOwnStyle);
    $section->addText('нотариально удостоверен (ст.320 ГК).',$myOwnStyle);
    $section->addText('     Несоблюдение  этого  условия  будет  расценено как несоблюдение',$myOwnStyle);
    $section->addText('формы кредитного договора, что всегда ведет к его недействительности',$myOwnStyle);
    $section->addText('(за  исключением случаев, предусмотренных ст.141 Банковского кодекса',$myOwnStyle);
    $section->addText('Республики Беларусь (БК)).',$myOwnStyle);
    $section->addText('     К  содержанию  кредитного  договора  предъявляются определенные',$myOwnStyle);
    $section->addText('требования. Он обязательно должен содержать сведения о:',$myOwnStyle);
    $section->addText('     - сумме кредита (с указанием валюты кредита);',$myOwnStyle);
    $section->addText('     - процентах за пользование кредитом и порядке их уплаты;',$myOwnStyle);
    $section->addText('     - целевом использования кредита;',$myOwnStyle);
    $section->addText('     - сроках и порядке предоставления и погашения кредита;',$myOwnStyle);
    $section->addText('     -  способах  обеспечения  исполнения обязательств по кредитному',$myOwnStyle);
    $section->addText('договору;',$myOwnStyle);
    $section->addText('     - ответственности кредитора и кредитополучателя за невыполнение',$myOwnStyle);
    $section->addText('условий договора (ст.142 БК).',$myOwnStyle);
    $section->addText('     Таким  образом,  невключение  вышеперечисленных условий в текст',$myOwnStyle);
    $section->addText('договора  будет  свидетельствовать о его фактическом незаключении со',$myOwnStyle);
    $section->addText('всеми вытекающими отсюда последствиями.',$myOwnStyle);
    $section->addText('     Срок  возврата  кредита  дополнительно оговаривается сторонами.',$myOwnStyle);
    $section->addText('Кредит  может  быть погашен единовременно или частями. Заемщик может',$myOwnStyle);
    $section->addText('возвратить  полученный кредит до истечения срока возврата, однако он',$myOwnStyle);
    $section->addText('должен  заранее  известить  об этом обслуживающий банк и получить на',$myOwnStyle);
    $section->addText('это  согласие  банка-кредитора,  в  случае если обслуживающий банк и',$myOwnStyle);
    $section->addText('банк-кредитор не совпадают.',$myOwnStyle);
    $section->addText('     Проценты  за  пользование  кредитными  средствами начисляются с',$myOwnStyle);
    $section->addText('момента  их  поступления  на  счет  заемщика,  а не с момента (даты)',$myOwnStyle);
    $section->addText('заключения  договора, поскольку они выплачиваются за время реального',$myOwnStyle);
    $section->addText('пользования кредитом (ст.147 БК).',$myOwnStyle);
    $section->addText('     Предоставление  кредита является прямой обязанностью кредитора,',$myOwnStyle);
    $section->addText('подписавшего  кредитный  договор.  Он  вправе  не  выполнить  данное',$myOwnStyle);




/* page 10 */
    $section->addText('обязательство  лишь  в  случае признания заемщика неплатежеспособным',$myOwnStyle);
    $section->addText('либо  при  наличии  доказательств (оснований), что заемщик не сможет',$myOwnStyle);
    $section->addText('вернуть   кредитные  средства  и  установленные  договором  проценты',$myOwnStyle);
    $section->addText('(ст.143 БК).',$myOwnStyle);
    $section->addText('     Кредитор  вправе  отказаться  от заключения кредитного договора',$myOwnStyle);
    $section->addText('при   непредоставлении   кредитополучателем   обеспечения  погашения',$myOwnStyle);
    $section->addText('кредита,   при  возбуждении  процедуры  признания  кредитополучателя',$myOwnStyle);
    $section->addText('экономически   несостоятельным   (банкротом)   и  при  наличии  иных',$myOwnStyle);
    $section->addText('оснований, предусмотренных законодательством Республики Беларусь.',$myOwnStyle);
    $section->addText('     В   соответствии   с   заключенным   договором  заемщик  вправе',$myOwnStyle);
    $section->addText('потребовать   предоставления   кредита.  В  то  же  время  он  может',$myOwnStyle);
    $section->addText('отказаться от получения кредита полностью или частично, если иное не',$myOwnStyle);
    $section->addText('предусмотрено законодательством или условиями договора.',$myOwnStyle);
    $section->addText('     В  этом  случае  Заемщик  обязан  уведомить Кредитора об отказе',$myOwnStyle);
    $section->addText('получения   кредита   до   наступления   срока  его  предоставления,',$myOwnStyle);
    $section->addText('установленного в договоре (ст.144 БК).',$myOwnStyle);
    $section->addText('     В  кредитном  договоре  может  быть  указан минимальный срок до',$myOwnStyle);
    $section->addText('наступления  момента  предоставления  кредитных  сумм,  по истечении',$myOwnStyle);
    $section->addText('которого  отказ  заемщика  от получения кредита не имеет юридической',$myOwnStyle);
    $section->addText('силы.  Договор  может  также  предусматривать  обязанность  заемщика',$myOwnStyle);
    $section->addText('возместить  кредитору  убытки,  вызванные  односторонним  изменением',$myOwnStyle);
    $section->addText('договора.',$myOwnStyle);
    $section->addText('     Заемщик  обязан  возвратить  кредитору полученную сумму займа и',$myOwnStyle);
    $section->addText('установленные  проценты  в  срок,  предусмотренный  договором. Им же',$myOwnStyle);
    $section->addText('определяется   порядок  возврата  суммы  займа  (ежемесячно  равными',$myOwnStyle);
    $section->addText('долями,  по  окончании  займа,  иным  образом).  При отсутствии этих',$myOwnStyle);
    $section->addText('условий  вся  сумма  займа  и  установленные  проценты  должны  быть',$myOwnStyle);
    $section->addText('возращены   не   позднее   30  дней  со  дня  предъявления  заемщику',$myOwnStyle);
    $section->addText('соответствующих требований (ст.763 ГК).',$myOwnStyle);
    $section->addText('     Эта   обязанность   считается   выполненной   после  зачисления',$myOwnStyle);
    $section->addText('соответствующих  средств  на банковский счет кредитора, если иное не',$myOwnStyle);
    $section->addText('предусмотрено  договором  (п.3 ст.763 ГК). Списание денежных средств',$myOwnStyle);
    $section->addText('со  счета  заемщика не освобождает его от ответственности за возврат',$myOwnStyle);
    $section->addText('суммы займа, если эти средства не поступили на счет кредитора.',$myOwnStyle);
    $section->addText('     Одним  из  условий  кредитного  договора  является  обязанность',$myOwnStyle);
    $section->addText('заемщика   по  обеспечению  возврата  кредитных  средств.  В  случае',$myOwnStyle);
    $section->addText('нарушения  этого  условия  кредитор (заимодавец) (ст.766 ГК) вправе,',$myOwnStyle);
    $section->addText('если   иное   не   установлено   договором,  потребовать  досрочного',$myOwnStyle);
    $section->addText('возвращения кредитных средств и предусмотренных процентов.',$myOwnStyle);
    $section->addText('     В  договоре  могут быть определены различные варианты погашения',$myOwnStyle);
    $section->addText('кредита.  Кроме  того,  стороны  вправе  предусмотреть  в  кредитном',$myOwnStyle);
    $section->addText('договоре  порядок,  при  котором  проценты  за  пользование кредитом',$myOwnStyle);
    $section->addText('уплачиваются  полностью  в  день  возврата  кредита или равномерными',$myOwnStyle);
    $section->addText('взносами в период его погашения.',$myOwnStyle);
    $section->addText('     Кредитор при заключении кредитного договора с каждым конкретным',$myOwnStyle);
    $section->addText('кредитополучателем  самостоятельно  определяет  размер  процентов за',$myOwnStyle);
    $section->addText('пользование  кредитом  (исключение:  кредит  предоставляется за счет',$myOwnStyle);
    $section->addText('бюджетных   средств   либо   в   предусмотренном   законодательством',$myOwnStyle);
    $section->addText('Республики   Беларусь   порядке   за  счет  других  централизованных',$myOwnStyle);
    $section->addText('ресурсов,  когда размер процентов за пользование кредитом определяет',$myOwnStyle);
    $section->addText('сам распорядитель указанных средств).',$myOwnStyle);
    $section->addText('     Невыполнение   обязательств  по  обеспечению  возврата  кредита',$myOwnStyle);
    $section->addText('является основанием для его досрочного взыскания. Поэтому независимо',$myOwnStyle);
    $section->addText('от  того,  зафиксировано  ли  соответствующее  положение в кредитном',$myOwnStyle);
    $section->addText('договоре,  банк  или иной кредитор (небанковская финансово-кредитная',$myOwnStyle);
    $section->addText('организация)   в   случаях   обнаружения  недостоверных  сведений  в',$myOwnStyle);
    $section->addText('документах,  при  невнесении  или  несвоевременном внесении задатка,',$myOwnStyle);
    $section->addText('предусмотренного  кредитным  договором, невыполнении обязательств по',$myOwnStyle);
    $section->addText('договору  залога,  связанному  с  кредитным  договором,  и  в других',$myOwnStyle);
    $section->addText('подобных      случаях,      свидетельствующих     о     неисполнении',$myOwnStyle);
    $section->addText('кредитополучателем   обязательств  по  кредитному  договору,  вправе',$myOwnStyle);
    $section->addText('требовать  от  должника  досрочного  возврата кредита или досрочного',$myOwnStyle);
    $section->addText('взыскания   его   в   судебном   порядке   при  отказе  должника  от',$myOwnStyle);
    $section->addText('добровольного   возвращения   финансовых   средств,   полученных  по',$myOwnStyle);




/* page 11 */
    $section->addText('кредитному  договору,  либо  банк  вправе  отказаться от дальнейшего',$myOwnStyle);
    $section->addText('кредитования   кредитополучателя   по  данному  кредитному  договору',$myOwnStyle);
    $section->addText('(ст.145 БК).',$myOwnStyle);
    $section->addText(' ',$myOwnStyle);
    $section->addText('24.09.2002 г.',$myOwnStyle);
    $section->addText(' ',$myOwnStyle);
    $section->addText('Валерий Захаров, консультант юридического управления',$myOwnStyle);
    $section->addText('Министерства экономики Республики Беларусь',$myOwnStyle);
    $section->addText(' ',$myOwnStyle);
    $section->addText('От редакции: В Гражданский кодекс Республики  Беларусь от 07.12.1998',$myOwnStyle);
    $section->addText('№ 218-3  на  основании  Закона  Республики  Беларусь  от  18.08.2004',$myOwnStyle);
    $section->addText('№ 316-З внесены дополнения и изменения.',$myOwnStyle);
    $section->addText('',$myOwnStyle);
    $section->addText('С 27 октября 2006 г. Банковский кодекс Республики Беларусь от',$myOwnStyle);
    $section->addText('',$myOwnStyle);
    $section->addText('25.10.2000 № 441-З на основании Закона Республики Беларусь от',$myOwnStyle);
    $section->addText('',$myOwnStyle);
    $section->addText('17.07.2006 № 145-З «О внесении изменений и дополнений в Банковский',$myOwnStyle);
    $section->addText('',$myOwnStyle);
    $section->addText('кодекс Республики Беларусь» изложен в новой редакции.',$myOwnStyle);
    $section->addText('',$myOwnStyle);


    $title = $loan_title.' - '.$last_name. ' ' .$first_name. ' ' .$father_name. ' - ' .$passport ;
    $url = './wp-content/themes/behoof/tmp/'.$title.'.docx';


//// At least write the document to webspace:
    $objWriter = PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');
//    //$objWriter->save('helloWorld.docx');
    $objWriter->save(ABSPATH . 'wp-content'. DS .'themes'. DS .'behoof'. DS .'tmp'. DS. '' . $title . '.docx');


    header("Content-type: application/json; charset=UTF-8");
    exit(json_encode(array(
            'title' => $title,
            'url' => $url
        )
    ));
}


add_action('wp_ajax_add_personal_cabinet', 'prefix_ajax_add_personal_cabinet');
add_action('wp_ajax_nopriv_add_personal_cabinet', 'prefix_ajax_add_personal_cabinet');

function prefix_ajax_add_personal_cabinet(){
    global $wpdb;

    if(isset($_POST['url']) && !empty($_POST['url'])) $url = $_POST['url']; else $url = '';
    if(isset($_POST['first_name']) && !empty($_POST['first_name'])) $first_name = $_POST['first_name']; else $first_name = '';
    if(isset($_POST['last_name']) && !empty($_POST['last_name'])) $last_name = $_POST['last_name']; else $last_name = '';
    if(isset($_POST['father_name']) && !empty($_POST['father_name'])) $father_name = $_POST['father_name']; else $father_name = '';
    if(isset($_POST['passport']) && !empty($_POST['passport'])) $passport = $_POST['passport']; else $passport = '';
    if(isset($_POST['email']) && !empty($_POST['email'])) $email = $_POST['email']; else $email = '';
    if(isset($_POST['loan_id']) && !empty($_POST['loan_id'])) $loan_id = $_POST['loan_id']; else $loan_id = '';

    if(isset($_POST['loanSumm']) && !empty($_POST['loanSumm'])) $loan_summ = $_POST['loanSumm']; else $loan_summ = '';
    if(isset($_POST['percent']) && !empty($_POST['percent'])) $percent = $_POST['percent']; else $percent = '';
    if(isset($_POST['belky']) && !empty($_POST['belky'])) $belky = $_POST['belky']; else $belky = 'false';
    if(isset($_POST['along']) && !empty($_POST['along'])) $long = $_POST['along']; else $long = false;
    if(isset($_POST['from']) && !empty($_POST['from'])) $from = $_POST['from']; else $from = '';
    if(isset($_POST['to']) && !empty($_POST['to'])) $to = $_POST['to']; else $to = '';
    if(isset($_POST['aim']) && !empty($_POST['aim'])) $aim = $_POST['aim']; else $aim = '';

    if($belky == 'true'){
        $valuta = 'бел.руб';
    }else{
        $valuta = 'USD';
    }
    $timestamp_start = strtotime($from);
    $start_date = date('Y-m-d',$timestamp_start);

    $percent = preg_replace('/\%/', '', $percent);
    $percent = str_replace(',', '.', $percent);
    $percent = (float) $percent;
    
    $to = preg_replace('/[а-я]/', '', $to);

    $loan_summ = (int) str_replace(" ","",$loan_summ) ;
    
    $col_months = $to * 12;
    
    $payment_arr = get_all_payment_data($to, $loan_summ, $percent);


    $total_summ = $payment_arr['total_summ'];
    $percent_per_month = $payment_arr['percent_per_month'];
    $main_summ_per_month = $payment_arr['main_summ_per_month']; 
    
    $loan_post = get_post($loan_id);
    $loan_title = $loan_post->post_title;

    $post_id = url_to_postid( $url );

    $category_id = get_category_by_slug( 'responses' )->term_id;

    // создадим нового пользователя
    // отошлем ему письмо с пользовательскими данными и ссылкой на личный кабинет

    $random_pswd = get_random_password();

    $user_name = implode('-',explode(' ', $passport));

    $user_id = username_exists( $user_name );
    if(is_null($user_id)){
        $args = array(
            'user_login' => $user_name,
            'user_pass' => md5($random_pswd),
            'user_nicename' => $user_name,
            'user_email' => $email,
            'user_url' => '',
            'user_registered' => date('Y-m-d H:i:s'),
            'user_activation_key' => $user_name,
            'user_status' =>0,
            'display_name' => $user_name
        );

        $wpdb->insert($wpdb->prefix . 'users', $args);
        $user_id = username_exists( $user_name );
        wp_update_user( array ( 'ID' => $user_id, 'role' => 'client' , 'first_name'=>$first_name, 'last_name'=>$last_name, 'display_name'=>$last_name . ' ' .$first_name) ) ;

        $email_message = 'Здравствуйте, ' . $last_name . ' ' . $first_name . ' ' . $father_name . '. Мы рады приветсвовать Вас в числе наших клиентов! Для Вас был создан Личный Кабинет на сайте Behoof.corp, с помощью которого, Вы сможете отслеживать и вовремя погашать платежи по кредиту. Необходимая информация для
            получения доступа в Ваш личный кабинет:
            Имя пользователя - '.$user_name.',
            Пароль - '.$random_pswd.',
            Адрес электронной почты - '. $email.'.
        Компания Behoof поздравляет Ваc и желает приятного сотрудничества!';
    }else{
        $email_message = 'Здравствуйте. Вы уже являетесь клиентом нашего банка, хотим лишь уведомить Вас об успешном заключении нового договора по кредиту! Спасибо за сотрудничество';
    }

    $recipients = mailusers_get_recipients_from_ids($user_id, '');
    $subject = 'Behoof Loans';
    $type='plaintext';
    $sender_name = 'Behoof company';
    $sender_email = 'behoof@gmail.com';

    mailusers_send_mail($recipients, $subject, $email_message, $type, $sender_name, $sender_email);

   // изменим категорию посту с нужным айдишником

    wp_set_post_categories( $post_id, $category_id );
    wp_set_post_tags( $post_id, $loan_title );
    wp_update_post( array ( 'ID' => $post_id, 'post_author' => $user_id ) ) ;

    // создать новый ивент в календаре как день, когда был заключен договор и автором выставить клиента

    $sql = "INSERT INTO ".WP_CALENDAR_TABLE." SET loan_id=".(int)$loan_id.", event_author='".$user_id."', event_begin='".$start_date."', event_end='".$start_date."', event_time='".date("H:i:s")."' , event_title='Платеж', event_desc='Кредит: ".$loan_title." : Сумма платежа: ".$main_summ_per_month." ".$valuta.": ".$percent_per_month." ".$valuta."  ', event_category='2' , event_recur='M', event_repeats='".$col_months."', 
           event_summ=".$loan_summ.", event_currency = '".$valuta."', event_percentage=".$percent.", event_all_summ=".$total_summ.", event_main_permonth=".$main_summ_per_month.", event_percent_permonth=".$percent_per_month.", event_col_months=".$col_months."   ";
    $wpdb->get_results($sql);


    $message = 'Личный кабинет пользователя '.$user_name.' был успешно создан.  ';
    $message .= '<a href="'. get_home_url() .'">Вернуться на домашнюю страницу.</a>';

    header("Content-type: application/json; charset=UTF-8");
    exit(json_encode(array(
            'message'=>$message,
            'success'=>true
        )
    ));

}


add_action('wp_ajax_edit_profile', 'prefix_ajax_edit_profile');
add_action('wp_ajax_nopriv_edit_profile', 'prefix_ajax_edit_profile');

function prefix_ajax_edit_profile(){
    global $wpdb;

    if(isset($_POST['first_name']) && !empty($_POST['first_name'])) $first_name = $_POST['first_name']; else $first_name = '';
    if(isset($_POST['last_name']) && !empty($_POST['last_name'])) $last_name = $_POST['last_name']; else $last_name = '';
    if(isset($_POST['password']) && !empty($_POST['password'])) $password = $_POST['password']; else $password = '';

    $user_id = get_current_user_id();
    if($password = ''){
        wp_update_user( array ( 'ID' => $user_id, 'role' => 'client' , 'first_name'=>$first_name, 'last_name'=>$last_name, 'display_name'=>$last_name . ' ' .$first_name) ) ;
    }else{
        wp_update_user( array ( 'ID' => $user_id, 'role' => 'client' , 'first_name'=>$first_name, 'last_name'=>$last_name, 'display_name'=>$last_name . ' ' .$first_name, 'password'=>md5($password)) ) ;
    }   

    return false;
}



add_action('wp_ajax_payment', 'prefix_ajax_payment');
add_action('wp_ajax_nopriv_payment', 'prefix_ajax_payment');

function prefix_ajax_payment(){
    global $wpdb;

    if(isset($_POST['id']) && !empty($_POST['id'])) $id = $_POST['id']; else $id = '';
    
    $part = false;
    
    if(isset($_POST['pay_main_permonth']) && !empty($_POST['pay_main_permonth']))  {$pay_main_permonth = (int)$_POST['pay_main_permonth']; $part = true;} else $pay_main_permonth = 0;
    if(isset($_POST['pay_percent_permonth']) && !empty($_POST['pay_percent_permonth'])) {$pay_percent_permonth = (int)$_POST['pay_percent_permonth']; $part = true; } else $pay_percent_permonth = 0;
    if(isset($_POST['pay_rate']) && !empty($_POST['pay_rate'])) {$pay_rate = (int)$_POST['pay_rate']; $part = true; }else $pay_rate = 0;

    // проверка на то, не все ли оплатил пользователь
//    $cur_event_main_permonth = $wpdb->get_results("SELECT event_main_permonth FROM ".WP_CALENDAR_TABLE." WHERE event_id=".$id." ");
//    $cur_event_percent_permonth = $wpdb->get_results("SELECT event_percent_permonth FROM ".WP_CALENDAR_TABLE." WHERE event_id=".$id." ");
    
    
    if($part){
        $cur_event_main_permonth_part = $wpdb->get_results("SELECT event_main_permonth_part FROM ".WP_CALENDAR_TABLE." WHERE event_id=".$id." ");
        $cur_event_percent_permonth_part = $wpdb->get_results("SELECT event_percent_permonth_part FROM ".WP_CALENDAR_TABLE." WHERE event_id=".$id." ");   
        $cur_event_rate_part = $wpdb->get_results("SELECT event_rate_part FROM ".WP_CALENDAR_TABLE." WHERE event_id=".$id." "); 
        
        $pay_main_permonth += $cur_event_main_permonth_part[0]->event_main_permonth_part;
        $pay_percent_permonth += $cur_event_percent_permonth_part[0]->event_percent_permonth_part;
        $pay_rate += $cur_event_rate_part[0]->event_rate_part;
        
        $wpdb->get_results("UPDATE ".WP_CALENDAR_TABLE." SET event_main_permonth_part=".$pay_main_permonth.", event_percent_permonth_part=".$pay_percent_permonth.", event_rate_part=".$pay_rate." WHERE event_id='".$id."' ");
 
    }else{
        pay_for_tis_month($id);
   }

    return false;
}

function pay_for_tis_month($id){
    global $wpdb;
    $cur_event_repeats = $wpdb->get_results("SELECT event_repeats FROM ".WP_CALENDAR_TABLE." WHERE event_id=".$id." ");

    $event_repeats = $cur_event_repeats[0]->event_repeats - 1;
    $cur_date = date('Y-m-d');
    $wpdb->get_results("UPDATE ".WP_CALENDAR_TABLE." SET event_begin='".$cur_date."', event_end='".$cur_date."', event_repeats='".$event_repeats."', event_main_permonth_part=0, event_percent_permonth_part=0, event_rate_part=0 WHERE event_id='".$id."' ");

    return true;
}



add_action('wp_ajax_remove_contract', 'prefix_ajax_remove_contract');
add_action('wp_ajax_nopriv_remove_contract', 'prefix_ajax_remove_contract');

function prefix_ajax_remove_contract(){
    global $wpdb;

    if(isset($_POST['id']) && !empty($_POST['id'])) $id = $_POST['id']; else $id = '';
    $event_id = explode('-',$id)[0];
    $contract_id = explode('-',$id)[1];

    $wpdb->get_results("DELETE FROM ".WP_CALENDAR_TABLE." WHERE event_id=".$event_id." ");
    //wp_delete_post( $contract_id, true );
    $query = 'UPDATE wp_posts SET post_excerpt = "Extinguished", post_modified = "'.date('Y-m-d', time()).'"  WHERE ID = "' . $contract_id .'"';

    $wpdb->query($query);

    return false;
}



add_action('wp_ajax_send_message', 'prefix_ajax_send_message');
add_action('wp_ajax_nopriv_send_message', 'prefix_ajax_send_message');

function prefix_ajax_send_message(){
    global $wpdb;

    if(isset($_POST['user_id']) && !empty($_POST['user_id'])) $user_id = $_POST['user_id']; else $user_id = '';
    if(isset($_POST['message']) && !empty($_POST['message'])) $email_message = $_POST['message']; else $email_message = '';


    $message = 'Сообщение было успешно отослано.  ';


    $recipients = mailusers_get_recipients_from_ids($user_id, '');
    $subject = 'Behoof Loans';
    $type='plaintext';
    $sender_name = 'Behoof company';
    $sender_email = 'behoof@gmail.com';
    //var_dump($user_id,$email_message ,$recipients);

    mailusers_send_mail($recipients, $subject, $email_message, $type, $sender_name, $sender_email);

    return false;
}

add_action('wp_ajax_ban_user', 'prefix_ajax_ban_user');
add_action('wp_ajax_nopriv_ban_user', 'prefix_ajax_ban_user');
function prefix_ajax_ban_user(){
    require_once'database.php';
    create_table();
    
    global $wpdb;

    if(isset($_POST['user_id']) && !empty($_POST['user_id'])) $user_id = $_POST['user_id']; else $user_id = '';
    
    $user_data = get_userdata( $user_id );
    $user_login = $user_data->user_login;
    $user_email = $user_data->user_email;
    $user_last_name = explode(' ', $user_data->display_name)[0];
    $user_first_name = explode(' ', $user_data->display_name)[1];
 
   
    $wpdb->insert(
	'wp_blacklist',
	array(
            'last_id' => $user_id,
            'user_login' => $user_login,
            'user_email' => $user_email,
            'user_first_name' => $user_first_name,
            'user_last_name' => $user_last_name,
            'date' => date('Y-m-d')
	),
	array(
                '%d',
		'%s',
		'%s',
                '%s',
                '%s',
                '%s'
	)
        );

//    wp_delete_user( $user_id, 1 );
    
    return false;
}


function form_validation($serial_number){
    global $wpdb;
    $result = true;
    
    $sql = 'SELECT user_login FROM wp_blacklist';
    $black_ids = $wpdb->get_results($sql);
    $black_array = array();
    foreach($black_ids as $key=>$bi){
        $black_array[] =  $bi->user_login;
    }
    if(in_array($serial_number, $black_array)) $result = false;
    
    return $result;
}



function get_advanced_repayment($event_id ){
    global $wpdb;
    $sql = 'SELECT * FROM wp_calendar WHERE event_id = '.$event_id.' ';
    $event = $wpdb->get_results($sql)[0];    
//    if($event->event_repeats <= event_col_months/2){
        $payment_arr = get_all_payment_data( ($event->event_col_months)/12, $event->event_summ, ($event->event_percentage)+7);
        
        $total_summ = $payment_arr['total_summ'];
        $result = ($event->event_repeats * $total_summ) / $event->event_col_months;
//    }else{
//        $result = null;
//    }

    return $result; 
}

















// ф-ии для статистики

function get_active_contracts($category_id, $date_str){   
    global $wpdb;
    $cur_month = date('m');
    $cur_year = date('y');
    $date_func = '';
    
    switch($date_str){
        case 'month':
            $date_func = '( ( MONTH( post_date ) = '.$cur_month.' ) )';
            break;
        case 'year':
            $date_func = '( ( YEAR( post_date ) = '.$cur_year.' ) )';
            break;   
        case 'all_period':
            $date_func = '';
            break;  
        default:
            break;
    }
    
    $activeSql = 'SELECT SQL_CALC_FOUND_ROWS * FROM wp_posts  INNER JOIN wp_term_relationships 
    ON (wp_posts.ID = wp_term_relationships.object_id) WHERE '.$date_func.'
    AND post_excerpt = "Approved" AND ( wp_term_relationships.term_taxonomy_id IN ('.$category_id.') ) 
    AND wp_posts.post_type = "post" AND (wp_posts.post_status = "publish")
    GROUP BY wp_posts.ID ORDER BY wp_posts.post_date DESC LIMIT 0, 10';
    
    $activeMonthContracts = $wpdb->get_results($activeSql);
    
    return $activeMonthContracts;
}

function get_added_contracts($category_id, $date_str){
    $cur_month = date('m');
    $cur_year = date('Y');
    $date_func = '';
    
    switch($date_str){
        case 'month':
            $date_func = '&monthnum='.$cur_month.'&year='.$cur_year;
            break;
        case 'year':
            $date_func = 'year';
            break;   
        case 'all_period':
            $date_func = '';
            break;  
        default:
            break;
    }    
    
    if($date_func == 'year'){
        $addedMonthContracts = array();
        for($i = 1; $i<12; $i++){
            $addedMonthContracts[$i] = count(query_posts('cat='.$category_id.'&monthnum='.$i.'&year='.$cur_year));
        }
        $addedMonthContracts = implode(',',$addedMonthContracts);
    }else{
        $addedMonthContracts = query_posts('cat='.$category_id.''.$date_func);
    }        
    
    return $addedMonthContracts;
}


function get_overdued_contracts($category_id, $date_str){
    global $wpdb;
    $cur_month = date('m');
    $cur_year = date('Y');
    $cur_date = date('Ym');
    $date_func = '';    
    
    switch($date_str){
        case 'month':
            $date_func = '( ( MONTH( post_date ) = '.$cur_month.' ) ) AND ( ( YEAR( post_date ) = '.$cur_year.' ) ) AND';
            break;
        case 'year':
            $date_func = '( ( YEAR( post_date ) = '.$cur_year.' ) )';
            break;   
        case 'all_period':
            $date_func = '';
            break;  
        default:
            break;
    }    
    
    $postAuthorsSql = 'SELECT SQL_CALC_FOUND_ROWS post_author FROM wp_posts  INNER JOIN wp_term_relationships 
        ON (wp_posts.ID = wp_term_relationships.object_id) WHERE '.$date_func.'
         post_excerpt = "Approved" AND ( wp_term_relationships.term_taxonomy_id IN ('.$category_id.') ) 
        AND wp_posts.post_type = "post" AND (wp_posts.post_status = "publish")
        GROUP BY wp_posts.ID ORDER BY wp_posts.post_date DESC LIMIT 0, 10';    
    
        $postAuthors = $wpdb->get_results($postAuthorsSql);
        $postAuthorsString = '';
        foreach($postAuthors as $key=>$postAuthor){
            if($key == 0){
                $postAuthorsString .= '"'.$postAuthor->post_author.'"';
            }else{
                $postAuthorsString .= ',"'.$postAuthor->post_author.'"';
            }
        }
        $overduedSql = 'SELECT SQL_CALC_FOUND_ROWS * FROM wp_calendar WHERE event_author IN ('.$postAuthorsString.') 
            AND PERIOD_DIFF ('.$cur_date.',DATE_FORMAT(event_begin,"%Y%m")) > 1';
        $overduedMonthContracts = $wpdb->get_results($overduedSql);   
        
        return $overduedMonthContracts;
}


function get_extinguished_ontracts($category_id, $date_str){
    global $wpdb;
    $cur_month = date('m');
    $cur_year = date('Y');
    $date_func = '';    
    
    switch($date_str){
        case 'month':
            $date_func = '( ( MONTH( post_date ) = '.$cur_month.' ) ) AND ( ( YEAR( post_date ) = '.$cur_year.' ) ) AND';
            break;
        case 'year':
            $date_func = 'year';
            break;   
        case 'all_period':
            $date_func = '';
            break;  
        default:
            break;
    }    
    
    if($date_func == 'year'){
        $extinguishedMonthContracts = array();
        for($i = 1; $i<12; $i++){
            $extinguishedSql = 'SELECT SQL_CALC_FOUND_ROWS * FROM wp_posts  INNER JOIN wp_term_relationships 
            ON (wp_posts.ID = wp_term_relationships.object_id) WHERE ( ( MONTH( post_date ) = '.$i.' ) )
            AND post_excerpt = "Extinguished" AND ( wp_term_relationships.term_taxonomy_id IN ('.$category_id.') ) 
            AND wp_posts.post_type = "post" AND (wp_posts.post_status = "publish")
            GROUP BY wp_posts.ID ORDER BY wp_posts.post_date DESC LIMIT 0, 10';             
            $extinguishedMonthContracts[$i] = count($wpdb->get_results($extinguishedSql));
        }
        $extinguishedMonthContracts = implode(',',$extinguishedMonthContracts);        
    }else{
        
        $extinguishedSql = 'SELECT SQL_CALC_FOUND_ROWS * FROM wp_posts  INNER JOIN wp_term_relationships 
        ON (wp_posts.ID = wp_term_relationships.object_id) WHERE '.$date_func.'
         post_excerpt = "Extinguished" AND ( wp_term_relationships.term_taxonomy_id IN ('.$category_id.') ) 
        AND wp_posts.post_type = "post" AND (wp_posts.post_status = "publish")
        GROUP BY wp_posts.ID ORDER BY wp_posts.post_date DESC LIMIT 0, 10';
        
        $extinguishedMonthContracts = $wpdb->get_results($extinguishedSql);
    }
    
    return $extinguishedMonthContracts;
}


function get_clients_events($user_id){
    global $wpdb;
    $clients_events = $wpdb->get_results("SELECT * FROM ".WP_CALENDAR_TABLE." WHERE event_author='".$user_id."' ");

    $events = array();
    foreach($clients_events as $key=>$event){
        $descr = $event->event_desc;
        $arr = explode(':' , $descr);        
        $events[$key]['main_per_month'] = $event->event_main_permonth;
        $events[$key]['percent_per_month'] = $event->event_percent_permonth;
        $events[$key]['title'] = $arr[1];
        $events[$key]['start_date'] =  $event->event_begin;
        $events[$key]['col_months'] = $event->event_repeats;
        $events[$key]['id'] = $event->event_id;
        
        $events[$key]['loan_summ'] = $event->event_summ;
        $events[$key]['loan_currency'] = $event->event_currency;
        $events[$key]['loan_all_summ'] = $event->event_all_summ;
        $events[$key]['loan_percentage'] = $event->event_percentage;
        
        $events[$key]['main_per_month_part'] = $event->event_main_permonth_part;
        $events[$key]['percent_per_month_part'] = $event->event_percent_permonth_part;
        $events[$key]['rate_part'] = $event->event_rate_part;   
        
    }

    return $events;
}



function get_random_password() {
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, strlen($alphabet)-1);
        $pass[$i] = $alphabet[$n];
    }
    $pass = implode('',$pass);
    return $pass;
}


function get_post_by_name($post_name, $output = OBJECT) {
    global $wpdb;
    $post = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_name = %s AND post_type='post'", $post_name ));
    if ( $post )
        return get_post($post, $output);

    return null;
}



function get_user_role() {
    global $wpdb;
    $user = wp_get_current_user();
    $user_ID = $user->ID;
    $user_data = get_userdata( $user_ID );
    $role = $user_data->roles[0];
    return $role;
}

function behoof_paging_nav_1($the_query) {
    if ( $the_query->max_num_pages < 2 )
        return;
    ?>
    <nav style="width: 100px;" class="navigation paging-navigation" role="navigation">
        <div class="nav-links">
            <?php
            $current = $the_query->query['paged'];
            $next = $current + 1;
            $previous = $current - 1;
            ?>
            <?php echo get_previous_posts_link( $previous ); echo '&nbsp'; echo $current; echo '&nbsp'; echo get_next_posts_link( $next, $the_query->max_num_pages ); ?>
        </div>
    </nav>
    <?php wp_reset_postdata(); ?>
<?php
}

function behoof_paging_nav($paging_length, $full = true, $wp_query, $paged = null)
{
    $max_page = $wp_query->max_num_pages;

    $post_page_count = $wp_query->post_count;
    $post_per_page = $wp_query->query_vars['posts_per_page'];

    $nump = $paging_length;
    if(is_null($paged)){
        $paged = intval(get_query_var('paged'));
    }

    if (empty($paged) || $paged == 0 || $paged > $max_page)
        $paged = 1;

    if ($max_page > 1)
    {
        if ($full)
        {
            //echo '<span style="width:100px;">Page ' . $wp_query->query['paged'] . ' of ' . $max_page;echo '</span>';
            echo '<nav class="navigation paging-navigation" role="navigation">';
            echo '<div class="nav-links">';

            if (($paged - $nump) > 1)
                $start = $paged - $nump;
            else
                $start = 1;

            if (($paged + $nump) < $max_page)
                $end = $paged + $nump;
            else
                $end = $max_page;

            $p = false;
            for($i=$start; $i<=$max_page; $i++){
                // проверим, находимся ли мы на последней странице и не равна ли эта последняя страница кол-ву показываемых страниц
                // показываем 1-ю страницу всегда,если перевалило за кол-во показываемых страниц
                if(($start-1)>0 && $i != $paged && $p == false){
                    echo '<a href="' . get_pagenum_link(1) . '" >' . 1 . '</a>'; echo '...';
                    $p = true;
                }
                //проверяем страницу на текущую
                else if($i == $paged){
                    //если текущая страница, то мы выводим просто номер страницы а не ссылку
                    echo $i; echo '&nbsp';
                }
                //если это не текущая страница, выводим ссылку на страницу
                else{
                    // проверим, сколько уже страниц мы вывели, если вывели достаточно, но нужно поставить многоточие, вывести последнюю страницу и выйти из цикла
                    if($i == $end){
                        echo '<a href="' . get_pagenum_link($i) . '" >' . $i . '</a>';
                        // проверить, не конец ли это вообще цикла, тогда не нужно выводить многоточие
                        if($i == $max_page || $i == ($max_page-1)){
                            echo '&nbsp';
                        }else{
                            echo '...';
                        }
                    }else if($i < $end){
                        echo '<a href="' . get_pagenum_link($i) . '" >' . $i . '</a>'; echo '&nbsp';
                    }else if($i > $end){
                        echo '<a href="' . get_pagenum_link($max_page) . '" >' . $max_page . '</a>';
                        break;
                    }
                }
            }
            echo '</div>' ;
            echo '</nav>';
        }
    }
}


add_action('wp_logout','go_home');
function go_home(){
    wp_redirect( home_url() );
    exit();
}


function behoof_widgets_init() {

    register_sidebar( array(
        'name' => 'sidebar',
        'id' => 'sidebar',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="rounded">',
        'after_title' => '</h2>',
    ) );
}
add_action( 'widgets_init', 'behoof_widgets_init' );


function get_max($max_array){
    $max = 0;
    for($i=0; $i<count($max_array); $i++){
        if($max_array[$i] > $max){
            $max = $max_array[$i];
        }
    } 
    
    return $max;
}

//function get_rate($days,$fine_rate,$main_per_month){            
//    $fine = 0; //пеня
//    if ($days <= 31)
//        $fine += (int)($main_per_month * $days * $fine_rate / 100);
//    else
//    {
//        $fine += (int)($main_per_month * 30 * $fine_rate / 100);
//        $main_per_month *= 2;
//        $days -= 30;
//        $fine += Count($main_per_month, $days, $fine_rate);
//    }
//    return $fine;   
//}


function get_rate_data($payment, $fine_rate, $col_months){
    $dates_of_payment = array();
    $current_date = date('Y/m/d');

    for($j = 0; $j<$col_months; $j++){
        $dates_of_payment[] = date("Y/m/d",strtotime(date("Y/m/d", strtotime($current_date)) . "-".$j." month"));
    }

     //дальше определяем текущую дату, например 
     
     $fine = 0; // определяем переменную для пени

     $days = 0; // переменная для количества дней в месяце
     for ($i = 0; $i < $col_months-1; $i++)
     {
         $days = date("d",strtotime($dates_of_payment[$i]) - strtotime($dates_of_payment[$i + 1]));
         $fine += month_fine_count($days, $payment * ($i + 1), $fine_rate);
     }

     $days = ($current_date - $dates_of_payment[$col_months - 1]);
     $fine += month_fine_count($days, $payment * $col_months, $fine_rate);
     
     return $fine;    
}

function month_fine_count( $days,  $sum,  $fine_rate)
{
    $fine = (int)($sum * $days * $fine_rate / 100);
    return $fine;
}



function get_all_payment_data($col_years,$loan_summ,$percent){
    $col_months = $col_years * 12;

    $chislitel = round($loan_summ * round( ($percent/1200), 5) );
    $znamenatel =  round( (1- (pow(1+round(($percent/1200),5), ($col_months*(-1))    )) ) , 5);
    $summ_per_month = (int) round( ($chislitel / $znamenatel));

    $total_summ = $summ_per_month * $col_months;

    $percent_per_month = (int) round($summ_per_month - ($loan_summ / ($col_months)));
    $main_summ_per_month = $summ_per_month - $percent_per_month;      
    
    return array('total_summ' => $total_summ, 'percent_per_month' =>$percent_per_month, 'main_summ_per_month' => $main_summ_per_month);
}

function curPageURL() {
    $pageURL = 'http';
    if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
     $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
    } else {
     $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    }
    return $pageURL;
}


add_action('wp_ajax_set_vote', 'prefix_ajax_set_vote');
add_action('wp_ajax_nopriv_set_vote', 'prefix_ajax_set_vote');
function prefix_ajax_set_vote(){
    global $wpdb;

    if(isset($_POST['request_id']) && !empty($_POST['request_id'])) $request_id = $_POST['request_id']; else $request_id = '';
    if(isset($_POST['user_id']) && !empty($_POST['user_id'])) $user_id = $_POST['user_id']; else $user_id = '';
    if(isset($_POST['checked']) && !empty($_POST['checked'])) $checked = $_POST['checked']; else $checked = '';
    
    $yes = 0;
    $no = 0;    
    
    if($checked == 1){
        $yes = 1;
    }else{
        $no = 1;
    }
    
    $count_query = 'SELECT * FROM wp_poll WHERE poll_request_id ='.$request_id.' ';
    $count = count($wpdb->get_results($count_query));
    if($count == 0){    
        $query = 'INSERT INTO wp_poll SET poll_request_id = '.$request_id.', poll_col_voters = poll_col_voters+1, poll_yes = poll_yes+'.$yes.', poll_no = poll_no+'.$no.'';
        $wpdb->query($query);
    }else{
        $query = 'UPDATE wp_poll SET poll_col_voters = poll_col_voters+1, poll_yes = poll_yes+'.$yes.', poll_no = poll_no+'.$no.' WHERE poll_request_id = '.$request_id.'';
        $wpdb->query($query);        
    }
    
    $query = 'INSERT INTO wp_poll_rel SET request_id = '.$request_id.', author_id = '.$user_id.'';

    $wpdb->query($query);
    
    return true;    
}

function get_vote_information($user_ID,$request_id){
    global $wpdb;
    $inf = array();
    // проверим, голосовал ли чувак уже, если да, то пойдем дальше, если нет, то ничего не делаем
    
    $count_query = 'SELECT * FROM wp_poll_rel WHERE request_id ='.$request_id.' AND  author_id='.$user_ID.' ';
    $count = count($wpdb->get_results($count_query));
    if($count != 0){
        //голосовал
        $inf['is_voted'] = true;
        $query = 'SELECT * FROM wp_poll WHERE poll_request_id ='.$request_id.' ';
        $information = $wpdb->get_results($query);   
        
        $all_votes = $information[0]->poll_col_voters;
        $yes_votes = $information[0]->poll_yes;
        $no_votes = $information[0]->poll_no;
        
        $yes_percent = round(($yes_votes*100)/$all_votes);
        $no_percent = round(($no_votes*100)/$all_votes);
        
        $inf['yes'] = $yes_percent;
        $inf['no'] = $no_percent;
        $inf['all'] = $all_votes;
        
        $args['role'] = 'head';     
        $users = get_users( $args );
        
        if(count($users) == (int)$all_votes){
            $inf['is_inprocess'] = false;
            if((int)$yes_votes > (int)$no_votes){
                $inf['func'] = 'yes'; 
            } 
            if((int)$yes_votes <= (int)$no_votes){
                $inf['func'] = 'no'; 
            } 
            
            // удалить запись в базе poll_rel и в poll , где есть айдишник поста

            $sql = "DELETE FROM wp_poll WHERE poll_request_id = $request_id";
            $wpdb->query($sql);
            $sql = "DELETE FROM wp_poll_rel WHERE request_id = $request_id";
            $wpdb->query($sql);            
            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');            
        }else{
            $inf['is_inprocess'] = true;
        }  
        

    }else{
        // не голосовал
        $inf['is_voted'] = false;
    }   
    
    return $inf;
}


function get_pagination_data($category_id,$page_num,$per_page = PER_PAGE){
    global $wpdb;
    
    $sql = 'SELECT * FROM wp_posts  INNER JOIN wp_term_relationships 
    ON (wp_posts.ID = wp_term_relationships.object_id) WHERE 
    ( wp_term_relationships.term_taxonomy_id IN ('.$category_id.') ) 
    AND wp_posts.post_type = "post" AND (wp_posts.post_status = "publish")
    GROUP BY wp_posts.ID ORDER BY wp_posts.post_date DESC LIMIT '.$per_page.' OFFSET '. (($page_num-1)*$per_page) .' ';
    
    $posts = $wpdb->get_results($sql);    
    
    return $posts;
}

function get_pagination_links($category_id,$page_num,$per_page = PER_PAGE){
    global $wpdb; 
    $cur_url = 'http://' .$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ;
    $end_url = end(explode('?', $cur_url));
    $get_array = explode('&', $end_url);
    
    $main_url_part = '';
    if(count($get_array) == 1){
        $main_url_part = $get_array[0];
    }else{
        for($i=0; $i< (count($get_array)); $i++){
            $part = (explode('=',$get_array[$i]));
            if($part[0] != 'page'){                
                $main_url_part .= $get_array[$i];
                if( ($i+1) < (count($get_array)) ){
                    $main_url_part .= '&'; 
                }
            }
        }
    }
    $last_letter = substr($main_url_part, -1);
    if($last_letter == '&'){
        $main_url_part = substr($main_url_part, 0, strlen($main_url_part)-1);
    }
    
    $sql = 'SELECT ID FROM wp_posts  INNER JOIN wp_term_relationships 
    ON (wp_posts.ID = wp_term_relationships.object_id) WHERE 
    ( wp_term_relationships.term_taxonomy_id IN ('.$category_id.') ) 
    AND wp_posts.post_type = "post" AND (wp_posts.post_status = "publish")
    GROUP BY wp_posts.ID ORDER BY wp_posts.post_date DESC';
    
    $page_max = round((count($wpdb->get_results($sql))/$per_page));

    $start = 1;
    $end = $page_num;
    
    if($page_num <= PAG_LINKS_NUM){
        $start = 1;
        if( ($page_max-$page_num)>=PAG_LINKS_NUM){
            $end = $page_num + PAG_LINKS_NUM;
        }else{
            $end = $page_max;
        }
    }
    if($page_num > PAG_LINKS_NUM){
        $start = $page_num - PAG_LINKS_NUM;
        if( ($page_max-$page_num)>=PAG_LINKS_NUM){
            $end = $page_num + PAG_LINKS_NUM;
        }else{
            $end = $page_max;
        }
    }
    $dis_start = '';
    $dis_end = '';
    
    if($page_num != 1){
        $prev = $page_num - 1;
    }else {
        $prev = 1;
        $dis_start = 'disabled';
    }
    if($page_num != $page_max){
        $next = $page_num + 1;
    }else{
        $next = $page_num;
        $dis_end = 'disabled';
    }
    
    ?>
    <ul class="pagination">
        <li class="<?=$dis_start;?>"><a href="<?php $cur_url; echo '?'.$main_url_part; echo '&page='.$prev; ?>">&laquo;</a></li>
        <?php for($i=$start; $i<=$end; $i++): ?>
            <?php if($i == $page_num) $class_name = 'active'; else $class_name = ''; ?>
            <li class="<?=$class_name;?>"><a href="<?php $cur_url; echo '?'.$main_url_part; echo'&page='.$i; ?> "><?=$i;?></a></li>           
        <?php endfor; ?>
        <li class="<?=$dis_end;?>"><a href="<?php $cur_url; echo '?'.$main_url_part; echo '&page='.$next; ?>">&raquo;</a></li>
    </ul>
    <?php 
}

function get_cur_page_num(){
    if(isset($_GET['page']) && !empty($_GET['page'])){
    $page_num = $_GET['page'];
    }else{
        $page_num = 1;
    } 
    
    return $page_num;
}
