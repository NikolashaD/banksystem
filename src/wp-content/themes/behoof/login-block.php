<div class="well sidebar-nav">
<!--  <form class="form-signin">
          <input type="text" class="form-control" placeholder="Username" autofocus="">
          <input type="password" class="form-control" placeholder="Password">
          <label class="checkbox">
            <input type="checkbox" value="remember-me"> Remember me
          </label>
          <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
          <a href="<?php echo get_bloginfo('wpurl'); ?>/wp-login.php?action=register" class="simplemodal-register">Register</a> or <a href="<?php echo get_bloginfo('wpurl'); ?>/wp-login.php?action=login" class="simplemodal-login">Log in</a>
    </form>
    <a href="<?php echo get_bloginfo('wpurl'); ?>/wp-login.php" class="simplemodal-login">Log In</a>
    <a href="<?php echo get_bloginfo('wpurl'); ?>/wp-login.php?action=register" class="simplemodal-register">Register</a>-->


<?php
    $login_args = array(
        'echo' => true,
        'redirect' => get_bloginfo('wpurl'),
        'form_id' => 'loginform',
        'label_username' => __( 'Имя пользователя' ),
        'label_password' => __( 'Пароль' ),
        'label_remember' => __( 'Запомнить меня' ),
        'label_log_in' => __( 'Войти' ),
        'id_username' => 'user_login',
        'id_password' => 'user_pass',
        'id_remember' => 'rememberme',
        'id_submit' => 'wp-submit',
        'remember' => true,
        'value_username' => NULL,
        'value_remember' => false );
?>
<?php
   if(!is_user_logged_in()){
       wp_login_form( $login_args );
   }else{
       $user_id = get_current_user_id();
       $user_name = get_userdata( $user_id )->display_name;

       echo 'Добро пожаловать,<br><span class="glyphicon glyphicon-user"></span><b> '.$user_name.'</b> <a style="float:right;" href="'. wp_logout_url('/') .'" class="simplemodal-register">Выйти</a>';
       $role = get_user_role();
       if($role == ADMIN){
           wp_register();
       }
   }
?>


</div><!--/.well -->