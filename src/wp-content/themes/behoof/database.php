<?php

    function remove_row_from_database($address,$price,$date,$building,$land,$sfBuilding,$sfLand,$propType,$subdivision,$comments,$construction,$user_ID) 
    {        
        global $wpdb;
        $sql = "DELETE FROM wp_matrix WHERE user_id = $user_ID AND address = $address AND price = $price AND date = $date AND building = $building AND land = $land AND sf_building = $sfBuilding AND sf_land = $sfLand AND prop_type = $propType AND subdivision = $subdivision AND comments = $comments AND construction = $construction";
    }

    function clear ($user_ID,$sold_comps, $forsale_comps, $target_prop, $table_name)
    {
        global $wpdb;	
        if(isset($sold_comps)) {
            $sql = "DELETE FROM $table_name WHERE user_id = $user_ID AND is_sold_comps = $sold_comps AND is_forsale_comps = $forsale_comps AND is_target_prop = $target_prop";
        }
        else {
             $sql = "DELETE FROM $table_name WHERE user_id = $user_ID";
        }
        $wpdb->query($sql);
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        
    }

    function deactivate ()
    {
        global $wpdb;	
        $table_name = $wpdb->prefix . "matrix";
        $sql = "DROP TABLE ". $table_name;
        $wpdb->query($sql);
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        $table_name = $wpdb->prefix . "tdc";
        $sql = "DROP TABLE ". $table_name;
        $wpdb->query($sql);
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        $table_name = $wpdb->prefix . "matrix_data";
        $sql = "DROP TABLE ". $table_name;
        $wpdb->query($sql);
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        $table_name = $wpdb->prefix . "noi";
        $sql = "DROP TABLE ". $table_name;
        $wpdb->query($sql);
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        $table_name = $wpdb->prefix . "noi_data";
        $sql = "DROP TABLE ". $table_name;
        $wpdb->query($sql);
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');        
    }

    function create_table()
    { 
        global $wpdb;
        $table_name = $wpdb->prefix . "blacklist";  

        if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {          
            $sql = "CREATE TABLE " . $table_name . "(
                `id` bigint(20) unsigned NOT NULL auto_increment,
                `last_id` bigint(20) unsigned NOT NULL,
                `user_login` varchar(255) default NULL,
                `user_email` varchar(255) default NULL,
                `date` date default NULL,
                `user_first_name` varchar(255) default NULL,
                `user_last_name` varchar(255) default NULL,
                PRIMARY KEY (`id`)
            );";       
            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            dbDelta($sql); 
        }
        
               

    }
